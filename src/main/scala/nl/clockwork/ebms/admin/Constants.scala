/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin

import java.awt.Color
import java.text.SimpleDateFormat
import java.util.Date

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import org.joda.time.{DateTime, Period}

object Constants
{
	final val DATE_FORMAT = "dd-MM-yyyy"
	final val DATE_MONTH_FORMAT = "MM-yyyy"
	final val DATE_YEAR_FORMAT = "yyyy"
	final val DATETIME_FORMAT = "dd-MM-yyyy HH:mm:ss"
	final val DATETIME_HOUR_FORMAT = "dd-MM-yyyy HH:mm"

	object JQueryLocale extends Enumeration
	{
		val EN = Value("en")
	}

	sealed abstract class TimeUnit(val timeUnit : Period, val period : Period, val dateFormat : String, val timeUnitDateFormat : String)
	{
		def format(date : Date) : String = new SimpleDateFormat(dateFormat).format(date)

		def getFrom : DateTime = getFrom(new Date)

		def getFrom(date : Date) : DateTime =
			this match
			{
				case TimeUnit.HOUR => new DateTime(date.getTime).withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).plusHours(1).minus(period)
				case TimeUnit.DAY => new DateTime(date.getTime).withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).withHourOfDay(0).plusDays(1).minus(period)
				//case TimeUnit.WEEK => new DateTime.withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).withHourOfDay(0).withDayOfWeek(1).plusWeeks(1).minus(this.getPeriod)
				case TimeUnit.MONTH => new DateTime(date.getTime).withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).withHourOfDay(0).withDayOfMonth(1).plusMonths(1).minus(this.period)
				case TimeUnit.YEAR => new DateTime(date.getTime).withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).withHourOfDay(0).withDayOfYear(1).plusYears(1).minus(this.period)
			}
	}
	object TimeUnit
	{
		case object HOUR extends TimeUnit(Period.minutes(1),Period.hours(1),DATETIME_HOUR_FORMAT,"mm")
		case object DAY extends TimeUnit(Period.hours(1),Period.days(1),DATE_FORMAT,"HH")
		//case object WEEK extends TimeUnit(Period.days(1),Period.weeks(1),DATE_FORMAT,"dd"), MONTH(Period.weeks(1),Period.months(1),DATE_FORMAT,"ww")
		case object MONTH extends TimeUnit(Period.days(1),Period.months(1),DATE_MONTH_FORMAT,"dd")
		case object YEAR extends TimeUnit(Period.months(1),Period.years(1),DATE_YEAR_FORMAT,"MM")

		val values = List(HOUR,DAY/*,WEEK*/,MONTH,YEAR)
	}
	
	sealed abstract class EbMSMessageTrafficChartSerie(val name : String, val color : Color, val ebMSMessageStatuses : Array[EbMSMessageStatus])
	object EbMSMessageTrafficChartSerie
	{

		case object RECEIVE_STATUS_OK extends EbMSMessageTrafficChartSerie("Ok", Color.GREEN, Array[EbMSMessageStatus](EbMSMessageStatus.PROCESSED, EbMSMessageStatus.FORWARDED))
		case object RECEIVE_STATUS_WARN extends EbMSMessageTrafficChartSerie("Warn", Color.ORANGE, Array[EbMSMessageStatus](EbMSMessageStatus.RECEIVED))
		case object RECEIVE_STATUS_NOK extends EbMSMessageTrafficChartSerie("Failed", Color.RED, Array[EbMSMessageStatus](EbMSMessageStatus.UNAUTHORIZED, EbMSMessageStatus.NOT_RECOGNIZED, EbMSMessageStatus.FAILED))
		case object RECEIVE_STATUS extends EbMSMessageTrafficChartSerie("Received", Color.BLACK, Array[EbMSMessageStatus](EbMSMessageStatus.UNAUTHORIZED, EbMSMessageStatus.NOT_RECOGNIZED, EbMSMessageStatus.RECEIVED, EbMSMessageStatus.PROCESSED, EbMSMessageStatus.FORWARDED, EbMSMessageStatus.FAILED))
		case object SEND_STATUS_OK extends EbMSMessageTrafficChartSerie("Ok", Color.GREEN, Array[EbMSMessageStatus](EbMSMessageStatus.DELIVERED))
		case object SEND_STATUS_WARN extends EbMSMessageTrafficChartSerie("Warn", Color.ORANGE, Array[EbMSMessageStatus](EbMSMessageStatus.SENT))
		case object SEND_STATUS_NOK extends EbMSMessageTrafficChartSerie("Failed", Color.RED, Array[EbMSMessageStatus](EbMSMessageStatus.DELIVERY_FAILED, EbMSMessageStatus.EXPIRED))
		case object SEND_STATUS extends EbMSMessageTrafficChartSerie("Sent", Color.BLUE, Array[EbMSMessageStatus](EbMSMessageStatus.SENT, EbMSMessageStatus.DELIVERED, EbMSMessageStatus.DELIVERY_FAILED, EbMSMessageStatus.EXPIRED))
	}
	sealed abstract class EbMSMessageTrafficChartOption(val title : String, val ebMSMessageTrafficChartSeries : Array[EbMSMessageTrafficChartSerie])
	object EbMSMessageTrafficChartOption
	{
		case object ALL extends EbMSMessageTrafficChartOption("All Messages",Array[EbMSMessageTrafficChartSerie](EbMSMessageTrafficChartSerie.RECEIVE_STATUS,EbMSMessageTrafficChartSerie.SEND_STATUS))
		case object RECEIVED extends EbMSMessageTrafficChartOption("Received Messages",Array[EbMSMessageTrafficChartSerie](EbMSMessageTrafficChartSerie.RECEIVE_STATUS_NOK,EbMSMessageTrafficChartSerie.RECEIVE_STATUS_WARN,EbMSMessageTrafficChartSerie.RECEIVE_STATUS_OK,EbMSMessageTrafficChartSerie.RECEIVE_STATUS))
		case object SENT extends EbMSMessageTrafficChartOption("Sent Messages",Array[EbMSMessageTrafficChartSerie](EbMSMessageTrafficChartSerie.SEND_STATUS_NOK,EbMSMessageTrafficChartSerie.SEND_STATUS_WARN,EbMSMessageTrafficChartSerie.SEND_STATUS_OK,EbMSMessageTrafficChartSerie.SEND_STATUS))

		val values = List(ALL,RECEIVED,SENT)
	}

}
