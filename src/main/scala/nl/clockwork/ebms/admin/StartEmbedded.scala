/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin

import java.io.File
import java.sql.DriverManager
import java.util.{ArrayList, EnumSet, HashMap, List, Map}

import nl.clockwork.ebms.admin.web.ExtensionProvider
import nl.clockwork.ebms.admin.web.configuration.JdbcURL
import org.apache.commons.cli.Options
import org.apache.commons.io.IOUtils
import org.apache.commons.lang.StringUtils
import org.eclipse.jetty.server.DispatcherType
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.bio.SocketConnector
import org.eclipse.jetty.server.ssl.SslSocketConnector
import org.eclipse.jetty.servlet.ErrorPageErrorHandler
import org.eclipse.jetty.servlet.FilterHolder
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.util.ssl.SslContextFactory
import org.hsqldb.persist.HsqlProperties
import org.hsqldb.server.EbMSServerProperties
import org.hsqldb.server.ServerConfiguration
import org.hsqldb.server.ServerConstants
import org.springframework.context.support.ClassPathXmlApplicationContext
import org.springframework.web.context.ContextLoaderListener
import resource.managed

import scala.collection.JavaConversions._

class StartEmbedded extends Start
{
	protected var properties : Map[String,String] = _

	override protected def createOptions : Options =
	{
		options = new Options
		options.addOption("h",false,"print this message")
		options.addOption("host",true,"set host")
		options.addOption("port",true,"set port")
		options.addOption("ssl",false,"use ssl")
		options.addOption("keystore",true,"set keystore")
		options.addOption("password",true,"set keystore password")
		options.addOption("authentication",false,"use basic authentication")
		options.addOption("jmx",false,"start mbean server")
		options.addOption("hsqldb",false,"start hsqldb server")
		options.addOption("hsqldbDir",true,"set hsqldb location (default: hsqldb)")
		options
	}
	
	private def getProperties(files : String*) : Map[String,String] =
	{
		var properties = null.asInstanceOf[Map[String,String]]
			managed (new ClassPathXmlApplicationContext(files : _*)).foreach(applicationContext =>
		{
			properties = applicationContext.getBean("propertyConfigurer").asInstanceOf[PropertyPlaceholderConfigurer].getProperties
		})
		properties
	}

	private def initHSQLDB
	{
		if ("org.hsqldb.jdbcDriver".equals(properties.get("ebms.jdbc.driverClassName")) && cmd.hasOption("hsqldb"))
		{
			val jdbcURL = nl.clockwork.ebms.admin.web.configuration.Utils.parseJdbcURL(properties.get("ebms.jdbc.url"),new JdbcURL)
			if (!jdbcURL.getHost.matches("(localhost|127.0.0.1)"))
			{
				System.out.println("Cannot start server on " + jdbcURL.getHost)
				System.exit(1)
			}
			System.out.println("Starting hsqldb...")
			startHSQLDBServer(jdbcURL)
		}
	}

	def startHSQLDBServer(jdbcURL : JdbcURL)
	{
		val options = new ArrayList[String]
		options.add("-database.0")
		options.add((if (cmd.hasOption("hsqldbDir")) "file:" + cmd.getOptionValue("hsqldbDir") else "file:hsqldb") + "/" + jdbcURL.getDatabase)
		options.add("-dbname.0")
		options.add(jdbcURL.getDatabase)
		if (jdbcURL.getPort != 0)
		{
			options.add("-port")
			options.add(Integer.toString(jdbcURL.getPort))
		}
		val argProps = HsqlProperties.argArrayToProps(options.toArray(new Array[String](0)),"server")
		val props = new EbMSServerProperties(ServerConstants.SC_PROTOCOL_HSQL)
		props.addProperties(argProps)
		ServerConfiguration.translateDefaultDatabaseProperty(props)
		ServerConfiguration.translateDefaultNoSystemExitProperty(props)
		ServerConfiguration.translateAddressProperty(props)
		val server = new org.hsqldb.server.Server
		server.setProperties(props)
		server.start
		initDatabase(server)
	}

	private def initDatabase(server : org.hsqldb.server.Server)
	{
    managed(DriverManager.getConnection("jdbc:hsqldb:hsql://localhost:" + server.getPort + "/" + server.getDatabaseName(0,true), "sa", "")).foreach(c =>
		{
			if (!c.createStatement.executeQuery("select table_name from information_schema.tables where table_name = 'CPA'").next)
			{
				c.createStatement.executeUpdate(IOUtils.toString(this.getClass.getResourceAsStream("/nl/clockwork/ebms/admin/database/hsqldb.sql")))
				val extensionProviders = ExtensionProvider.get
				for (extensionProvider <- extensionProviders)
					if (!StringUtils.isEmpty(extensionProvider.getHSQLDBFile))
						c.createStatement.executeUpdate(IOUtils.toString(this.getClass.getResourceAsStream(extensionProvider.getHSQLDBFile)))
				System.out.println("EbMS tables created")
			}
			else
				System.out.println("EbMS tables already exist")
		})
	}

	private def initEbMSServer
	{
		if (!"true".equals(properties.get("ebms.ssl")))
		{
			val connector = new SocketConnector
			connector.setHost(if (StringUtils.isEmpty(properties.get("ebms.host"))) "0.0.0.0" else properties.get("ebms.host"))
			connector.setPort(if (StringUtils.isEmpty(properties.get("ebms.port"))) 8888 else Integer.parseInt(properties.get("ebms.port")))
			server.addConnector(connector)
			System.out.println("EbMS service configured on http://" + getHost(connector.getHost) + ":" + connector.getPort + properties.get("ebms.path"))
		}
		else
		{
			val keystore = getResource(properties.get("keystore.path"))
			if (keystore != null && keystore.exists)
			{
				val factory = new SslContextFactory
				if (!StringUtils.isEmpty(properties.get("https.enabledProtocols")))
					factory.setIncludeProtocols(properties.get("https.enabledProtocols").split(',').map(_.trim) : _*)
				if (!StringUtils.isEmpty(properties.get("https.enabledCipherSuites")))
					factory.setIncludeCipherSuites(properties.get("https.enabledCipherSuites").split(',').map(_.trim) : _*)
				factory.setKeyStoreResource(keystore)
				factory.setKeyStorePassword(properties.get("keystore.password"))
				if ("true".equals(properties.get("https.requireClientAuthentication")))
				{
					val truststore = getResource(properties.get("truststore.path"))
					if (truststore != null && truststore.exists)
					{
						factory.setNeedClientAuth(true)
						factory.setTrustStoreResource(truststore)
						factory.setTrustStorePassword(properties.get("truststore.password"))
					}
					else
					{
						System.out.println("EbMS service not available: truststore " + properties.get("truststore.path") + " not found!")
						System.exit(1)
					}
				}
				val connector = new SslSocketConnector(factory)
				connector.setHost(if (StringUtils.isEmpty(properties.get("ebms.host"))) "0.0.0.0" else properties.get("ebms.host"))
				connector.setPort(if (StringUtils.isEmpty(properties.get("ebms.port"))) 8888 else Integer.parseInt(properties.get("ebms.port")))
				server.addConnector(connector)
				System.out.println("EbMS service configured on https://" + connector.getHost + ":" + connector.getPort + properties.get("ebms.path"))
			}
			else
			{
				System.out.println("EbMS service not available: keystore " + properties.get("keystore.path") + " not found!")
				System.exit(1)
			}
		}
	}

	override protected def initWebContext
	{
		val context = new ServletContextHandler(ServletContextHandler.SESSIONS)
		server.setHandler(context)

		context.setContextPath("/")

		if (cmd.hasOption("authentication"))
		{
			System.out.println("Configuring web server authentication:")
			val file = new File(REALM_FILE)
			if (file.exists)
				System.out.println("Using file: " + file.getAbsoluteFile)
			else
				createRealmFile(file)
			context.setSecurityHandler(getSecurityHandler)
		}

		context.setInitParameter("configuration","deployment")

		var contextConfigLocation = "classpath:nl/clockwork/ebms/admin/applicationContext.embedded.xml"
		val extensionProviders = ExtensionProvider.get
		for (extensionProvider <- extensionProviders)
			if (!StringUtils.isEmpty(extensionProvider.getSpringConfigurationFile))
				contextConfigLocation += "," + extensionProvider.getSpringConfigurationFile

		context.setInitParameter("contextConfigLocation",contextConfigLocation)

		val servletHolder = new ServletHolder(classOf[nl.clockwork.ebms.admin.web.ResourceServlet])
		context.addServlet(servletHolder,"/css/*")
		context.addServlet(servletHolder,"/fonts/*")
		context.addServlet(servletHolder,"/images/*")
		context.addServlet(servletHolder,"/js/*")

		context.addServlet(classOf[nl.clockwork.ebms.servlet.EbMSServlet],properties.get("ebms.path"))

		context.addServlet(classOf[org.eclipse.jetty.servlet.DefaultServlet],"/")
		
		val filterHolder = new FilterHolder(classOf[org.apache.wicket.protocol.http.WicketFilter])
		filterHolder.setInitParameter("applicationClassName","nl.clockwork.ebms.admin.web.WicketApplication")
		filterHolder.setInitParameter("filterMappingUrlPattern","/*")
		context.addFilter(filterHolder,"/*",EnumSet.of(DispatcherType.REQUEST,DispatcherType.ERROR))
		
		val errorHandler = new ErrorPageErrorHandler
		context.setErrorHandler(errorHandler)
		val errorPages = new HashMap[String,String]
		errorPages.put("404","/404")
		errorHandler.setErrorPages(errorPages)
		
		val listener = new ContextLoaderListener
		context.addEventListener(listener)
	}

}
object StartEmbedded
{
	def main(args : Array[String])
	{
		val start = new StartEmbedded
		start.initCmd(args)

		if (start.cmd.hasOption("h"))
			start.printUsage

		start.properties = start.getProperties("nl/clockwork/ebms/admin/applicationConfig.embedded.xml")
		start.server = new Server

		start.initHSQLDB
		start.initWebServer
		start.initEbMSServer
		start.initJMX
		start.initWebContext

		System.out.println("Starting web server...")

		start.server.start
		start.server.join
	}
}