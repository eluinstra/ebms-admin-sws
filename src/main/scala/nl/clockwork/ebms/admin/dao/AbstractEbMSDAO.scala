/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.dao

import java.io.IOException
import java.sql.{ResultSet, SQLException}
import java.util.Date
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

import nl.clockwork.ebms.Constants
import nl.clockwork.ebms.Constants.EbMSEventStatus
import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.Constants.TimeUnit
import nl.clockwork.ebms.admin.model.CPA
import nl.clockwork.ebms.admin.model.EbMSAttachment
import nl.clockwork.ebms.admin.model.EbMSEvent
import nl.clockwork.ebms.admin.model.EbMSEventLog
import nl.clockwork.ebms.admin.model.EbMSMessage
import nl.clockwork.ebms.admin.web.Utils
import nl.clockwork.ebms.admin.web.message.EbMSMessageFilter
import org.apache.commons.csv.CSVPrinter
import org.springframework.dao.IncorrectResultSizeDataAccessException
import org.springframework.transaction.support.TransactionTemplate

import scala.collection.JavaConversions._

abstract class AbstractEbMSDAO(val transactionTemplate : TransactionTemplate, val jdbcTemplate : JdbcTemplate ) extends EbMSDAO
{
	val baseQueryEbMSMessage = "select time_stamp, cpa_id, conversation_id, message_id, message_nr, ref_to_message_id, time_to_live, from_party_id, from_role, to_party_id, to_role, service, action, content, status, status_time from ebms_message"
	implicit def toEbMSMessageStatus(status : AnyRef) : EbMSMessageStatus = if (status != null) EbMSMessageStatus get status.asInstanceOf[Int] else null
	def mapEbMSMessage(rs : ResultSet) : EbMSMessage =
	{
		val result = new EbMSMessage
		result timestamp = rs getTimestamp "time_stamp"
		result cpaId = rs getString "cpa_id"
		result conversationId = rs getString "conversation_id"
		result messageId = rs getString "message_id"
		result messageNr = rs getInt "message_nr"
		result refToMessageId = rs getString "ref_to_message_id"
		result timeToLive = rs getTimestamp "time_to_live"
		result fromPartyId = rs getString "from_party_id"
		result fromRole = rs getString "from_role"
		result toPartyId = rs getString "to_party_id"
		result toRole = rs getString "to_role"
		result service = rs getString "service"
		result action = rs getString "action"
		result content = rs getString "content"
		result status = rs getObject "status"
		result statusTime = rs getTimestamp "status_time"
		result
	}

	override def findCPA(cpaId : String) : Option[CPA] =
		try
		{
			jdbcTemplate.queryForObjectAndMap("select cpa_id, cpa from cpa where cpa_id = ?",cpaId)
			{
				(rs, _) => new CPA(rs getString "cpa_id", rs getString "cpa")
			}
		}
		catch
		{
			case _ : IncorrectResultSizeDataAccessException => None
		}

	override def countCPAs : Int = jdbcTemplate.queryForObject[Integer]("select count(cpa_id) from cpa").get

	override def selectCPAIds : Seq[String] = jdbcTemplate queryForSeq[String] "select cpa_id from cpa order by cpa_id"

	override def selectCPAs(first : Long, count : Long) : Seq[CPA]

	override def findMessage(messageId : String) : Option[EbMSMessage] = findMessage(messageId,0)

	override def findMessage(messageId : String , messageNr : Int) : Option[EbMSMessage] =
	{
		val result = jdbcTemplate.queryForObjectAndMap(baseQueryEbMSMessage +	" where message_id = ? and message_nr = ?",messageId,messageNr)
		{
			(rs, _) => mapEbMSMessage(rs)
		}
		val message = result.get
		message attachments  = getAttachments(messageId,messageNr)
		message.attachments.foreach(attachment => attachment message = message)
		message event = getEvent(messageId).get
		message events = getEvents(messageId)
		message.events.foreach(event => event message = result get)
		result
	}

	override def findResponseMessage(messageId : String) : Option[EbMSMessage] =
	{
		val result = jdbcTemplate.queryForObjectAndMap(baseQueryEbMSMessage + " where ref_to_message_id = ? and message_nr = ? and service = ?",messageId,0,Constants.EBMS_SERVICE_URI)
		{
			(rs, _) => mapEbMSMessage(rs)
		}
		val message = result.get
		message attachments = List[EbMSAttachment]()
		message events = getEvents(messageId)
		message.events.foreach(event => event message = message)
		result
	}

	override def countMessages(filter : EbMSMessageFilter) : Int =
	{
		val parameters = Seq[Object]()
		jdbcTemplate queryForObject[Integer]("select count(message_id) from ebms_message where 1 = 1" + getMessageFilter(filter,parameters),parameters : _*) get
	}
	
	override def selectMessages(filter : EbMSMessageFilter, first : Long, count : Long) : Seq[EbMSMessage] =
	{
		val parameters = Seq[Object]()
		jdbcTemplate.queryAndMap(baseQueryEbMSMessage + " where 1 = 1" + getMessageFilter(filter,parameters) + " order by time_stamp desc limit " + count + " offset " + first,parameters)
		{
			(rs, _) => mapEbMSMessage(rs)
		}
	}
	
	override def findAttachment(messageId : String, messageNr : Int, contentId : String) : Option[EbMSAttachment] =
		jdbcTemplate.queryForObjectAndMap("select name, content_id, content_type, content from ebms_attachment where message_id = ? and message_nr = ? and content_id = ?",messageId,messageNr,contentId)
		{
			(rs, _) => new EbMSAttachment(rs getString "name", rs getString "content_id" ,rs getString "content_type", rs getBytes "content")
		}

	def getAttachments(messageId : String, messageNr : Int) : Seq[EbMSAttachment] =
		jdbcTemplate.queryAndMap("select name, content_id, content_type from ebms_attachment where message_id = ? and message_nr = ?",messageId,messageNr)
		{
			(rs,_) => new EbMSAttachment(rs getString "name", rs getString "content_id", rs getString "content_type", null)
		}

	private def getEvent(messageId : String) : Option[EbMSEvent] =
		try
		{
			jdbcTemplate.queryForObjectAndMap("select time_to_live, time_stamp, retries from ebms_event where message_id = ?",messageId)
			{
				(rs, _) => new EbMSEvent(rs getTimestamp "time_to_live", rs getTimestamp "time_stamp", rs getInt "retries")
			}
		}
		catch
		{
			case _ : IncorrectResultSizeDataAccessException => None
		}

	private def getEvents(messageId : String) : Seq[EbMSEventLog] =
		jdbcTemplate.queryAndMap("select message_id, time_stamp, uri, status, error_message from ebms_event_log where message_id = ?",messageId)
		{
			(rs, _) => new EbMSEventLog(rs getTimestamp "time_stamp", rs getString "uri", EbMSEventStatus get(rs getInt "status"), rs getString "error_message")
		}

	override def selectMessageIds(cpaId : String, fromRole : String, toRole : String, statuses : EbMSMessageStatus*) : Seq[String] =
		jdbcTemplate.queryForSeq[String]("select message_id from ebms_message where cpa_id = ? and from_role = ? and to_role = ? and status in (" + statuses.map(s => s.id).mkString(",") + ") order by time_stamp desc",cpaId,fromRole,toRole)

	override def selectMessageTraffic(from : Date, to : Date, timeUnit : TimeUnit, statuses : EbMSMessageStatus*) : Map[Date,Number] =
	{
		var result = Map[Date,Number]()
		jdbcTemplate.queryAndMap("select trunc(time_stamp,'" + toDateFormat(timeUnit timeUnitDateFormat) + "') time, count(*) nr from ebms_message" +
			" where time_stamp >= ? and time_stamp < ?" + (if (statuses.length == 0) " and status is not null" else " and status in (" + statuses.map(s => s.id).mkString(",") + ")") +" group by trunc(time_stamp,'" + toDateFormat(timeUnit timeUnitDateFormat) + "')",from,to)
		{
			(rs, _) => result += (rs.getTimestamp("time") -> rs.getInt("nr")); null
		}
		result
	}

	def toDateFormat(timeUnitDateFormat : String) : String = if ("mm" equals timeUnitDateFormat) "mi" else timeUnitDateFormat

	override def printMessagesToCSV(printer : CSVPrinter, filter : EbMSMessageFilter)
	{
		val parameters = List[Object]()
		jdbcTemplate.queryAndMap("select time_stamp, cpa_id, conversation_id, message_id, message_nr, ref_to_message_id, time_to_live, from_party_id, from_role, to_party_id, to_role, service, action, status, status_time from ebms_message" +
			" where 1 = 1" + getMessageFilter(filter,parameters) + " order by time_stamp desc",parameters)
		{
			(rs, _) =>
				try
				{
					printer print(rs getString "message_id")
					printer print(rs getInt "message_nr")
					printer print(rs getString "ref_to_message_id")
					printer print(rs getString "conversation_id")
					printer print(rs getTimestamp "time_stamp")
					printer print(rs getTimestamp "time_to_live")
					printer print(rs getString "cpa_id")
					printer print(rs getString "from_role")
					printer print(rs getString "to_role")
					printer print(rs getString "service")
					printer print(rs getString "action")
					printer print(rs getObject "status" : EbMSMessageStatus)
					printer print(rs getTimestamp "status_time")
					printer println()
					null
				}
				catch
				{
					case e : IOException => throw new SQLException(e)
				}
		}
	}

	override def writeMessageToZip(messageId : String, messageNr : Int, zip : ZipOutputStream)
	{
		jdbcTemplate.queryForObjectAndMap("select content from ebms_message where message_id = ? and message_nr = ?",messageId,messageNr)
		{
			(rs, _) =>
				try
				{
					val entry = new ZipEntry("message.xml")
					zip putNextEntry entry
					zip write(rs getString "content" getBytes)
					zip closeEntry()
					null
				}
				catch
				{
					case e : IOException => throw new SQLException(e)
				}
		}
		writeAttachmentsToZip(messageId, messageNr, zip)
	}

	def writeAttachmentsToZip(messageId : String, messageNr : Int, zip : ZipOutputStream)
	{
		jdbcTemplate.queryAndMap("select name, content_id, content_type, content from ebms_attachment where message_id = ? and message_nr = ?",messageId,messageNr)
		{
			(rs, _) =>
				try
				{
					val entry = new ZipEntry("attachments/" + (if (rs getString "name" isEmpty) rs getString("content_id") + Utils.getFileExtension(rs getString "content_type") else rs getString "name"))
					entry setComment("Content-Type: " + rs.getString("content_type"))
					zip putNextEntry entry
					zip write(rs getBytes "content")
					zip closeEntry()
					null
				}
				catch
				{
					case e : IOException => throw new SQLException(e)
				}
		}
	}
	
	def getMessageFilter(messageFilter : EbMSMessageFilter, parameters : Seq[Object]) : String =
	{
		var result = ""
		if (messageFilter != null)
		{
			if (messageFilter.getCpaId != null)
			{
				parameters add messageFilter.getCpaId
				result += " and cpa_id = ?"
			}
			if (messageFilter.getFromRole != null)
			{
				if (messageFilter.getFromRole.getPartyId != null)
				{
					parameters add messageFilter.getFromRole.getPartyId
					result += " and from_party_id = ?"
				}
				if (messageFilter.getFromRole.getRole != null)
				{
					parameters add messageFilter.getFromRole.getRole
					result += " and from_role = ?"
				}
			}
			if (messageFilter.getToRole != null)
			{
				if (messageFilter.getToRole.getPartyId != null)
				{
					parameters add messageFilter.getToRole.getPartyId
					result += " and to_party_id = ?"
				}
				if (messageFilter.getToRole.getRole != null)
				{
					parameters add messageFilter.getToRole.getRole
					result += " and to_role = ?"
				}
			}
			if (messageFilter.getService != null)
			{
				parameters add messageFilter.getService
				result += " and service = ?"
			}
			if (messageFilter.getAction != null)
			{
				parameters add messageFilter.getAction
				result += " and action = ?"
			}
			if (messageFilter.getConversationId != null)
			{
				parameters add messageFilter.getConversationId
				result += " and conversation_id = ?"
			}
			if (messageFilter.getMessageId != null)
			{
				parameters add messageFilter.getMessageId
				result += " and message_id = ?"
			}
			if (messageFilter.getMessageNr.isDefined)
			{
				parameters add messageFilter.getMessageNr.get.asInstanceOf[Object]
				result += " and message_nr = ?"
			}
			if (messageFilter.getRefToMessageId != null)
			{
				parameters add messageFilter.getRefToMessageId
				result += " and ref_to_message_id = ?"
			}
			if (messageFilter.getStatuses.size > 0)
			{
				result += " and status in (" + messageFilter.getStatuses.map(status => status.id).mkString(",") + ")"
			}
			if (messageFilter.getServiceMessage.isDefined)
			{
				parameters add Constants.EBMS_SERVICE_URI
				result += (if (messageFilter.getServiceMessage.get) " and service = ?" else " and service != ?")
			}
			if (messageFilter.getFrom != null)
			{
				parameters add messageFilter.getFrom
				result += " and time_stamp >= ?"
			}
			if (messageFilter.getTo != null)
			{
				parameters add messageFilter.getTo
				result += " and time_stamp < ?"
			}
		}
		result
	}
	
}