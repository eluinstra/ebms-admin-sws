/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.dao

import java.util.Date
import java.util.zip.ZipOutputStream

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.Constants.TimeUnit
import nl.clockwork.ebms.admin.model.{CPA, EbMSAttachment, EbMSMessage}
import nl.clockwork.ebms.admin.web.message.EbMSMessageFilter
import org.apache.commons.csv.CSVPrinter

trait EbMSDAO
{
	def findCPA(cpaId : String) : Option[CPA]
	def countCPAs : Int
	def selectCPAIds : Seq[String]
	def	selectCPAs(first : Long, count : Long) : Seq[CPA]

	def findMessage(messageId : String) : Option[EbMSMessage]
	def findMessage(messageId : String, messageNr : Int) : Option[EbMSMessage]
	def findResponseMessage(messageId : String) : Option[EbMSMessage]
	def countMessages(filter : EbMSMessageFilter) : Int
	def selectMessages(filter : EbMSMessageFilter, first : Long, count : Long) : Seq[EbMSMessage]

	def findAttachment(messageId : String, messageNr : Int, contentId : String) : Option[EbMSAttachment]

	def selectMessageIds(cpaId : String, fromRole : String, toRole : String, status : EbMSMessageStatus*) : Seq[String]

	def selectMessageTraffic(from : Date, to : Date, timeUnit : TimeUnit, status : EbMSMessageStatus*) : Map[Date,Number]

	def writeMessageToZip(messageId : String, messageNr : Int, stream : ZipOutputStream)
	def printMessagesToCSV(printer : CSVPrinter, filter : EbMSMessageFilter)
}
