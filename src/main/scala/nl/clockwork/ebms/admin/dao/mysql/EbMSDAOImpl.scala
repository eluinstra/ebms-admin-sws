/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.dao.mysql

import java.io.IOException
import java.sql.SQLException
import java.util.Date
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.Constants.TimeUnit
import nl.clockwork.ebms.admin.dao.{AbstractEbMSDAO, JdbcTemplate}
import nl.clockwork.ebms.admin.model.{CPA, EbMSAttachment, EbMSMessage}
import nl.clockwork.ebms.admin.web.Utils
import nl.clockwork.ebms.admin.web.message.EbMSMessageFilter
import org.springframework.transaction.support.TransactionTemplate

class EbMSDAOImpl(transactionTemplate : TransactionTemplate, jdbcTemplate : JdbcTemplate) extends AbstractEbMSDAO(transactionTemplate,jdbcTemplate)
{
	override def selectCPAs(first : Long, count : Long) : Seq[CPA] =
		jdbcTemplate.queryAndMap("select cpa_id, cpa from cpa order by cpa_id limit " + count + " offset " + first)
		{
			(rs, _) => new CPA(rs getString "cpa_id", rs getString "cpa")
		}

	override def selectMessages(filter : EbMSMessageFilter, first : Long, count : Long) : Seq[EbMSMessage] =
	{
		val parameters = Seq[Object]()
		jdbcTemplate.queryAndMap(baseQueryEbMSMessage + " where 1 = 1" + getMessageFilter(filter,parameters) + " order by time_stamp desc limit " + count + " offset " + first,parameters)
		{
			(rs, _) => mapEbMSMessage(rs)
		}
	}

	override def findAttachment(messageId : String, messageNr : Int, contentId : String) : Option[EbMSAttachment] =
		jdbcTemplate.queryForObjectAndMap("select a.name, a.content_id, a.content_type, a.content from ebms_message m, ebms_attachment a" +
			"where m.message_id = ? and m.message_nr = ? and m.id = a.ebms_message_id and a.content_id = ?",messageId,messageNr,contentId)
		{
			(rs, _) => new EbMSAttachment(rs getString "name", rs getString "content_id", rs getString "content_type" , rs getBytes "content")
		}

	override def getAttachments(messageId : String, messageNr : Int) : Seq[EbMSAttachment] =
		jdbcTemplate.queryAndMap("select a.name, a.content_id, a.content_type from ebms_message m, ebms_attachment a" +
			" where m.message_id = ? and m.message_nr = ? and m.id = a.ebms_message_id",messageId,messageNr)
		{
			(rs, _) => new EbMSAttachment(rs getString "name", rs getString "content_id", rs getString "content_type", null)
		}

	override def selectMessageTraffic(from : Date, to : Date, timeUnit : TimeUnit, statuses : EbMSMessageStatus*) : Map[Date,Number] =
	{
		var result = Map[Date,Number]()
		jdbcTemplate.queryAndMap("select str_to_date(date_format(time_stamp,'" + toDateFormat(timeUnit timeUnitDateFormat) + "'),'%Y-%m-%d %k:%i:%s') time, count(*) nr from ebms_message" +
			" where time_stamp >= ? and time_stamp < ?" + (if (statuses.length == 0) " and status is not null" else " and status in (" + statuses.map(s => s.id).mkString(",") + ")") +
			" group by date_format(time_stamp,'" + toDateFormat(timeUnit timeUnitDateFormat) + "')",from,to
		)
		{
			(rs, _) => result += (rs.getTimestamp("time") -> rs.getInt("nr")); null
		}
		result
	}

	override def writeAttachmentsToZip(messageId : String , messageNr : Int, zip : ZipOutputStream)
	{
		jdbcTemplate.queryAndMap("select a.name, a.content_id, a.content_type, a.content from ebms_message m, ebms_attachment a" +
			" where m.message_id = ? and m.message_nr = ? and m.id = a.ebms_message_id",messageId,messageNr)
		{
			(rs, _) =>
				try
				{
					val entry = new ZipEntry("attachments/" + (if (rs getString "name" isEmpty) rs.getString("content_id") + Utils.getFileExtension(rs getString "content_type") else rs getString "name"))
					entry setComment("Content-Type: " + rs.getString("content_type"))
					zip putNextEntry(entry)
					zip write(rs.getBytes("content"))
					zip closeEntry()
					null
				}
				catch
				{
					case e : IOException => throw new SQLException(e)
				}
		}
	}
	
	override def toDateFormat(timeUnitDateFormat : String) : String =
		if ("mm" equals timeUnitDateFormat)
			"%Y-%m-%d %k:%i:00"
		else if ("HH" equals timeUnitDateFormat)
			"%Y-%m-%d %k:00:00"
		else if ("dd" equals timeUnitDateFormat)
			"%Y-%m-%d 00:00:00"
		else if ("MM" equals timeUnitDateFormat)
			"%Y-%m-01 00:00:00"
		else
			timeUnitDateFormat
}
