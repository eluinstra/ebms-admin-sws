/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.dao

import org.springframework.transaction.support.TransactionTemplate
import nl.clockwork.ebms.dao.AbstractDAOFactory.DefaultDAOFactory

import scala.beans.BeanProperty

class EbMSDAOFactory(@BeanProperty var transactionTemplate : TransactionTemplate, @BeanProperty var jdbcTemplate : JdbcTemplate) extends DefaultDAOFactory[EbMSDAO]
{
	def this() = this(null,null)

	override def getObjectType : Class[EbMSDAO] = classOf[nl.clockwork.ebms.admin.dao.EbMSDAO]

	override def createHSqlDbDAO : EbMSDAO = new nl.clockwork.ebms.admin.dao.hsqldb.EbMSDAOImpl(transactionTemplate,jdbcTemplate)

	override def createMySqlDAO : EbMSDAO = new nl.clockwork.ebms.admin.dao.mysql.EbMSDAOImpl(transactionTemplate,jdbcTemplate)

	override def createPostgresDAO : EbMSDAO = new nl.clockwork.ebms.admin.dao.postgresql.EbMSDAOImpl(transactionTemplate,jdbcTemplate)

	override def createOracleDAO : EbMSDAO = new nl.clockwork.ebms.admin.dao.oracle.EbMSDAOImpl(transactionTemplate,jdbcTemplate)

	override def createMsSqlDAO : EbMSDAO = new nl.clockwork.ebms.admin.dao.mssql.EbMSDAOImpl(transactionTemplate,jdbcTemplate)
}
