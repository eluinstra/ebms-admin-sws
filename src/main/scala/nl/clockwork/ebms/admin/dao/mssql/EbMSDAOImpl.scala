/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.dao.mssql

import java.util.Date

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.Constants.TimeUnit
import nl.clockwork.ebms.admin.dao.JdbcTemplate
import nl.clockwork.ebms.admin.model.{CPA, EbMSMessage}
import nl.clockwork.ebms.admin.web.message.EbMSMessageFilter
import org.springframework.transaction.support.TransactionTemplate

class EbMSDAOImpl(transactionTemplate : TransactionTemplate, jdbcTemplate : JdbcTemplate) extends nl.clockwork.ebms.admin.dao.mysql.EbMSDAOImpl(transactionTemplate,jdbcTemplate)
{
	override def selectCPAs(first : Long, count : Long) : Seq[CPA] =
	{
		jdbcTemplate.queryAndMap("select a.* from (select row_number over (order by cpa_id) as rn, cpa_id, cpa from cpa) a where rn between " + first + " and " + (first + count))
		{
			(rs, _) => new CPA(rs getString "cpa_id", rs getString "cpa")
		}
	}

	override def selectMessages(filter : EbMSMessageFilter, first : Long, count : Long) : Seq[EbMSMessage] =
	{
		val parameters = Seq[Object]()
		jdbcTemplate.queryAndMap(baseQueryEbMSMessage.replace("select","select row_number over (order by time_stamp desc) as rn,") + " where 1 = 1" + getMessageFilter(filter,parameters) + ") a where rn between " + first + " and " + (first + count))
		{
			(rs, _) => mapEbMSMessage(rs)
		}
	}

	override def selectMessageTraffic(from : Date, to : Date, timeUnit : TimeUnit, statuses : EbMSMessageStatus*) : Map[Date,Number] =
	{
		var result = Map[Date,Number]()
		jdbcTemplate.queryAndMap("select cast(" + toDateFormat(timeUnit timeUnitDateFormat) + "as datetime) time, count(*) nr from ebms_message" +
			" where time_stamp >= ? and time_stamp < ?" + (if (statuses.length == 0) " and status is not null" else " and status in (" + statuses.map(s => s.id).mkString(",") + ")") +
			" group by " + toDateFormat(timeUnit timeUnitDateFormat),from,to)
		{
			(rs, _) => result += (rs.getTimestamp("time") -> rs.getInt("nr")); null

		}
		result
	}

	override def toDateFormat(timeUnitDateFormat : String) : String =
		if ("mm".equals(timeUnitDateFormat))
			"cast(datepart(yyyy,time_stamp) as varchar(4)) + '-' + cast(datepart(mm,time_stamp) as varchar(2)) + '-' + cast(datepart(dd,time_stamp) as varchar(2)) + ' ' + cast(datepart(hh,time_stamp) as varchar(2)) + ':' + cast(datepart(mi,time_stamp) as varchar(2)) + ':' + '00'"
		else if ("HH".equals(timeUnitDateFormat))
			"cast(datepart(yyyy,time_stamp) as varchar(4)) + '-' + cast(datepart(mm,time_stamp) as varchar(2)) + '-' + cast(datepart(dd,time_stamp) as varchar(2)) + ' ' + cast(datepart(hh,time_stamp) as varchar(2)) + ':' + '00' + ':' + '00'"
		else if ("dd".equals(timeUnitDateFormat))
			"cast(datepart(yyyy,time_stamp) as varchar(4)) + '-' + cast(datepart(mm,time_stamp) as varchar(2)) + '-' + cast(datepart(dd,time_stamp) as varchar(2)) + ' ' + '00' + ':' + '00' + ':' + '00'"
		else if ("MM".equals(timeUnitDateFormat))
			"cast(datepart(yyyy,time_stamp) as varchar(4)) + '-' + cast(datepart(mm,time_stamp) as varchar(2)) + '-' + '01' + ' ' + '00' + ':' + '00' + ':' + '00'"
		else
			timeUnitDateFormat

}
