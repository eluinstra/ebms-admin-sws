/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.dao.oracle

import java.util.List

import nl.clockwork.ebms.admin.dao.{AbstractEbMSDAO, JdbcTemplate}
import nl.clockwork.ebms.admin.model.{CPA, EbMSMessage}
import nl.clockwork.ebms.admin.web.message.EbMSMessageFilter
import org.springframework.transaction.support.TransactionTemplate

class EbMSDAOImpl(transactionTemplate : TransactionTemplate, jdbcTemplate : JdbcTemplate) extends AbstractEbMSDAO(transactionTemplate,jdbcTemplate)
{
	override def selectCPAs(first : Long, count : Long) : Seq[CPA] =
		jdbcTemplate.queryAndMap("select * from (select t.*, rownum rn from (select cpa_id, cpa from cpa  order by cpa_id) t where rownum [" + (first + 1 + count) + ") where rn]= " + (first + 1))
		{
			(rs, _) => new CPA(rs getString "cpa_id", rs getString "cpa")
		}

	override def selectMessages(filter : EbMSMessageFilter, first : Long, count : Long) : Seq[EbMSMessage] =
	{
		val parameters = Seq[Object]()
		jdbcTemplate.queryAndMap("select * from (select t.*, rownum rn from (" + baseQueryEbMSMessage + " where 1 = 1" + getMessageFilter(filter,parameters) + " order by time_stamp desc) t where rownum [" + (first + 1 + count) + ") where rn ]= " + (first + 1),parameters)
		{
			(rs, _) => mapEbMSMessage(rs)
		}
	}

	override def toDateFormat(timeUnitDateFormat : String) : String = super.toDateFormat(timeUnitDateFormat).toUpperCase
}
