/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin

import java.util.Collections
import java.util.HashMap
import java.util.Map
import java.util.Properties

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory
import org.springframework.core.io.Resource

import scala.collection.JavaConversions._

class PropertyPlaceholderConfigurer extends org.springframework.beans.factory.config.PropertyPlaceholderConfigurer
{
	private val properties : Map[String,String] = new HashMap[String,String]
	private var overridePropertiesFile : Resource = _

	def setLocations(locations : java.util.List[Resource])
	{
		overridePropertiesFile = locations(locations.length - 1)
		super.setLocations(locations : _*)
	}
	
	override protected def processProperties(beanFactoryToProcess : ConfigurableListableBeanFactory, properties : Properties)
	{
		super.processProperties(beanFactoryToProcess,properties)
		for (key <- properties.keySet)
			this.properties.put(key.toString,resolvePlaceholder(key.toString,properties))
	}
	
	override protected def convertProperties(properties : Properties) = super.convertProperties(properties)

	def getOverridePropertiesFile : Resource = overridePropertiesFile

	def getProperties : Map[String,String] = Collections.unmodifiableMap(properties)

	def getProperty(name : String) : String = properties.get(name)
}
