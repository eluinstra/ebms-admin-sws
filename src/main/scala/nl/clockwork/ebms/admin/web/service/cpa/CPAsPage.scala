/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.cpa

import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.OddOrEvenIndexStringModel
import nl.clockwork.ebms.admin.web.PageClassLink
import nl.clockwork.ebms.admin.web.WebMarkupContainer
import nl.clockwork.ebms.service.CPAService

import org.apache.commons.logging.LogFactory
import org.apache.wicket.AttributeModifier
import org.apache.wicket.Component
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.markup.html.form.AjaxButton
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.repeater.Item
import org.apache.wicket.markup.repeater.data.DataView
import org.apache.wicket.markup.repeater.data.IDataProvider
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean

class CPAsPage extends BasePage
{
	private class CPAIdsDataView(val id : String, val dataProvider : IDataProvider[String],components : Component*) extends DataView[String](id,dataProvider)
	{
		setOutputMarkupId(true)

		override protected def populateItem(item : Item[String])
		{
			val cpaId = item.getModelObject
			item add(createViewLink("view",cpaId))
			val urlModalWindow = new UrlModalWindow("urlModalWindow",cpaService,cpaId,components)
			item add(urlModalWindow)
			item add(createEditUrl("editUrl",urlModalWindow))
			item add(new DownloadCPALink("downloadCPA",cpaService,cpaId))
			item add(createDeleteButton("delete"))
			item add(AttributeModifier.replace("class",new OddOrEvenIndexStringModel(item.getIndex)))
		}

		private def createViewLink(id : String, cpaId : String) : Link[Void] = new Link[Void](id)
		{
			override def onClick = setResponsePage(new CPAPage(cpaService.getCPA(cpaId),CPAsPage.this))
		}
		.add(new Label("cpaId",cpaId)).asInstanceOf[Link[Void]]

		private def createEditUrl(id : String, urlModalWindow : ModalWindow) : AjaxButton = new AjaxButton(id)
		{
			override protected def onSubmit(target : AjaxRequestTarget, form : Form[_]) : Unit = urlModalWindow.show(target)
		}

		private def createDeleteButton(id : String) : Button = new Button(id,new ResourceModel("cmd.delete"))
		{
			override def onSubmit
			{
				try
				{
					val cpaId = getParent.getDefaultModelObject.asInstanceOf[String]
					cpaService deleteCPA cpaId
					setResponsePage(new CPAsPage)
				}
				catch
				{
				  case e : Exception =>	logger error("",e);	error(e getMessage)
				}
			}
		}
    add(AttributeModifier replace("onclick","confirm('" + getLocalizer.getString("confirm",this) + "')"))
	}

	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="cpaService") var cpaService : CPAService = _

	add(new BootstrapFeedbackPanel("feedback"))
	add(new EditCPAsForm("form"))

	class EditCPAsForm(id : String) extends Form[Void](id)
	{
		val container = new WebMarkupContainer("container")
		add(container)
		container add(new CPAIdsDataView("cpaIds",CPADataProvider(cpaService),EditCPAsForm.this))
    add(new PageClassLink("new",classOf[CPAUploadPage]))
	}
	
	override def getPageTitle : String = getLocalizer getString("cpas",this)
}
object CPAsPage
{
  def apply = new CPAsPage
}
