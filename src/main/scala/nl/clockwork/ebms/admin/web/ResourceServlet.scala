/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web


import javax.servlet.GenericServlet
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.apache.wicket.util.io.IOUtils

class ResourceServlet extends GenericServlet
{
	override def init = super.init

	override def service(request : ServletRequest, response : ServletResponse)
	{
		val input = this.getClass.getResourceAsStream(request.asInstanceOf[HttpServletRequest].getServletPath + request.asInstanceOf[HttpServletRequest].getPathInfo)
		if (input == null)
			response.asInstanceOf[HttpServletResponse].setStatus(204)
		else
		{
			response.asInstanceOf[HttpServletResponse].setStatus(200)
			response.asInstanceOf[HttpServletResponse].setContentType(getContentType(request.asInstanceOf[HttpServletRequest].getPathInfo))
			IOUtils.copy(input,response.getOutputStream)
		}
	}

	private def getContentType(pathInfo : String) : String =
	{
		pathInfo match
		{
			case s if s.endsWith(".css") => "text/css"
			case s if s.endsWith(".js") => "text/javascript"
			case s if s.endsWith(".gif") => "image/gif"
			case s if s.endsWith(".eot") => "application/vnd.ms-fontobject"
			case s if s.endsWith(".svg") => "image/svg+xml"
			case s if s.endsWith(".ttf") => "font/ttf"
			case s if s.endsWith(".woff") => "application/font-woff"
		}
	}
}
