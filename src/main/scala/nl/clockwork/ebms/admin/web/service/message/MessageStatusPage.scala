/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.util.ArrayList
import java.util.List
import javax.xml.bind.JAXBException

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.CPAUtils
import nl.clockwork.ebms.admin.Utils
import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import nl.clockwork.ebms.admin.web.ResetButton
import nl.clockwork.ebms.common.XMLMessageBuilder
import nl.clockwork.ebms.model.Party
import nl.clockwork.ebms.service.CPAService
import nl.clockwork.ebms.service.EbMSMessageService
import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.html.form.{Button, CheckBox, DropDownChoice, TextField, _}
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.apache.wicket.util.io.IClusterable
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.CollaborationProtocolAgreement

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class MessageStatusPage extends BasePage
{
	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="ebMSAdminDAO") var ebMSDAO : EbMSDAO = _
	@SpringBean(name="cpaService") var cpaService : CPAService = _
	@SpringBean(name="ebMSMessageService") var ebMSMessageService : EbMSMessageService = _
	@SpringBean(name="cleoPatch") var cleoPatch : Boolean = _

	add(new BootstrapFeedbackPanel("feedback") setOutputMarkupId true )
	add(new MessageStatusForm("form"))

	override def getPageTitle : String = getLocalizer.getString("messageStatus",this)

	class MessageStatusForm(id : String) extends Form[MessageStatusFormModel](id,new CompoundPropertyModel[MessageStatusFormModel](new MessageStatusFormModel))
	{
		add(new BootstrapFormComponentFeedbackBorder("cpaIdFeedback",createCPAIdChoice("cpaId")))
		add(new BootstrapFormComponentFeedbackBorder("fromPartyIdFeedback",createFromPartyIdChoice("fromPartyId")) setVisible cleoPatch)
		add(new BootstrapFormComponentFeedbackBorder("fromRoleFeedback",createFromRoleChoice("fromRole")))
		add(new BootstrapFormComponentFeedbackBorder("toPartyIdFeedback",createToPartyIdChoice("toPartyId")) setVisible cleoPatch)
		add(new BootstrapFormComponentFeedbackBorder("toRoleFeedback",createToRoleChoice("toRole")))
		val messageIds = createMessageIdsChoice("messageIds")
		add(new BootstrapFormComponentFeedbackBorder("messageIdFeedback",messageIds,createMessageIdField("messageId")))
		add(createManualCheckBox("manual",messageIds))
		val check = createCheckButton("check")
		setDefaultButton(check)
		add(check)
		add(new ResetButton("reset",new ResourceModel("cmd.reset"),classOf[MessageStatusPage]))

		private def createCPAIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,Model ofList Utils.toList(cpaService.getCPAIds toList))
			{
				override def isEnabled : Boolean = !MessageStatusForm.this.getModelObject.manual
			}
			.setLabel(new ResourceModel("lbl.cpaId"))
			//.setRequired(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageStatusForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]).handle(cpaService getCPA model.cpaId)
						model resetFromPartyIds(CPAUtils getPartyIds cpa)
						model resetFromRoles(CPAUtils getRoleNames cpa)
						model.resetToPartyIds
						model.resetToRoles
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger.error("",e); error(e.getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createFromPartyIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"fromPartyIds"))
			{
				override def isEnabled : Boolean = !MessageStatusForm.this.getModelObject.manual

				override def isRequired : Boolean = !MessageStatusForm.this.getModelObject.manual
			}
			.setLabel(new ResourceModel("lbl.fromPartyId"))
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageStatusForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]).handle(cpaService getCPA model.cpaId)
						model resetFromRoles(CPAUtils getRoleNames(cpa,model fromPartyId))
						model resetToPartyIds(CPAUtils getOtherPartyIds(cpa,model fromPartyId))
						if (model.fromRole != null)
							model resetToRoles(CPAUtils.getRoleNames(cpa,model toPartyId))
						model resetMessageIds(ebMSDAO selectMessageIds(model cpaId,model fromRole,model toRole,EbMSMessageStatus SENT,EbMSMessageStatus EXPIRED))
						if (model.getMessageIds.size == 0)
							info("No messages found")
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createFromRoleChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"fromRoles"))
			{
				override def isEnabled : Boolean = !MessageStatusForm.this.getModelObject.manual
			}
			.setLabel(new ResourceModel("lbl.fromRole"))
			.setRequired(false)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageStatusForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]).handle(cpaService getCPA model.cpaId)
						model resetFromPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model fromRole))
						model resetToPartyIds(CPAUtils getOtherPartyIds(cpa,model fromPartyId))
						model resetToRoles(CPAUtils.getRoleNames(cpa,model toPartyId))
						model resetMessageIds(ebMSDAO selectMessageIds(model cpaId,model fromRole,model toRole,EbMSMessageStatus SENT,EbMSMessageStatus EXPIRED))
						if (model.getMessageIds.size == 0)
							info("No messages found")
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
  		.asInstanceOf[DropDownChoice[String]]

		private def createToPartyIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"toPartyIds"))
			{
				override def isEnabled : Boolean = !MessageStatusForm.this.getModelObject.manual

				override def isRequired : Boolean = !MessageStatusForm.this.getModelObject.manual
			}
			.setLabel(new ResourceModel("lbl.toPartyId"))
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageStatusForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]).handle(cpaService getCPA model.cpaId)
						model resetToRoles(CPAUtils getRoleNames(cpa,model toPartyId))
						model resetMessageIds(ebMSDAO selectMessageIds(model cpaId,model fromRole,model toRole,EbMSMessageStatus SENT,EbMSMessageStatus EXPIRED))
						if (model.getMessageIds.size == 0)
							info("No messages found")
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createToRoleChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"toRoles"))
			{
				override def isEnabled : Boolean = !MessageStatusForm.this.getModelObject.manual
			}
			.setLabel(new ResourceModel("lbl.toRole"))
			.setRequired(false)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageStatusForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]).handle(cpaService getCPA model.cpaId)
						if (model.toPartyId == null)
							model resetToPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model toRole))
						model resetMessageIds(ebMSDAO selectMessageIds(model cpaId,model fromRole,model toRole,EbMSMessageStatus SENT,EbMSMessageStatus EXPIRED))
						if (model.getMessageIds.size == 0)
							info("No messages found")
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
			.asInstanceOf[DropDownChoice[String]]

		private def createMessageIdsChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[String](getModelObject,"messageId"),new PropertyModel[List[String]](getModelObject,"messageIds"))
			{
				override def isVisible : Boolean = !MessageStatusForm.this.getModelObject.manual
			}
			.setLabel(new ResourceModel("lbl.messageId"))
			.setRequired(true)
			.setOutputMarkupPlaceholderTag(true)
		  .asInstanceOf[DropDownChoice[String]]

		private def createMessageIdField(id : String) : TextField[String] =
			new TextField[String](id)
			{
				override def isVisible : Boolean = MessageStatusForm.this.getModelObject.manual
			}
			.setLabel(new ResourceModel("lbl.messageId"))
			.setRequired(true).setOutputMarkupPlaceholderTag(true)
		  .asInstanceOf[TextField[String]]

		private def createManualCheckBox(id : String, messageIds : DropDownChoice[String]) : CheckBox =
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.manual"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					if (messageIds.isVisible)
					{
						val model = MessageStatusForm.this.getModelObject
						model.resetMessageIds(ebMSDAO selectMessageIds(model cpaId,model fromRole,model toRole,EbMSMessageStatus SENT,EbMSMessageStatus EXPIRED))
						if (model.getMessageIds.size == 0)
							info("No messages found")
					}
					target add(getPage get "feedback")
					target add(getPage get "form")
				}
      })
			.asInstanceOf[CheckBox]

		private def createCheckButton(id : String) : Button =
			new Button(id,new ResourceModel("cmd.check"))
			{
				override def onSubmit
				{
					try
					{
						val model = MessageStatusForm.this.getModelObject
						val messageStatus =
							if (!model.manual || (model.cpaId != null && model.fromPartyId != null && model.toPartyId != null))
								ebMSMessageService getMessageStatus(model cpaId,new Party(model fromPartyId,model fromRole),new Party(model toPartyId,model toRole),model messageId)
							else
								ebMSMessageService getMessageStatus(model messageId)
						info(new StringResourceModel("getMessageStatus.ok",Model of messageStatus.getStatus) getString)
					}
					catch
					{
						case e : Exception => logger error("",e); error(e getMessage)
					}
				}
			}

	}

	class MessageStatusFormModel(
		@BeanProperty var cpaId : String,
		@BeanProperty val fromPartyIds : List[String] = new ArrayList[String],
		@BeanProperty var fromPartyId : String,
		@BeanProperty val fromRoles : List[String] = new ArrayList[String],
		@BeanProperty var fromRole : String,
		@BeanProperty val toPartyIds : List[String] = new ArrayList[String],
		@BeanProperty var toPartyId : String,
		@BeanProperty val toRoles : List[String] = new ArrayList[String],
		@BeanProperty var toRole : String,
		@BeanProperty val messageIds : List[String] = new ArrayList[String],
		@BeanProperty var messageId : String,
		@BeanProperty val manual : Boolean = true) extends IClusterable
	{
		def this() = this(null,new ArrayList[String],null,new ArrayList[String],null,new ArrayList[String],null,new ArrayList[String],null,new ArrayList[String],null,true)

		def resetFromPartyIds
		{
			fromPartyIds.clear
			fromPartyId = null
		}
		def resetFromPartyIds(partyIds : List[String])
		{
			resetFromPartyIds
			fromPartyIds addAll partyIds
			fromPartyId = if (fromPartyIds.size == 1) fromPartyIds head else null
		}
		def resetFromRoles
		{
			fromRoles.clear
			fromRole = null
		}
		def resetFromRoles(roleNames : List[String])
		{
			resetFromRoles
			fromRoles addAll roleNames
		}
		def resetToPartyIds
		{
			toPartyIds.clear
			toPartyId = null
		}
		def resetToPartyIds(partyIds : List[String])
		{
			resetToPartyIds
			toPartyIds addAll partyIds
			toPartyId = if (toPartyIds.size == 1) toPartyIds head else null
		}
		def resetToRoles
		{
			getToRoles.clear
			toRole = null
		}
		def resetToRoles(roleNames : List[String])
		{
			resetToRoles
			toRoles addAll roleNames
			toRole = if (fromRole != null && toRoles.size == 1) toRoles head else null
		}
		def resetMessageId = setMessageId(null)

		def resetMessageIds(messageIds : List[String])
		{
			messageIds.clear
			messageIds addAll messageIds
			messageId = null
		}
	}
}
