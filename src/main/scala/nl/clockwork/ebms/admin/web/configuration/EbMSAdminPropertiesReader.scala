/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.io.Reader
import java.util.Properties

import nl.clockwork.ebms.admin.web.configuration.PropertiesType._

class EbMSAdminPropertiesReader(override val reader : Reader) extends EbMSCorePropertiesReader(reader)
{
	def read(ebMSAdminProperties : EbMSAdminPropertiesFormModel, propertiesType : PropertiesType)
	{
		val properties = new Properties
		propertiesType match
		{
			case EBMS_ADMIN =>
				properties.load(reader)
				read(properties,ebMSAdminProperties.consoleProperties)
				read(properties,ebMSAdminProperties.coreProperties)
				read(properties,ebMSAdminProperties.serviceProperties)
				read(properties,ebMSAdminProperties.jdbcProperties)
			case EBMS_ADMIN_EMBEDDED =>
				properties.load(reader)
				read(properties,ebMSAdminProperties.consoleProperties)
				read(properties,ebMSAdminProperties.coreProperties)
				read(properties,ebMSAdminProperties.httpProperties)
				read(properties,ebMSAdminProperties.signatureProperties)
				read(properties,ebMSAdminProperties.jdbcProperties)
			case EBMS_CORE =>
				read(ebMSAdminProperties)
		}
	}
	
	protected def read(properties : Properties, consoleProperties : ConsolePropertiesFormModel) = consoleProperties.setMaxItemsPerPage(Integer.parseInt(properties.getProperty("maxItemsPerPage")))

	protected def read(properties : Properties, serviceProperties : ServicePropertiesFormModel ) = serviceProperties.setUrl(properties.getProperty("service.ebms.url"))
}
