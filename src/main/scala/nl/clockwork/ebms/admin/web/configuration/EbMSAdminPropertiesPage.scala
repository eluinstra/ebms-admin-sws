/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.io.{File, FileNotFoundException, FileReader}
import java.util.ArrayList
import java.util.List

import nl.clockwork.ebms.admin.PropertyPlaceholderConfigurer
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.BootstrapPanelBorder
import nl.clockwork.ebms.admin.web.ResetButton
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.{Button, Form}
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.ListView
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean

import scala.beans.BeanProperty

class EbMSAdminPropertiesPage(var ebMSAdminPropertiesFormModel : EbMSAdminPropertiesFormModel) extends BasePage
{
	private class ComponentsListView(val id : String, val list : List[_ <: BootstrapPanelBorder]) extends ListView[BootstrapPanelBorder](id,list)
	{
		setReuseItems(true)

		override protected def populateItem(item : ListItem[BootstrapPanelBorder])
		{
			item.add(item.getModelObject)
		}
	}

	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="propertyConfigurer") var propertyPlaceholderConfigurer : PropertyPlaceholderConfigurer = _
	private val propertiesType : PropertiesType = PropertiesType.getPropertiesType(propertyPlaceholderConfigurer.getOverridePropertiesFile.getFilename)
	add(new BootstrapFeedbackPanel("feedback"))
	if (ebMSAdminPropertiesFormModel == null)
	{
		ebMSAdminPropertiesFormModel = new EbMSAdminPropertiesFormModel
		try
		{
			val file = new File(propertiesType.filename)
			val reader = new FileReader(file)
			new EbMSAdminPropertiesReader(reader).read(ebMSAdminPropertiesFormModel,propertiesType)
			this.info(new StringResourceModel("properties.loaded",this,Model.of(file)).getString)
		}
		catch
		{
			case e : FileNotFoundException => logger error("",e); error(e getMessage)
		}
	}
	add(new EbMSAdminPropertiesForm("form",ebMSAdminPropertiesFormModel))

	def this() = this(null)

	override def getPageTitle : String = getLocalizer.getString("ebMSAdminProperties",this)

	class EbMSAdminPropertiesForm(val id : String, val model : EbMSAdminPropertiesFormModel) extends Form[EbMSAdminPropertiesFormModel](id,new CompoundPropertyModel[EbMSAdminPropertiesFormModel](model))
	{
		val components = new ArrayList[BootstrapPanelBorder]
		components.add(new BootstrapPanelBorder("panelBorder",EbMSAdminPropertiesPage.this.getString("consoleProperties"),new ConsolePropertiesFormPanel("component",new PropertyModel[ConsolePropertiesFormModel](getModelObject,"consoleProperties"))))
		components.add(new BootstrapPanelBorder("panelBorder",EbMSAdminPropertiesPage.this.getString("coreProperties"),new CorePropertiesFormPanel("component",new PropertyModel[CorePropertiesFormModel](getModelObject,"coreProperties"))))
		if (PropertiesType.EBMS_ADMIN.equals(propertiesType))
			components.add(new BootstrapPanelBorder("panelBorder",EbMSAdminPropertiesPage.this.getString("serviceProperties"),new ServicePropertiesFormPanel("component",new PropertyModel[ServicePropertiesFormModel](getModelObject,"serviceProperties"))))
		if (PropertiesType.EBMS_ADMIN_EMBEDDED.equals(propertiesType))
		{
			components.add(new BootstrapPanelBorder("panelBorder",EbMSAdminPropertiesPage.this.getString("httpProperties"),new HttpPropertiesFormPanel("component",new PropertyModel[HttpPropertiesFormModel](getModelObject,"httpProperties"))))
			components.add(new BootstrapPanelBorder("panelBorder",EbMSAdminPropertiesPage.this.getString("signatureProperties"),new SignaturePropertiesFormPanel("component",new PropertyModel[SignaturePropertiesFormModel](getModelObject,"signatureProperties"))))
		}
		components.add(new BootstrapPanelBorder("panelBorder",EbMSAdminPropertiesPage.this.getString("jdbcProperties"),new JdbcPropertiesFormPanel("component",new PropertyModel[JdbcPropertiesFormModel](getModelObject,"jdbcProperties"))))
		add(new ComponentsListView("components",components))
		add(createValidateButton("validate"))
		add(new DownloadEbMSAdminPropertiesButton("download",new ResourceModel("cmd.download"),getModelObject,propertiesType))
		add(new SaveEbMSAdminPropertiesButton("save",new ResourceModel("cmd.save"),getModelObject,propertiesType))
		add(new ResetButton("reset",new ResourceModel("cmd.reset"),classOf[EbMSAdminPropertiesPage]))

		private def createValidateButton(id : String) : Button = new Button(id)
		{
			override def onSubmit = info(EbMSAdminPropertiesPage.this.getString("validate.ok"))
		}
	}

}
class EbMSAdminPropertiesFormModel(
	@BeanProperty val consoleProperties : ConsolePropertiesFormModel = new ConsolePropertiesFormModel,
	@BeanProperty override val coreProperties : CorePropertiesFormModel = new CorePropertiesFormModel,
	@BeanProperty val serviceProperties : ServicePropertiesFormModel = new ServicePropertiesFormModel) extends EbMSCorePropertiesFormModel
