/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.markup.html.AjaxLink
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
import org.apache.wicket.markup.html.basic.MultiLineLabel
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.IModel

class ErrorMessagePanel(val window : ModalWindow, val model : IModel[String]) extends Panel(window.getContentId,model)
{
	add(new MultiLineLabel("errorMessage",model))
	add(createClose("close",window))

	private def createClose(id : String, window : ModalWindow) : AjaxLink[Void] =
		new AjaxLink[Void](id)
		{
			override def onClick(target : AjaxRequestTarget) = window.close(target)
		}

}
