/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.util.ArrayList
import java.util.List
import javax.xml.bind.JAXBException

import nl.clockwork.ebms.admin.CPAUtils
import nl.clockwork.ebms.admin.Utils
import nl.clockwork.ebms.admin.web.{BootstrapFormComponentFeedbackBorder, _}
import nl.clockwork.ebms.common.XMLMessageBuilder
import nl.clockwork.ebms.model.{EbMSMessageContent, EbMSMessageContext, Role}
import nl.clockwork.ebms.service.CPAService
import nl.clockwork.ebms.service.EbMSMessageService
import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.html.form.{CheckBox, DropDownChoice, _}
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.CollaborationProtocolAgreement

import scala.beans.BeanProperty

import scala.collection.JavaConversions._

class SendMessagePageX extends BasePage
{
	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="cpaService") var cpaService : CPAService = _
	@SpringBean(name="ebMSMessageService") var ebMSMessageService : EbMSMessageService = _

	add(new BootstrapFeedbackPanel("feedback").setOutputMarkupId(true))
	add(new MessageForm("form"))

	override def getPageTitle : String = getLocalizer.getString("messageSend",this)

	class MessageForm(id : String) extends Form[EbMSMessageContextModel](id,new CompoundPropertyModel[EbMSMessageContextModel](new EbMSMessageContextModel))
	{
		var dataSources : DataSourcesPanel = _

		setMultiPart(true)
		add(new BootstrapFormComponentFeedbackBorder("cpaIdFeedback",createCPAIdChoice("cpaId")))
		add(new BootstrapFormComponentFeedbackBorder("fromPartyIdFeedback",createFromPartyIdChoice("fromRole.partyId")))
		add(new BootstrapFormComponentFeedbackBorder("fromRoleFeedback",createFromRoleChoice("fromRole.role")))
		add(createToPartyIdFeedbackBorder("toPartyIdFeedback",createToPartyIdChoice("toRole.partyId")))
		add(createToRoleFeedbackBorder("toRoleFeedback",createToRoleChoice("toRole.role")))
		add(new BootstrapFormComponentFeedbackBorder("serviceFeedback",createServiceChoice("service")))
		add(new BootstrapFormComponentFeedbackBorder("actionFeedback",createActionChoice("action")))
		add(new TextField[String]("conversationId").setLabel(new ResourceModel("lbl.conversationId")))
		add(new TextField[String]("messageId").setLabel(new ResourceModel("lbl.messageId")))
		add(new TextField[String]("refToMessageId").setLabel(new ResourceModel("lbl.refToMessageId")))
		val rawInputContainer = createRawInputContainer
		add(rawInputContainer)
		rawInputContainer.add(createRawInputCheckBox("rawInput"))
		dataSources = new EmptyDataSourcesPanel("dataSources")
		add(dataSources)
		add(createExtendedCheckBox("extended"))
		val send = createSendButton("send")
		setDefaultButton(send)
		add(send)
		add(new ResetButton("reset",new ResourceModel("cmd.reset"),classOf[SendMessagePageX]))

		private def createCPAIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,Model ofList Utils.toList(cpaService.getCPAIds toList))
			.setLabel(new ResourceModel("lbl.cpaId"))
			.setRequired(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetFromPartyIds(CPAUtils getPartyIds cpa)
						model resetFromRoles(CPAUtils getRoleNames cpa)
						if (model extended)
						{
							model.resetToPartyIds
							model.resetToRoles
						}
						model.resetServices
						model.resetActions
						val dataSourcesNew = new EmptyDataSourcesPanel(dataSources getId)
						dataSources replaceWith dataSourcesNew
						dataSources = dataSourcesNew
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger.error("",e); error(e getMessage)
					}
				}
			})
			.asInstanceOf[DropDownChoice[String]]

		private def createFromPartyIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"fromPartyIds"))
			.setLabel(new ResourceModel("lbl.fromPartyId"))
			.setRequired(false).setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetFromRoles(CPAUtils getRoleNames(cpa,model.getFromRole getPartyId))
						if (model extended)
						{
							if (model.getFromRole.getRole != null)
								model resetToRoles(CPAUtils getOtherRoleNames(cpa,model.getFromRole getPartyId,model.getFromRole getRole))
							model.resetToPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model.getToRole getRole))
						}
						model resetServices(CPAUtils getServiceNames(cpa,model.getFromRole getRole))
						model.resetActions
						val dataSourcesNew = new EmptyDataSourcesPanel(dataSources getId)
						dataSources replaceWith dataSourcesNew
						dataSources = dataSourcesNew
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createFromRoleChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"fromRoles"))
			.setLabel(new ResourceModel("lbl.fromRole"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetFromPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model.getFromRole getRole))
						if (model extended)
						{
							model resetToRoles(CPAUtils getOtherRoleNames(cpa,model.getFromRole getPartyId,model.getFromRole getRole))
							model resetToPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model.getToRole getRole))
						}
						model resetServices(CPAUtils getServiceNames(cpa,model.getFromRole getRole))
						model.resetActions
						val dataSourcesNew = new EmptyDataSourcesPanel(dataSources getId)
						dataSources replaceWith dataSourcesNew
						dataSources = dataSourcesNew
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createToPartyIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"toPartyIds"))
			.setLabel(new ResourceModel("lbl.toPartyId"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						if (model extended)
							if (model.getToRole.getRole == null)
								model resetToRoles(CPAUtils getRoleNames(cpa,model.getToRole getPartyId))
						model resetServices(CPAUtils getServiceNames(cpa,model.getFromRole getRole))
						model.resetActions
						val dataSourcesNew = new EmptyDataSourcesPanel(dataSources getId)
						dataSources replaceWith dataSourcesNew
						dataSources = dataSourcesNew
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createToPartyIdFeedbackBorder(id : String, toPartyIdChoice : DropDownChoice[String]) : BootstrapFormComponentFeedbackBorder =
			new BootstrapFormComponentFeedbackBorder(id,toPartyIdChoice)
			{
				override def isVisible : Boolean = MessageForm.this.getModelObject.extended
			}

		private def createToRoleChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"toRoles"))
			.setLabel(new ResourceModel("lbl.toRole"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						if (model extended)
//							if (model.getToRole.getPartyId == null)
								model resetToPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model.getToRole getRole))
						model resetServices(CPAUtils getServiceNames(cpa,model.getFromRole getRole))
						model.resetActions
						val dataSourcesNew = new EmptyDataSourcesPanel(dataSources getId)
						dataSources replaceWith dataSourcesNew
						dataSources = dataSourcesNew
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createToRoleFeedbackBorder(id : String, toRoleChoice : DropDownChoice[String]) : BootstrapFormComponentFeedbackBorder =
			new BootstrapFormComponentFeedbackBorder(id,toRoleChoice)
			{
				override def isVisible : Boolean = MessageForm.this.getModelObject.extended
			}

		private def createServiceChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"services"))
			.setLabel(new ResourceModel("lbl.service"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model.resetActions(CPAUtils getFromActionNames(cpa,model.getFromRole getRole,model getService))
						val dataSourcesNew = new EmptyDataSourcesPanel(dataSources getId)
						dataSources replaceWith dataSourcesNew
						dataSources = dataSourcesNew
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createActionChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"actions"))
			.setLabel(new ResourceModel("lbl.action"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					val model = MessageForm.this.getModelObject
					val dataSourcesNew =
						if (WicketApplication.get.getMessageEditPanels containsKey(MessageProvider createId(model getService,model getAction)))
							WicketApplication.get.getMessageEditPanels get(MessageProvider createId(model getService,model getAction)) getPanel(dataSources getId)
						else
							new DefaultDataSourcesPanel(dataSources getId)
					dataSources replaceWith dataSourcesNew
					dataSources = dataSourcesNew
					model setRawInput false
					target add(getPage get "feedback")
					target add(getPage get "form")
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createRawInputContainer : WebMarkupContainer =
			new WebMarkupContainer("rawInputContainer")
			{
				override def isVisible : Boolean =
				{
					val model = MessageForm.this.getModelObject
					model.getAction != null && (WicketApplication.get.getMessageEditPanels.containsKey(MessageProvider.createId(model.getService,model.getAction))) || (dataSources != null && !(dataSources.isInstanceOf[EmptyDataSourcesPanel] || dataSources.isInstanceOf[DefaultDataSourcesPanel]))
				}
			}

		private def createRawInputCheckBox(id : String) : CheckBox =
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.rawInput"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					val model = MessageForm.this.getModelObject
					val dataSourcesNew =
						if (model getRawInput)
							new DefaultDataSourcesPanel(dataSources getId)
						else
							WicketApplication.get.getMessageEditPanels get(MessageProvider createId(model getService,model getAction)) getPanel(dataSources getId)
					dataSources replaceWith dataSourcesNew
					target add(getPage get "feedback")
					target add(getPage get "form")
				}
			})
		  .asInstanceOf[CheckBox]

		private def createExtendedCheckBox(id : String) : CheckBox =
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.extended"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						if (model extended)
						{
							model setToRole new Role
							if (model.getCpaId != null)
							{
								val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
								if (model.getFromRole.getRole != null)
									model resetToRoles(CPAUtils getOtherRoleNames(cpa,model.getFromRole getPartyId,model.getFromRole getRole))
								model resetToPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model.getToRole getRole))
							}
						}
						else
							model setToRole null
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e.getMessage)
					}
				}
      })
		  .asInstanceOf[CheckBox]

		private def createSendButton(id : String) : Button =
			new Button(id,new ResourceModel("cmd.send"))
			{
				override def onSubmit
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val messageContent = new EbMSMessageContent(model,dataSources getDataSources)
						val messageId = ebMSMessageService.sendMessage(messageContent)
						info(new StringResourceModel("sendMessage.ok",Model of messageId) getString)
					}
					catch
					{
						case e : Exception => logger error("",e); error(e getMessage)
					}
				}
			}

	}

	class EbMSMessageContextModel(
		@BeanProperty val fromPartyIds : List[String] = new ArrayList[String],
		@BeanProperty val fromRoles : List[String] = new ArrayList[String],
		@BeanProperty val toPartyIds : List[String] = new ArrayList[String],
		@BeanProperty val toRoles : List[String] = new ArrayList[String],
		@BeanProperty val services : List[String] = new ArrayList[String],
		@BeanProperty val actions : List[String] = new ArrayList[String],
		@BeanProperty var rawInput : Boolean,
		@BeanProperty var extended : Boolean = true) extends EbMSMessageContext
	{
		setFromRole(new Role)
		setToRole(new Role)

		def this() = this(new ArrayList[String],new ArrayList[String],new ArrayList[String],new ArrayList[String],new ArrayList[String],new ArrayList[String],false,true)

		def resetFromPartyIds
		{
			getFromPartyIds.clear
			getFromRole.setPartyId(null)
		}
		def resetFromPartyIds(partyIds : List[String])
		{
			resetFromPartyIds
			getFromPartyIds.addAll(partyIds)
		}
		def resetFromRoles
		{
			getFromRoles.clear
			getFromRole.setRole(null)
		}
		def resetFromRoles(roles : List[String])
		{
			resetFromRoles
			getFromRoles.addAll(roles)
			getFromRole.setRole(if (getFromRoles.size == 1) getFromRoles get 0 else null)
		}
		def resetToPartyIds
		{
			getToPartyIds.clear
			getToRole.setPartyId(null)
		}
		def resetToPartyIds(partyIds : List[String])
		{
			resetToPartyIds
			getToPartyIds.addAll(partyIds)
			getToRole.setPartyId(if (getFromRole.getPartyId != null && getToPartyIds.size == 1) getToPartyIds get 0 else null)
		}
		def resetToRoles
		{
			getToRoles.clear
			getToRole.setRole(null)
		}
		def resetToRoles(roles : List[String])
		{
			resetToRoles
			getToRoles.addAll(roles)
			getToRole.setRole(if (getToRoles.size == 1) getToRoles get 0 else null)
		}
		def resetServices
		{
			getServices.clear
			setService(null)
		}
		def resetServices(serviceNames : List[String])
		{
			resetServices
			getServices.addAll(serviceNames)
		}
		def resetActions
		{
			getActions.clear
			setAction(null)
		}
		def resetActions(actionNames : List[String])
		{
			resetActions
			getActions.addAll(actionNames)
		}
	}
}
