/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.{Button, Form, TextField}
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class ServicePropertiesFormPanel(val id : String, val model : IModel[ServicePropertiesFormModel]) extends Panel(id,model)
{
	@transient protected def logger = LogFactory getLog getClass

	add(new ServicePropertiesForm("form",model))

	class ServicePropertiesForm(id : String, model : IModel[ServicePropertiesFormModel]) extends Form[ServicePropertiesFormModel](id,new CompoundPropertyModel[ServicePropertiesFormModel](model))
	{
		add(new BootstrapFormComponentFeedbackBorder("urlFeedback",new TextField[String]("url") setLabel new ResourceModel("lbl.url") setRequired true))
		add(createTestButton("test",model))

		private def createTestButton(id : String, model : IModel[ServicePropertiesFormModel]) : Button =
			new Button(id,new ResourceModel("cmd.test"))
			{
				override def onSubmit
				{
					try
					{
						val o = model.getObject
						Utils testEbMSUrl o.url
						info(ServicePropertiesForm.this getString "test.ok")
					}
					catch
					{
						case e : Exception => logger error("",e); error(new StringResourceModel("test.nok",ServicePropertiesForm.this,Model of e).getString)
					}
				}
			}
	}

}
class ServicePropertiesFormModel(@BeanProperty var url : String = "http://localhost:8089/adapter") extends IClusterable
