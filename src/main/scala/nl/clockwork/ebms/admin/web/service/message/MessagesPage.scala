/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapPagingNavigator
import nl.clockwork.ebms.admin.web.MaxItemsPerPageChoice
import nl.clockwork.ebms.admin.web.OddOrEvenIndexStringModel
import nl.clockwork.ebms.admin.web.PageLink
import nl.clockwork.ebms.admin.web.WebMarkupContainer
import nl.clockwork.ebms.model.EbMSMessageContext
import nl.clockwork.ebms.service.EbMSMessageService
import org.apache.wicket.AttributeModifier
import org.apache.wicket.markup.html.WebPage
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.repeater.Item
import org.apache.wicket.markup.repeater.data.DataView
import org.apache.wicket.markup.repeater.data.IDataProvider
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.spring.injection.annot.SpringBean

class MessagesPage(val filter : EbMSMessageContext, val responsePage : WebPage) extends BasePage
{
	class MessageDataView(id : String, dataProvider : IDataProvider[String]) extends DataView[String](id,dataProvider)
	{
		setOutputMarkupId(true)

		override def getItemsPerPage : Long = maxItemsPerPage.toLong

		override protected def populateItem(item : Item[String])
		{
			val messageId = item.getModelObject
			item add(createViewLink("view",messageId))
			item add(AttributeModifier.replace("class",new OddOrEvenIndexStringModel(item getIndex)))
		}

		private def createViewLink(id : String, messageId : String) : Link[Void] =
			new Link[Void](id)
			{
				override def onClick = setResponsePage(new MessagePage(ebMSMessageService.getMessage(messageId,null),MessagesPage.this))
			}
			.add(new Label("messageId",messageId))
		  .asInstanceOf[Link[Void]]
	}

	@SpringBean(name="ebMSMessageService") var ebMSMessageService : EbMSMessageService = _
	@SpringBean(name="maxItemsPerPage") var maxItemsPerPage : Integer = _

	val container = new WebMarkupContainer("container")
	add(container)
	val messages = new MessageDataView("messages",new MessageDataProvider(ebMSMessageService,this.filter))
	container.add(messages)
	val navigator = new BootstrapPagingNavigator("navigator",messages)
	add(navigator)
	add(new MaxItemsPerPageChoice("maxItemsPerPage",new PropertyModel[Int](this,"maxItemsPerPage"),navigator,container))
	add(new PageLink("back",responsePage).setVisible(responsePage != null))
	add(new DownloadEbMSMessageIdsCSVLink("download",ebMSMessageService,filter))

	def this() = this(new EbMSMessageContext,null)

	def this(filter : EbMSMessageContext) = this(filter,null)

	override def getPageTitle : String = getLocalizer.getString("messages",this)
}
