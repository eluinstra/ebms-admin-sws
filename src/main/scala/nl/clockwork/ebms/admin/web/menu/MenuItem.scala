/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.menu

import org.apache.wicket.util.io.IClusterable

import scala.collection.mutable.ListBuffer

class MenuItem(val parent0 : MenuItem, val id0 : String, val name0 : String) extends IClusterable
{
	var parent : MenuItem = parent0
	var id : String = id0
	val name : String = name0
	private var children = ListBuffer[MenuItem]()

	if (parent0 != null) parent0.children += this

	def this(id : String, name : String) = this(null,id,name)

	def getChildren : List[MenuItem] = children.toList

	def addChild(child : MenuItem)
	{
		child.id = id + "." + children.size
		child.parent = this
		children += child
	}

	override def toString : String = id
}
