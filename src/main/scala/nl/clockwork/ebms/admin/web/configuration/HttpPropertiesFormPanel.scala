/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.util.Locale

import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.form.{CheckBox, Form, FormComponent, TextField}
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.util.convert.IConverter
import org.apache.wicket.util.convert.converter.AbstractConverter
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class HttpPropertiesFormPanel(val id : String, val model : IModel[HttpPropertiesFormModel]) extends Panel(id,model)
{
	@transient val logger = LogFactory getLog getClass

	add(new HttpPropertiesForm("form", model))

	class HttpPropertiesForm(val id : String,	val model : IModel[HttpPropertiesFormModel]) extends Form[HttpPropertiesFormModel](id, new CompoundPropertyModel[HttpPropertiesFormModel](model))
	{
		add(new BootstrapFormComponentFeedbackBorder("hostFeedback", createHostField("host")).add(new Label("protocol")))
		add(new BootstrapFormComponentFeedbackBorder("portFeedback", createPortField("port")))
		add(new BootstrapFormComponentFeedbackBorder("pathFeedback", createPathField("path")))
		add(new TextField[String]("url").setLabel(new ResourceModel("lbl.url")).setOutputMarkupId(true).setEnabled(false))
		add(new CheckBox("chunkedStreamingMode").setLabel(new ResourceModel("lbl.chunkedStreamingMode")))
		add(new CheckBox("base64Writer").setLabel(new ResourceModel("lbl.base64Writer")))
		add(CreateSslCheckBox("ssl"))
		add(createSslPropertiesPanel("sslProperties"))
		add(createProxyCheckBox("proxy"))
		add(createProxyPropertiesPanel("proxyProperties"))

		private def createHostField(id : String) : FormComponent[String] =
			new TextField[String](id)
			.setLabel(new ResourceModel("lbl.host"))
			.setRequired(true)
			.add(new OnChangeAjaxBehavior
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(HttpPropertiesForm.this.get("url"))
			})
		  .asInstanceOf[FormComponent[String]]

		private def createPortField (id : String) : TextField[Int] =
			new TextField[Int](id)
			.setLabel(new ResourceModel("lbl.port"))
			.add(new OnChangeAjaxBehavior
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(HttpPropertiesForm.this.get("url"))
			})
			.asInstanceOf[TextField[Int]]

		private def createPathField (id : String) : TextField[String] =
			new TextField[String](id)
			{
				override def getConverter[C](`type`: Class[C]): IConverter[C] = new PathConverter().asInstanceOf[IConverter[C]]
			}
			.setLabel(new ResourceModel("lbl.path"))
			.setRequired(true)
			.add(new OnChangeAjaxBehavior
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(HttpPropertiesForm.this.get("url"))
			}).asInstanceOf[TextField[String]]

		private def CreateSslCheckBox (id : String) : CheckBox =
		{
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.ssl"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(HttpPropertiesForm.this)
			})
			.asInstanceOf[CheckBox]
		}

		private def createSslPropertiesPanel (id : String) : SslPropertiesFormPanel =
		{
			new SslPropertiesFormPanel(id, new PropertyModel[SslPropertiesFormModel](getModelObject, "sslProperties"))
			{
				override def isVisible : Boolean = getModelObject.ssl
			}
		}

		private def createProxyCheckBox (id : String) : CheckBox =
		{
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.proxy"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate (target : AjaxRequestTarget) = target.add(HttpPropertiesForm.this)
			})
			.asInstanceOf[CheckBox]
		}

		private def createProxyPropertiesPanel (id : String) : ProxyPropertiesFormPanel =
		{
			new ProxyPropertiesFormPanel(id, new PropertyModel[ProxyPropertiesFormModel](getModelObject, "proxyProperties"))
			{
				override def isVisible : Boolean = getModelObject.proxy
			}
		}

	}
}
class HttpPropertiesFormModel(
	@BeanProperty var host : String = "0.0.0.0",
	@BeanProperty var port : Int = 8888,
	@BeanProperty var path : String = "/digipoortStub",
	@BeanProperty var chunkedStreamingMode : Boolean = true,
	@BeanProperty var base64Writer : Boolean = true,
	@BeanProperty var ssl : Boolean = true,
	@BeanProperty var sslProperties : SslPropertiesFormModel = new SslPropertiesFormModel,
	@BeanProperty var proxy : Boolean = false,
	@BeanProperty var proxyProperties : ProxyPropertiesFormModel = new ProxyPropertiesFormModel) extends IClusterable
{
	def getProtocol : String = if (ssl) "https://" else "http://"

	def getUrl : String = getProtocol + host + (if(port == 0) "" else ":" + port.toString) + path
}

class PathConverter extends AbstractConverter[String]
{
	override def convertToObject(value : String, locale : Locale) : String = "/" + value

	override def convertToString(value : String, locale : Locale) : String = value.substring(1)

	override protected def getTargetType : Class[String] = classOf[String]
}
