/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.cpa

import java.util.List

import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import nl.clockwork.ebms.admin.web.ResetButton
import nl.clockwork.ebms.service.CPAService

import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.markup.html.form.CheckBox
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.form.TextField
import org.apache.wicket.markup.html.form.upload.FileUpload
import org.apache.wicket.markup.html.form.upload.FileUploadField
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class CPAUploadPage extends BasePage
{
	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="cpaService") var cpaService : CPAService = _

	add(new BootstrapFeedbackPanel("feedback"))
	add(new EditUploadForm("form"))
	
	override def getPageTitle : String = getLocalizer getString("cpaUpload",this)

	class EditUploadForm(val id : String) extends Form[EditUploadFormModel](id,new CompoundPropertyModel[EditUploadFormModel](new EditUploadFormModel))
	{
		setMultiPart(true)
		add(new BootstrapFormComponentFeedbackBorder("cpaFeedback",createCPAFileField("cpaFile")))
		add(new TextField[String]("url").setLabel(new ResourceModel("lbl.url")))
		add(new CheckBox("overwrite").setLabel(new ResourceModel("lbl.overwrite")))
		add(createValidateButton("validate"))
		val upload = createUploadButton("upload")
 		setDefaultButton(upload)
		add(upload)
		add(new ResetButton("reset",new ResourceModel("cmd.reset"),classOf[CPAUploadPage]))

		private def createCPAFileField(id : String) : FileUploadField = new FileUploadField(id).setLabel(new ResourceModel("lbl.cpa")).setRequired(true).asInstanceOf[FileUploadField]

		private def createValidateButton(id : String) : Button = new Button(id,new ResourceModel("cmd.validate"))
		{
			override def onSubmit
			{
				try
				{
					val files = EditUploadForm.this.getModelObject.cpaFile
					if (files != null && files.size == 1)
					{
						val file = files get 0
						//val contentType = file getContentType
						//FIXME char encoding
						cpaService validateCPA(new String(file.getBytes))
					}
					info(getString("cpa.valid"))
				}
				catch
				{
				  case e : Exception =>	logger error("",e);	error(e getMessage)
				}
			}
		}

		private def createUploadButton(id : String) : Button =
		{
		  new Button(id,new ResourceModel("cmd.upload"))
  		{
  			override def onSubmit
  			{
  				try
  				{
  					val files = EditUploadForm.this.getModelObject.cpaFile
  					if (files != null && files.size == 1)
  					{
  						val file = files get 0
  						//val contentType = file getContentType
  						//FIXME char encoding
  						cpaService insertCPA(new String(file.getBytes),EditUploadForm.this.getModelObject.url,EditUploadForm.this.getModelObject.overwrite)
  					}
  					setResponsePage(new CPAsPage)
  				}
  				catch
  				{
  				  case e : Exception =>	logger error("",e);	error(e getMessage)
  				}
  			}
  		}
		}
	}
	
	class EditUploadFormModel(@BeanProperty var cpaFile : List[FileUpload], @BeanProperty var url : String, @BeanProperty var overwrite : Boolean) extends IClusterable
	{
	  def this() = this(null,null,false)
	}

}
