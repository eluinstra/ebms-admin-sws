/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import org.apache.wicket.feedback.ContainerFeedbackMessageFilter
import org.apache.wicket.feedback.FeedbackCollector
import org.apache.wicket.feedback.IFeedback
import org.apache.wicket.feedback.IFeedbackMessageFilter
import org.apache.wicket.markup.ComponentTag
import org.apache.wicket.markup.html.border.Border
import org.apache.wicket.markup.html.form.FormComponent

class BootstrapFormComponentFeedbackBorder(val id : String, val formComponents : FormComponent[_]*) extends Border(id) with IFeedback
{
	add(formComponents : _*)

	override protected def onBeforeRender = super.onBeforeRender

	override protected def onComponentTag(tag : ComponentTag)
	{
		if (new FeedbackCollector(getPage).collect(getMessagesFilter).size > 0)
			tag.put("class",(if (tag.getAttribute("class") == null) "" else  tag.getAttribute("class") + " ") + "has-error")
		super.onComponentTag(tag)
	}
	
	protected def getMessagesFilter : IFeedbackMessageFilter = new ContainerFeedbackMessageFilter(this)
}
