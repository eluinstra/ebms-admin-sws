/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.cpa

import java.util.ArrayList
import java.util.Iterator

import nl.clockwork.ebms.admin.Utils
import nl.clockwork.ebms.service.CPAService

import org.apache.wicket.markup.repeater.data.IDataProvider
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model

import scala.collection.JavaConversions._

class CPADataProvider(val cpaService : CPAService) extends IDataProvider[String]
{
	override def iterator(first : Long, count : Long) : Iterator[_ <: String] =
	{
		val cpaIds = Utils toList(cpaService.getCPAIds toList)
		if (cpaIds == null) new ArrayList[String] iterator else cpaIds iterator
	}

	override def model(cpaId : String) : IModel[String] = Model.of(cpaId)

	override def size : Long =
	{
		val cpaIds = Utils toList(cpaService.getCPAIds toList)
		if (cpaIds == null) 0 else cpaIds size
	}

	override def detach {}
}
object CPADataProvider
{
  def apply(cpaService : CPAService) = new CPADataProvider(cpaService) 
}