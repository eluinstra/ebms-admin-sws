/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import org.apache.commons.logging.LogFactory
import org.apache.wicket.Component
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior
import org.apache.wicket.markup.html.form.{Button, TextField, _}
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class JdbcPropertiesFormPanel(val id : String, val model : IModel[JdbcPropertiesFormModel]) extends Panel(id,model)
{
	@transient protected val logger = LogFactory getLog getClass

	add(new JdbcPropertiesForm("form",model))

	class JdbcPropertiesForm(val id : String, val model : IModel[JdbcPropertiesFormModel]) extends Form[JdbcPropertiesFormModel](id,new CompoundPropertyModel[JdbcPropertiesFormModel](model))
	{
		add(new BootstrapFormComponentFeedbackBorder("driverFeedback",createDriverChoice("driver",model)))
		add(new BootstrapFormComponentFeedbackBorder("hostFeedback",createHostsField("host")))
		add(new BootstrapFormComponentFeedbackBorder("portFeedback",createPortField("port")))
		add(new BootstrapFormComponentFeedbackBorder("databaseFeedback",createDatabaseField("database")))
		add(new TextField[String]("url").setLabel(new ResourceModel("lbl.url")).setOutputMarkupId(true).setEnabled(false))
		add(createTestButton("test",model))
		add(new BootstrapFormComponentFeedbackBorder("usernameFeedback",new TextField[String]("username").setLabel(new ResourceModel("lbl.username")).setRequired(true)))
		add(new BootstrapFormComponentFeedbackBorder("passwordFeedback",new PasswordTextField("password").setResetPassword(false).setLabel(new ResourceModel("lbl.password")).setRequired(false)))

		private def createDriverChoice(id : String, model : IModel[JdbcPropertiesFormModel]) : DropDownChoice[JdbcDriver] =
			new DropDownChoice[JdbcDriver](id,new PropertyModel[java.util.List[JdbcDriver]](model.getObject,"drivers"))
			.setLabel(new ResourceModel("lbl.driver"))
			.setRequired(true)
			.add(new OnChangeAjaxBehavior
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(getUrlComponent)
			})
			.asInstanceOf[DropDownChoice[JdbcDriver]]

		private def createHostsField(id : String) : TextField[String] =
			new TextField[String](id)
			.setLabel(new ResourceModel("lbl.host"))
			.setRequired(true)
			.add(new OnChangeAjaxBehavior
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(getUrlComponent)
			})
		  .asInstanceOf[TextField[String]]

		private def createPortField(id : String) : TextField[Integer] =
			new TextField[Integer](id)
			.setLabel(new ResourceModel("lbl.port"))
			.add(new OnChangeAjaxBehavior
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(getUrlComponent)
			})
		  .asInstanceOf[TextField[Integer]]

		private def createDatabaseField(id : String) : TextField[String] =
			new TextField[String](id)
			.setLabel(new ResourceModel("lbl.database"))
			.setRequired(true)
			.add(new OnChangeAjaxBehavior
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(getUrlComponent)
			})
		  .asInstanceOf[TextField[String]]

		private def createTestButton(id : String, model : IModel[JdbcPropertiesFormModel]) : Button =
		{
			new Button(id,new ResourceModel("cmd.test"))
			{
				override def onSubmit
				{
					try
					{
						val m = model.getObject
						Utils.testJdbcConnection(m.getDriver driverClassName,m getUrl,m getUsername,m getPassword)
						info(JdbcPropertiesForm.this getString "test.ok")
					}
					catch
					{
						case e : Exception => logger error("",e); error(new StringResourceModel("test.nok",JdbcPropertiesForm.this,Model of e).getString)
					}
				}
			}
		}

		private def getUrlComponent : Component = this.get("url")
	}
}
class JdbcPropertiesFormModel(
	@BeanProperty var driver : JdbcDriver = JdbcDriver.HSQLDB,
	@BeanProperty var username : String = "sa",
	@BeanProperty var password : String = "") extends JdbcURL with IClusterable
{
	def getDrivers : java.util.List[JdbcDriver] = JdbcDriver.values

	def getUrl : String = JdbcDriver.createJdbcURL(driver.urlExpr,host,port,database)
}
