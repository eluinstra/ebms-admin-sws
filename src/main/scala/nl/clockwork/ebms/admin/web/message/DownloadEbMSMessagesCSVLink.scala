/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.OutputStreamWriter

import nl.clockwork.ebms.admin.dao.EbMSDAO
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler
import org.apache.wicket.request.resource.ContentDisposition
import org.apache.wicket.util.resource.IResourceStream
import resource._

class DownloadEbMSMessagesCSVLink(val id : String, val ebMSDAO : EbMSDAO, val filter : EbMSMessageFilter) extends Link[Void](id)
{
	@transient val logger = LogFactory getLog getClass

	override def onClick
	{
		try
		{
			val output = new ByteArrayOutputStream
			managed (new CSVPrinter(new OutputStreamWriter(output),CSVFormat.DEFAULT)).foreach(printer =>
			{
				ebMSDAO.printMessagesToCSV(printer,filter)
			})
			val resourceStream = new ByteArrayResourceStream(output,"text/csv")
			getRequestCycle scheduleRequestHandlerAfterCurrent createRequestHandler(resourceStream)
		}
		catch
		{
			case e : IOException => logger error("",e); error(e getMessage)
		}
	}

	private def createRequestHandler(resourceStream : IResourceStream) : ResourceStreamRequestHandler =
		new ResourceStreamRequestHandler(resourceStream) setFileName "messages.csv" setContentDisposition ContentDisposition.ATTACHMENT
}
