/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.util.ArrayList
import java.util.List
import javax.xml.bind.JAXBException

import nl.clockwork.ebms.admin.CPAUtils
import nl.clockwork.ebms.admin.Utils
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import nl.clockwork.ebms.admin.web.ResetButton
import nl.clockwork.ebms.common.XMLMessageBuilder
import nl.clockwork.ebms.model.Party
import nl.clockwork.ebms.service.CPAService
import nl.clockwork.ebms.service.EbMSMessageService
import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.markup.html.form.DropDownChoice
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.apache.wicket.util.io.IClusterable
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.CollaborationProtocolAgreement

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class PingPage extends BasePage
{
	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="cpaService") private var cpaService : CPAService = _
	@SpringBean(name="ebMSMessageService") private var ebMSMessageService : EbMSMessageService = _
	@SpringBean(name="cleoPatch") private var cleoPatch : Boolean = _

	add(new BootstrapFeedbackPanel("feedback").setOutputMarkupId(true))
	add(new PingForm("form"))
	
	override def getPageTitle : String = getLocalizer.getString("ping",this)

	class PingForm(val id : String) extends Form[PingFormModel](id,new CompoundPropertyModel[PingFormModel](new PingFormModel))
	{
		add(new BootstrapFormComponentFeedbackBorder("cpaIdFeedback",createCPAIdChoice("cpaId")))
		add(new BootstrapFormComponentFeedbackBorder("fromPartyIdFeedback",createFromPartyIdChoice("fromPartyId")))
		add(new BootstrapFormComponentFeedbackBorder("fromRoleFeedback",createFromRoleChoice("fromRole")).setVisible(cleoPatch))
		add(new BootstrapFormComponentFeedbackBorder("toPartyIdFeedback",createToPartyIdChoice("toPartyId")))
		add(new BootstrapFormComponentFeedbackBorder("toRoleFeedback",createToRoleChoice("toRole")).setVisible(cleoPatch))
		val ping = createPingButton("ping")
		setDefaultButton(ping)
		add(ping)
		add(new ResetButton("reset",new ResourceModel("cmd.reset"),classOf[PingPage]))

		private def createCPAIdChoice(id : String) : DropDownChoice[String] =
		{
			new DropDownChoice[String](id,Model ofList Utils.toList(cpaService.getCPAIds toList))
			.setLabel(new ResourceModel("lbl.cpaId"))
			.setRequired(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = PingForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.cpaId)
						model resetFromPartyIds(CPAUtils getPartyIds cpa)
						model resetFromRoles(CPAUtils getRoleNames cpa)
            model resetToPartyIds()
						model resetToRoles()
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
					  case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
			.asInstanceOf[DropDownChoice[String]]
		}

		private def createFromPartyIdChoice(id : String) : DropDownChoice[String] =
		{
			new DropDownChoice[String](id,new PropertyModel[List[String]](this.getModelObject,"fromPartyIds"))
			.setLabel(new ResourceModel("lbl.fromPartyId"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = PingForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.cpaId)
						model resetFromRoles(CPAUtils getRoleNames(cpa,model fromPartyId))
						model resetToPartyIds(CPAUtils getOtherPartyIds(cpa,model fromPartyId))
						model resetToRoles(if (model.fromRole != null) CPAUtils getRoleNames(cpa,model toPartyId) else new ArrayList[String])
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
					  case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
			.asInstanceOf[DropDownChoice[String]]
		}

		private def createFromRoleChoice(id : String) : DropDownChoice[String] =
		{
			new DropDownChoice[String](id,new PropertyModel[List[String]](this.getModelObject,"fromRoles"))
			.setLabel(new ResourceModel("lbl.fromRole"))
			.setRequired(false)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = PingForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.cpaId)
						model resetFromPartyIds(CPAUtils getPartyIdsByRoleName(cpa,model fromRole))
						model resetToPartyIds(CPAUtils getOtherPartyIds(cpa,model fromPartyId))
						model resetToRoles(CPAUtils getRoleNames(cpa,model toPartyId))
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
					  case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
			.asInstanceOf[DropDownChoice[String]]
		}

		private def createToPartyIdChoice(id : String) : DropDownChoice[String] =
		{
			new DropDownChoice[String](id,new PropertyModel[List[String]](this.getModelObject,"toPartyIds"))
			.setLabel(new ResourceModel("lbl.toPartyId"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = PingForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.cpaId)
						model resetToRoles(CPAUtils getRoleNames(cpa,model toPartyId))
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
					  case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
			.asInstanceOf[DropDownChoice[String]]
		}

		private def createToRoleChoice(id : String) : DropDownChoice[String] =
		{
			new DropDownChoice[String](id,new PropertyModel[List[String]](this.getModelObject,"toRoles"))
			.setLabel(new ResourceModel("lbl.toRole"))
			.setRequired(false).setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = PingForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.cpaId)
						model.resetToPartyIds(if (model.getToPartyId == null) CPAUtils getPartyIdsByRoleName(cpa,model toRole) else new ArrayList[String])
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
					  case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
			.asInstanceOf[DropDownChoice[String]]
		}

		private def createPingButton(id : String) : Button =
		{
			new Button(id,new ResourceModel("cmd.ping"))
			{
				override def onSubmit
				{
					try
					{
						val model = PingForm.this.getModelObject
						ebMSMessageService ping(model cpaId,new Party(model fromPartyId,model fromRole),new Party(model toPartyId,model toRole))
						info(PingPage.this.getString("ping.ok"))
					}
					catch
					{
					  case e : Exception => logger error("",e); error(e getMessage)
					}
				}
			}
		}

	}

	class PingFormModel(
	  @BeanProperty var cpaId : String,
	  @BeanProperty val fromPartyIds : List[String] = new ArrayList[String],
	  @BeanProperty var fromPartyId : String,
	  @BeanProperty val fromRoles : List[String] = new ArrayList[String],
	  @BeanProperty var fromRole : String,
	  @BeanProperty val toPartyIds : List[String] = new ArrayList[String],
	  @BeanProperty var toPartyId : String,
	  @BeanProperty val toRoles : List[String] = new ArrayList[String],
	  @BeanProperty var toRole : String) extends IClusterable
	{
	  def this() = this(null,new ArrayList[String],null,new ArrayList[String],null,new ArrayList[String],null,new ArrayList[String],null)

	  def resetFromPartyIds()
		{
			getFromPartyIds.clear
			setFromPartyId(null)
		}
		def resetFromPartyIds(partyIds : List[String])
		{
			resetFromPartyIds
			getFromPartyIds addAll partyIds
			setFromPartyId(if (getFromPartyIds.size == 1) getFromPartyIds get 0 else null)
		}
		def resetFromRoles()
		{
			getFromRoles.clear
			setFromRole(null)
		}
		def resetFromRoles(roleNames : List[String])
		{
			resetFromRoles
			getFromRoles addAll roleNames
		}
		def resetToPartyIds()
		{
			getToPartyIds.clear
			setToPartyId(null)
		}
		def resetToPartyIds(partyIds : List[String])
		{
			resetToPartyIds
			getToPartyIds addAll partyIds
			setToPartyId(if (getToPartyIds.size == 1) getToPartyIds get 0 else null)
		}
		def resetToRoles()
		{
			getToRoles.clear
			setToRole(null)
		}
		def resetToRoles(roleNames : List[String])
		{
			resetToRoles
			getToRoles addAll roleNames
			setToRole(if (getFromRole != null && getToRoles.size == 1) getToRoles get 0 else null)
		}
	}		

}
