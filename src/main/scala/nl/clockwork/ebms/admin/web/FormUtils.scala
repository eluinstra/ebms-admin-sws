package nl.clockwork.ebms.admin.web

import org.apache.wicket.Page
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.html.form.DropDownChoice
import org.apache.wicket.model.{IModel, ResourceModel}

trait FormUtils
{
  def createDropDownChoice(id : String, choices: IModel[_ <: java.util.List[_ <: String]], label : String, page : => Page, updateComponents : List[String], update : => Unit, error : Exception => Unit) : DropDownChoice[String] =
    new DropDownChoice[String](id,choices)
    .setLabel(new ResourceModel(label))
    .setRequired(true)
    .setOutputMarkupId(true)
    .add(new AjaxFormComponentUpdatingBehavior("onchange")
    {
      override def onUpdate(target : AjaxRequestTarget)
      {
        try
        {
          update
          updateComponents.foreach(updateComponent => target add(page get updateComponent))
        }
        catch
        {
          case e : Exception => error(e)
        }
      }
    })
    .asInstanceOf[DropDownChoice[String]]

}
