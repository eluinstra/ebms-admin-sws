/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.util.List

import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import nl.clockwork.ebms.admin.web.Utils
import nl.clockwork.ebms.model.EbMSDataSource
import org.apache.commons.lang.StringUtils
import org.apache.commons.logging.LogFactory
import org.apache.wicket.Component
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.markup.html.form.AjaxButton
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
import org.apache.wicket.markup.html.form.{Button, Form, TextField}
import org.apache.wicket.markup.html.form.upload.FileUpload
import org.apache.wicket.markup.html.form.upload.FileUploadField
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.ResourceModel

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class DataSourceModalWindow(val id : String, val dataSources : List[EbMSDataSource], val components : Component*) extends ModalWindow(id)
{
	setCssClassName(ModalWindow.CSS_CLASS_GRAY)
	setContent(createDataSourcePanel(dataSources,components))
	setCookieName("dataSource")
	setCloseButtonCallback(new nl.clockwork.ebms.admin.web.CloseButtonCallback)

	override def getTitle : IModel[String] = Model of getLocalizer.getString("dataSource",this)

	private def createDataSourcePanel(dataSources : List[EbMSDataSource], components : Seq[Component]) : DataSourcePanel =
		new DataSourcePanel(getContentId)
		{
			override def addDataSource(dataSource : EbMSDataSource) : Unit = dataSources.add(dataSource)

			override def getComponents : Seq[Component] = components

			override def getWindow : ModalWindow = DataSourceModalWindow.this
		}

	abstract class DataSourcePanel(id : String) extends Panel(id)
	{
		@transient val logger = LogFactory getLog getClass

		add(new DataSourceForm("form"))

		def addDataSource(dataSource : EbMSDataSource) : Unit
		def getComponents : Seq[Component]
		def getWindow : ModalWindow

		class DataSourceForm(id : String) extends Form[DataSourceModel](id,new CompoundPropertyModel[DataSourceModel](new DataSourceModel))
		{
			add(new BootstrapFeedbackPanel("feedback"))
			add(new BootstrapFormComponentFeedbackBorder("fileFeedback",createFileField("file")))
			add(new TextField[String]("name").setLabel(new ResourceModel("lbl.name")))
			add(new TextField[String]("contentType").setLabel(new ResourceModel("lbl.contentType")))
			add(createAddButton("add"))
			add(createCancelButton("cancel"))

			def createFileField(id : String) : FileUploadField =
				new FileUploadField(id)
				.setLabel(new ResourceModel("lbl.file"))
				.setRequired(true)
			  .asInstanceOf[FileUploadField]

			def createAddButton(id : String) : AjaxButton =
				new AjaxButton(id,new ResourceModel("cmd.add"))
				{
					override protected def onSubmit(target : AjaxRequestTarget, form : Form[_])
					{
						val model = DataSourceForm.this.getModelObject
						model.getFile.foreach(file => addDataSource(new EbMSDataSource(if (model.getName.trim.nonEmpty) file getClientFileName else model getName,if (model.getContentType.trim.nonEmpty) Utils.getContentType(file getClientFileName) else model getContentType,file getBytes)))
						if (target != null)
						{
							target add(getComponents : _*)
							getWindow close target
						}
					}

					override protected def onError(target : AjaxRequestTarget, form : Form[_])
					{
						super.onError(target,form)
						if (target != null)
							target.add(form)
					}
				}

			def createCancelButton(id : String) : Button =
				new AjaxButton(id,new ResourceModel("cmd.cancel"))
				{
					override protected def onSubmit(target : AjaxRequestTarget, form : Form[_])
					{
						getWindow.close(target)
					}
				}
				.setDefaultFormProcessing(false)
		}
	}

	class DataSourceModel(@BeanProperty var file : List[FileUpload]) extends EbMSDataSource
	{
		def this() = this(null)
	}
}
