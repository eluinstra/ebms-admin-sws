/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.util.ServiceLoader

import nl.clockwork.ebms.admin.model.EbMSAttachment
import nl.clockwork.ebms.admin.model.EbMSMessage
import nl.clockwork.ebms.admin.web.service.message.DataSourcesPanel

import org.apache.wicket.markup.html.panel.Panel

import scala.collection.JavaConversions._

abstract class MessageProvider
{
	def getMessageViewPanels : List[MessageViewPanel]
	def getMessageEditPanels : List[MessageEditPanel]
}
object MessageProvider
{
	def get : List[MessageProvider] =
	{
		val providers = ServiceLoader.load(classOf[MessageProvider]).toList
		for (provider <- providers)
			yield provider
	}

	def createId(message : EbMSMessage) : Object = createId(message.getService,message.getAction)

	def createId(service : String, action : String) : String = service + ":" + action
}
class MessagePanel(val service : String, val action : String)
{
	def getId : String = MessageProvider.createId(service,action)
}
abstract class MessageViewPanel(override val service : String, override val action : String) extends MessagePanel(service,action)
{
	def getPanel(id : String, attachments : List[EbMSAttachment]) : Panel
}

abstract class MessageEditPanel(override val service : String, override val action : String) extends MessagePanel(service,action)
{
	def getPanel(id : String) : DataSourcesPanel
}
