/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.net.HttpURLConnection
import java.net.URL
import java.security.KeyStore
import java.sql.Driver
import java.util.Properties
import java.util.Scanner

import javax.net.ssl.SSLContext
import javax.net.ssl.SSLEngine

import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource

import scala.collection.JavaConversions._
import resource._

object Utils
{
	var sslEngine : SSLEngine = _
	try
	{
		val sslContext = SSLContext.getInstance("TLS")
		sslContext.init(null,null,null)
		sslEngine = sslContext.createSSLEngine
	}
	catch
	{
		case _ : Exception =>
	}

	def defaultValue(param: String = null) =
		param match
		{
			case null => ""
			case s => s
		}

	def getSupportedSSLProtocols : Array[String] = sslEngine.getSupportedProtocols

	def getSupportedSSLCipherSuites : Array[String] = sslEngine.getSupportedCipherSuites

	def createURL(hostname : String, port : Int) : String = hostname + (if (port == 0) "" else ":" + port)

	def parseJdbcURL(jdbcURL : String, model : JdbcURL) : JdbcURL =
	{
		managed (new Scanner(jdbcURL)).foreach(scanner =>
		{
			val protocol = scanner.findInLine("(://|@|:@//)")
			if (protocol != null)
			{
				val urlString = scanner.findInLine("[^/:]+(:\\d+){0,1}")
				scanner.findInLine("(/|:|;databaseName=)")
				val database = scanner.findInLine("[^;]*")
				if (urlString != null)
				{
					val url = new URL("http://" + urlString)
					model.host = url.getHost
					model.port = url.getPort
					model.database = database
				}
			}
		})
		model
	}

  def testEbMSUrl(uri : String)
	{
		val url = new URL(uri + "/cpa?wsdl")
		//val url = new URL(uri + "/message?wsdl")
		val connection = url.openConnection
		if (connection.isInstanceOf[HttpURLConnection])
		{
			connection setDoOutput true
			connection.asInstanceOf[HttpURLConnection] setRequestMethod "GET"
			connection.connect
			if (connection.asInstanceOf[HttpURLConnection].getResponseCode != 200)
				throw new RuntimeException("Status code " + connection.asInstanceOf[HttpURLConnection].getResponseCode)
		}
		else
			throw new IllegalArgumentException("Unknown protocol: " + uri)
	}
	
	def getResource(path : String) : Resource =
{
		val result = new FileSystemResource(path)
		if (result exists) result else new ClassPathResource(path)
	}
  
	def testKeystore(path : String, password : String)
	{
		val resource = getResource(path)
		val keyStore = KeyStore getInstance "JKS"
		keyStore load(resource getInputStream,password toCharArray)
		val aliases = keyStore.aliases
		aliases.foreach(alias =>
		{
			if (keyStore isKeyEntry alias)
				keyStore getKey(alias,password toCharArray)
		})
	}

	def testJdbcConnection(driverClassName : String, jdbcUrl : String, username : String, password : String)
	{
    val loader = getClass.getClassLoader
    val driverClass = loader loadClass driverClassName
    val driver = driverClass.newInstance.asInstanceOf[Driver]
    if (!driver.acceptsURL(jdbcUrl))
    	throw new IllegalArgumentException("Jdbc Url '" + jdbcUrl + "' not valid!")
    val info = new Properties
    info setProperty("user",username)
    if (password != null)
    	info setProperty("password",password)
    managed (driver connect(jdbcUrl,info))
	}
}
