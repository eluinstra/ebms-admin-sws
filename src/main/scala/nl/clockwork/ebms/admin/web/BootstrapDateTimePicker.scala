/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.util.ArrayList
import java.util.Date

import nl.clockwork.ebms.admin.Constants.JQueryLocale

import org.apache.commons.lang.StringUtils
import org.apache.wicket.MarkupContainer
import org.apache.wicket.extensions.markup.html.form.DateTextField
import org.apache.wicket.markup.head.OnDomReadyHeaderItem
import org.apache.wicket.markup.html.form.FormComponentPanel
import org.apache.wicket.markup.html.form.TextField
import org.apache.wicket.markup.html.internal.HtmlHeaderContainer
import org.apache.wicket.model.IModel
import org.apache.wicket.model.PropertyModel

class BootstrapDateTimePicker(val id : String, val model : IModel[Date], val format : String, val `type` : Type) extends FormComponentPanel[Date](id,model)
{
	private val formatJS: String = format.replaceAll("H", "h")
	private val hourFormat: HourFormat = if (format.contains("H")) HourFormat.H24 else HourFormat.H12
	private var startDate: Date = _
	private var endDate: Date = _
	private var dateTime: Date = _

	//FIXME??? setType(`type`)

	val dateTimePicker = new MarkupContainer("dateTimePicker"){}
	dateTimePicker.setMarkupId(getDateTimePickerId)
	dateTimePicker.setOutputMarkupId(true)
	add(dateTimePicker)

	private val dateTimeField: TextField[Date] = new DateTextField("dateTime", new PropertyModel[Date](this, "dateTime"), format)
	{
		override def isRequired: Boolean = BootstrapDateTimePicker.this.isRequired
	}
	dateTimePicker.add(dateTimeField)

	def this(id : String, model : IModel[Date]) = this(id, model, "dd-MM-yyyy HH:mm:ss", Type.DATE_TIME)

	def this(id : String) = this(id, null.asInstanceOf[(IModel[Date])])

	def this(id : String, format : String, `type` : Type) = this(id, null, format,	`type`)

	override def renderHead(container : HtmlHeaderContainer)
	{
		val response = container.getHeaderResponse
		val options = new ArrayList[String]
		if (formatJS != null)
			options.add("format: '" + formatJS + "'")
		if (!Type.DATE_TIME.equals(`type`) & !Type.DATE.equals(`type`))
		options.add("pickDate: false")
		if (!Type.DATE_TIME.equals(`type`) & !Type.TIME.equals(`type`))
		options.add("pickTime: false")
		if (HourFormat.H12.equals(hourFormat))
			options.add("pick12HourFormat: true")
		if (getJQueryLocale != null)
			options.add("language: '" + getLocale.toString + "'")
		if (startDate != null)
			options.add("startDate: new Date(" + startDate.getTime + ")")
		if (endDate != null)
			options.add("endDate: new Date(" + endDate.getTime + ")")
		response.render(OnDomReadyHeaderItem.forScript("$(function  {$('#" + getDateTimePickerId + "').datetimepicker({" + StringUtils.join(options,",") + "})})"))
		super.renderHead(container)
	}

	override protected def convertInput
	{
		dateTime = dateTimeField.getConvertedInput
		setConvertedInput(dateTime)
	}

	override protected def onBeforeRender
	{
		dateTime = getModelObject
		super.onBeforeRender
	}

	def getJQueryLocale = JQueryLocale.EN

	def setStartDate(startDate : Date) = this.startDate = startDate

	def setEndDate(endDate : Date) = this.endDate = endDate

	private def getDateTimePickerId : String = getMarkupId + "DateTimePicker"

	def getDateTime : Date = dateTime

	def setDateTime(dateTime : Date) = this.dateTime = dateTime
}
object BootstrapDateTimePicker
{
	def getLinkBootstrapDateTimePickersJavaScript(startDate : BootstrapDateTimePicker, endDate : BootstrapDateTimePicker) : String =
	{
		"$(function  {" +
			"$('#" + startDate.getDateTimePickerId + "').on('changeDate',function  {" +
				"var d = $('#" + startDate.getDateTimePickerId + "').data('datetimepicker').getDate" +
				"d.setDate(d.getDate + 1)" +
				"$('#" + endDate.getDateTimePickerId + "').data('datetimepicker').setStartDate(d)" +
			"})" +
			"$('#" + endDate.getDateTimePickerId + "').on('changeDate',function (e) {" +
				"var d = $('#" + endDate.getDateTimePickerId + "').data('datetimepicker').getDate" +
				"d.setDate(d.getDate - 1)" +
				"$('#" + startDate.getDateTimePickerId + "').data('datetimepicker').setEndDate(d)" +
			"})" +
		"})"
	}
	
}
sealed trait Type
object Type
{
	case object DATE_TIME extends Type
	case object DATE extends Type
	case object TIME extends Type
}
sealed trait HourFormat
object HourFormat
{
	case object H12 extends HourFormat
	case object H24 extends HourFormat
}
