/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.EbMSAttachment
import nl.clockwork.ebms.admin.web.Utils

import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler
import org.apache.wicket.request.resource.ContentDisposition
import org.apache.wicket.util.encoding.UrlEncoder
import org.apache.wicket.util.resource.IResourceStream

class DownloadEbMSAttachmentLink(val id : String, val ebMSDAO : EbMSDAO , val messageId : String , val messageNr : Int, val contentId : String) extends Link[Void](id,null)
{
	def this(id : String, ebMSDAO : EbMSDAO, attachment : EbMSAttachment) =  this(id,ebMSDAO,attachment.getMessage.getMessageId,attachment.getMessage.getMessageNr,attachment.getContentId)

	override def onClick
	{
		val attachment = ebMSDAO.findAttachment(messageId,messageNr,contentId).get
		val fileName = UrlEncoder.QUERY_INSTANCE.encode(if(attachment.getName isEmpty) attachment.getContentId + Utils.getFileExtension(attachment getContentType) else attachment getName,getRequest getCharset)
		val resourceStream = AttachmentResourceStream(attachment)
		getRequestCycle scheduleRequestHandlerAfterCurrent createRequestHandler(fileName,resourceStream)
	}

	private def createRequestHandler(fileName : String, resourceStream : IResourceStream) : ResourceStreamRequestHandler =
		new ResourceStreamRequestHandler(resourceStream) setFileName fileName setContentDisposition ContentDisposition.ATTACHMENT
}
