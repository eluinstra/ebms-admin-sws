/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message;

import java.util.ArrayList;
import java.util.Iterator;

import nl.clockwork.ebms.admin.Utils;
import nl.clockwork.ebms.model.EbMSMessageContext;
import nl.clockwork.ebms.service.EbMSMessageService;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import scala.collection.JavaConversions._

class MessageDataProvider(val ebMSMessageService : EbMSMessageService , val filter : EbMSMessageContext ) extends IDataProvider[String]
{
	override def iterator(first : Long, count : Long) : Iterator[_ <: String] =
	{
		val messageIds = Utils.toList(ebMSMessageService.getMessageIds(filter,(first+count).toInt) toList)
		if (messageIds == null) new ArrayList[String]().iterator() else messageIds.listIterator(first.toInt)
	}

	override def model(messageId : String) : IModel[String] = Model of messageId

	override def size : Long =
	{
		val messageIds = Utils.toList(ebMSMessageService.getMessageIds(filter,null) toList)
		if (messageIds == null) 0 else messageIds size
	}

	override def detach {}
}
