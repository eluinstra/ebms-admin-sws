/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import java.text.SimpleDateFormat
import java.util.Date

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.Constants.{EbMSMessageTrafficChartOption, TimeUnit}
import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.ajax.markup.html.AjaxLink
import org.apache.wicket.markup.html.form.{DropDownChoice, Form}
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.apache.wicket.util.io.IClusterable
import org.joda.time.DateTime
import com.googlecode.wickedcharts.highcharts.options._
import com.googlecode.wickedcharts.highcharts.options.series.SimpleSeries
import com.googlecode.wickedcharts.wicket6.highcharts.Chart

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer

class TrafficChartPage(val model : TrafficChartFormModel) extends BasePage
{
	@SpringBean(name="ebMSAdminDAO") var ebMSDAO : EbMSDAO = _
	private var chart : Chart = _

	add(new BootstrapFeedbackPanel("feedback"))
	add(new TrafficChartForm("form",model))

	def this() = this(TrafficChartPage.getTrafficChartFormModel(TimeUnit DAY,EbMSMessageTrafficChartOption ALL))

	private def createOptions(model : TrafficChartFormModel) : Options =
	{
		var from = new DateTime(model.from getTime)
		val to = from.plus(model.timeUnit period)

		val dates = ListBuffer[Date]()
		while (from.isBefore(to))
		{
			dates += from.toDate
			from = from.plus(model.timeUnit timeUnit)
		}

		val dateString = ListBuffer[String]()
		dates.foreach(date => dateString.add(new SimpleDateFormat(model.timeUnit timeUnitDateFormat) format date))

		val result = new Options
		result.setChartOptions(new ChartOptions() setType SeriesType.LINE)
		result.setTitle(new Title(model.getEbMSMessageTrafficChartOption.title + " " + new SimpleDateFormat(model.timeUnit.dateFormat).format(model.getFrom)))
		result.setxAxis(new Axis() setCategories dateString)
		result.setyAxis(new Axis() setTitle new Title("Messages"))
		result.setLegend(new Legend() setLayout LegendLayout.VERTICAL setAlign HorizontalAlignment.RIGHT setVerticalAlign VerticalAlignment.TOP setX 0 setY 1000 setBorderWidth 0)

		model.getEbMSMessageTrafficChartOption.ebMSMessageTrafficChartSeries.foreach(status => {
			val receivedMessages = getMessages(dates.toList,model,status.ebMSMessageStatuses : _*)
			result.addSeries(new SimpleSeries() setName status.name setColor status.color setData receivedMessages)
		})
		
		result
	}

	private def getMessages(dates : List[Date], model : TrafficChartFormModel, statuses : EbMSMessageStatus*) : List[Number] =
	{
		val messageTraffic = ebMSDAO.selectMessageTraffic(model.from,new DateTime(model.from.getTime).plus(model.timeUnit.period).toDate,model.timeUnit,statuses : _*)
		for (date <- dates)
			yield (if (messageTraffic containsKey date) messageTraffic.get(date) getOrElse null else 0) : Number
	}

	override def getPageTitle : String = getLocalizer.getString("trafficChart",this)

	class TrafficChartForm(id : String, model : TrafficChartFormModel) extends Form[TrafficChartFormModel](id,new CompoundPropertyModel[TrafficChartFormModel](model))
	{
		add(createTimeUnitChoice("timeUnit"))
		add(createMinusLink("minus"))
		add(createPlusLink("plus"))
		chart = new Chart("chart",createOptions(model))
		add(chart)
		add(createEbMSMessageTrafficChartOptions("ebMSMessageTrafficChartOptions"))

		private def createTimeUnitChoice(id : String) =
			new DropDownChoice[TimeUnit](id,new PropertyModel[java.util.List[TimeUnit]](this.getModelObject,"timeUnits"))
			{
				override protected def localizeDisplayValues : Boolean = true
			}
			.setLabel(new ResourceModel("lbl.timeUnit"))
			.setRequired(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					val model = TrafficChartForm.this.getModelObject
					model setFrom model.getTimeUnit.getFrom.toDate
					chart setOptions createOptions(model)
					target add chart
				}
			})

		private def createMinusLink(id : String) : AjaxLink[Void] =
			new AjaxLink[Void](id)
			{
				override def onClick(target : AjaxRequestTarget)
				{
					val model = TrafficChartForm.this.getModelObject
					model.setFrom(new DateTime(model.getFrom.getTime).minus(model.getTimeUnit.period).toDate)
					chart.setOptions(createOptions(model))
					target.add(chart)
				}
			}

		private def createPlusLink(id : String) : AjaxLink[Void] =
			new AjaxLink[Void](id)
			{
				override def onClick(target : AjaxRequestTarget)
				{
					val model = TrafficChartForm.this.getModelObject
					model.setFrom(new DateTime(model.getFrom.getTime).plus(model.getTimeUnit.period).toDate)
					chart.setOptions(createOptions(model))
					target.add(chart)
				}
			}

		private def createEbMSMessageTrafficChartOptions(id : String) : DropDownChoice[EbMSMessageTrafficChartOption] =
			new DropDownChoice[EbMSMessageTrafficChartOption](id,new PropertyModel[EbMSMessageTrafficChartOption](this.getModelObject,"ebMSMessageTrafficChartOption"),new PropertyModel[java.util.List[EbMSMessageTrafficChartOption]](this.getModelObject,"ebMSMessageTrafficChartOptions"))
			{
				override protected def localizeDisplayValues : Boolean = true
			}
			.setLabel(new ResourceModel("lbl.ebMSMessageTrafficChartOption"))
			.setRequired(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					val model = TrafficChartForm.this.getModelObject
					chart setOptions createOptions(model)
					target.add(chart)
				}
			})
		  .asInstanceOf[DropDownChoice[EbMSMessageTrafficChartOption]]
	}

}
object TrafficChartPage
{
	def getTrafficChartFormModel(timeUnit : TimeUnit, ebMSMessageTrafficChartOption : EbMSMessageTrafficChartOption) : TrafficChartFormModel =
	{
		val from = timeUnit getFrom
		val to = from plus timeUnit.period
		new TrafficChartFormModel(timeUnit,from toDate,to toDate,ebMSMessageTrafficChartOption)
	}
}
class TrafficChartFormModel(
	@BeanProperty var timeUnit : TimeUnit,
	@BeanProperty var from : Date,
	@BeanProperty var to : Date,
	@BeanProperty var ebMSMessageTrafficChartOption : EbMSMessageTrafficChartOption) extends IClusterable
{
	def getTimeUnits : java.util.List[TimeUnit] = TimeUnit.values

	def getEbMSMessageTrafficChartOptions : java.util.List[EbMSMessageTrafficChartOption] = EbMSMessageTrafficChartOption.values
}
