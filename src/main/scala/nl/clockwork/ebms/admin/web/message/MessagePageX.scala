/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import nl.clockwork.ebms.Constants.EbMSEventStatus
import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.Constants
import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.EbMSEventLog
import nl.clockwork.ebms.admin.model.EbMSMessage
import nl.clockwork.ebms.admin.web.{WebMarkupContainer, _}
import org.apache.wicket.AttributeModifier
import org.apache.wicket.Component
import org.apache.wicket.IGenericComponent
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.ajax.markup.html.AjaxLink
import org.apache.wicket.datetime.markup.html.basic.DateLabel
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
import org.apache.wicket.markup.html.WebPage
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.form.{CheckBox, TextArea}
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.PropertyListView
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean

import scala.beans.BeanProperty
import scala.collection.JavaConverters._

class MessagePageX(val message : EbMSMessage, val responsePage : WebPage) extends BasePage with IGenericComponent[EbMSMessage]
{
	@SpringBean(name="ebMSAdminDAO") var ebMSDAO : EbMSDAO = _
	protected var showContent : Boolean = _
	protected var messageViewPanel : Panel = _
	@BeanProperty protected var rawOutput : Boolean = _

	setModel(new CompoundPropertyModel[EbMSMessage](message))
	add(new BootstrapFeedbackPanel("feedback"))
	add(new Label("messageId"))
	add(new Label("messageNr"))
	add(new Label("conversationId"))
	add(createViewRefToMessageIdLink("viewRefToMessageId",message))
	add(DateLabel.forDatePattern("timestamp",Constants.DATETIME_FORMAT))
	add(new Label("cpaId"))
	add(new Label("fromPartyId"))
	add(new Label("fromRole"))
	add(new Label("toPartyId"))
	add(new Label("toRole"))
	add(new Label("service"))
	add(new Label("action"))
	add(createViewMessageErrorLink("viewMessageError",message))
	add(new Label("statusTime"))
	add(createNextEventContainer("nextEvent",message))
	add(createEventLogContainer("eventLog",message))
	add(createRawOutputContainer("rawOutputContainer",message))
	messageViewPanel = createMessageViewPanel("attachments",message)
	add(messageViewPanel)
	add(new PageLink("back",responsePage))
	add(new DownloadEbMSMessageLink("download",ebMSDAO,message))
	val content = createContentField("content",message)
	add(content)
	add(createContentToggleLink("toggleContent",content))

	override def getPageTitle : String = getLocalizer.getString("message",this)

	class ErrorMessageModalWindow(val id : String, val errorMessage : String) extends ModalWindow(id)
	{
		setCssClassName(ModalWindow.CSS_CLASS_GRAY)
		setContent(new ErrorMessagePanel(this,Model.of(errorMessage)))
		setCookieName("eventError")
		setCloseButtonCallback(new ModalWindow.CloseButtonCallback
		{
			def onCloseButtonClicked(target : AjaxRequestTarget) : Boolean = true
		})

		override def getTitle : IModel[String] = new Model[String](getLocalizer.getString("eventError",this))
	}

	private def createViewRefToMessageIdLink(id : String, message : EbMSMessage) : Link[Void] =
		new Link[Void](id)
		{
			override def onClick = setResponsePage(new MessagePageX(ebMSDAO.findMessage(message.getRefToMessageId).get,MessagePageX.this))
		}
		.add(new Label("refToMessageId"))
	  .asInstanceOf[Link[Void]]

	private def createViewMessageErrorLink(id : String, message : EbMSMessage) : AjaxLink[Void] =
		new AjaxLink[Void](id)
		{
			override def onClick(target : AjaxRequestTarget) = setResponsePage(new MessagePageX(ebMSDAO.findResponseMessage(message.getMessageId).get,MessagePageX.this))
		}
		.add(new Label("status"))
		.setEnabled(EbMSMessageStatus.DELIVERY_FAILED.equals(message.getStatus))
		.add(AttributeModifier.replace("class",Model.of(Utils.getTableCellCssClass(message.getStatus))))
	  .asInstanceOf[AjaxLink[Void]]

	private def createNextEventContainer(id : String, message : EbMSMessage) : WebMarkupContainer =
	{
		val result = new WebMarkupContainer(id)
		result.setVisible(message.getEvent != null)
		if (message.getEvent != null)
		{
			result.add(DateLabel.forDatePattern("event.timestamp",Constants.DATETIME_FORMAT))
			result.add(new Label("event.retries"))
			result.add(DateLabel.forDatePattern("event.timeToLive",Constants.DATETIME_FORMAT))
		}
		result
	}

	private def createEventLogContainer(id : String, message : EbMSMessage) : WebMarkupContainer =
	{
		val result = new WebMarkupContainer(id)
		result.setVisible(message.getEvents.size > 0)
		val events = new PropertyListView[EbMSEventLog]("events",message.getEvents)
		{
			override protected def populateItem(item : ListItem[EbMSEventLog])
			{
				val errorMessageModalWindow = new ErrorMessageModalWindow("errorMessageWindow",item.getModelObject.errorMessage)
				item.add(DateLabel.forDatePattern("timestamp",Constants.DATETIME_FORMAT))
				item.add(new Label("uri"))
				item.add(errorMessageModalWindow)
				val link = new AjaxLink[Void]("showErrorMessageWindow")
				{
					def onClick(target : AjaxRequestTarget) = errorMessageModalWindow.show(target)
				}
				link.setEnabled(EbMSEventStatus.FAILED.equals(item.getModelObject.status))
				link.add(new Label("status"))
				item.add(link)
			}
		}
		result.add(events)
		result
	}

	private def createRawOutputContainer(id : String, message : EbMSMessage) : WebMarkupContainer =
	{
		val result = new WebMarkupContainer(id)
		{
			override def isVisible : Boolean = WicketApplication.get.getMessageViewPanels.containsKey(MessageProvider.createId(message.getService,message.getAction))
		}
		val rawOutput = new CheckBox("rawOutput",new PropertyModel[java.lang.Boolean](this,"rawOutput"))
		rawOutput.setLabel(new ResourceModel("lbl.rawOutput"))
		rawOutput.add(new AjaxFormComponentUpdatingBehavior("onchange")
		{
			override protected def onUpdate(target : AjaxRequestTarget)
			{
				if (getRawOutput)
				{
					val messageViewPanelNew = new AttachmentsPanel("attachments",message.getAttachments.asScala.toList)
					messageViewPanel.replaceWith(messageViewPanelNew)
				}
				else
				{
					try
					{
						val messageViewPanelNew = WicketApplication.get.getMessageViewPanels.get(MessageProvider.createId(message.getService,message.getAction))
						messageViewPanel.replaceWith(messageViewPanelNew.getPanel("attachments",message.getAttachments.asScala.toList))
					}
					catch
					{
						case e : Exception =>
							warn("Unable to view message for action" + MessageProvider.createId(message.getService,message.getAction) + ". " + e.getMessage)
							val messageViewPanelNew = new AttachmentsPanel("attachments",message.getAttachments.asScala.toList)
							messageViewPanel.replaceWith(messageViewPanelNew)
					}
				}
				target.add(getPage)
			}
		})
		result.add(rawOutput)
		result
	}

	private def createMessageViewPanel(id : String, message : EbMSMessage) : Panel =
	{
		val actionId = MessageProvider.createId(message getService,message getAction)
		if (WicketApplication.get.getMessageViewPanels containsKey actionId)
		{
			try
			{
				WicketApplication.get.getMessageViewPanels.get(actionId) getPanel(id,message.getAttachments.asScala.toList)
			}
			catch
			{
				case e : Exception =>
					warn("Unable to view message for action" + actionId + ". " + e.getMessage)
					new AttachmentsPanel(id,message.getAttachments.asScala.toList)
			}

		}
		else
			new AttachmentsPanel(id,message.getAttachments.asScala.toList)
	}

	private def createContentField(id : String, message : EbMSMessage) =
		new TextArea[String](id,Model of message.getContent)
		{
			override def isVisible : Boolean = showContent
		}
		.setOutputMarkupPlaceholderTag(true)
		.setEnabled(false)

	private def createContentToggleLink(id : String, content : Component) =
		new AjaxLink[String](id)
		{
			override def onClick(target : AjaxRequestTarget)
			{
				showContent = !showContent
				target.add(this)
				target.add(content)
			}
		}
		.add(new Label("label",new Model[String]
		{
			override def getObject : String = MessagePageX.this.getLocalizer.getString(if (showContent) "cmd.hide" else "cmd.show",MessagePageX.this)
		}))

	override def getModelObject : EbMSMessage = getDefaultModelObject.asInstanceOf[EbMSMessage]

	override def setModelObject(o : EbMSMessage) = setDefaultModelObject(o)

	override def getModel : IModel[EbMSMessage] = getDefaultModel.asInstanceOf[IModel[EbMSMessage]]

	override def setModel(model : IModel[EbMSMessage]) = setDefaultModel(model)

}
