/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import org.apache.wicket.Component
import org.apache.wicket.feedback.FeedbackMessage
import org.apache.wicket.feedback.IFeedbackMessageFilter
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.markup.html.panel.FeedbackPanel

class BootstrapAlertFeedbackPanel(val id : String, val filter : IFeedbackMessageFilter) extends FeedbackPanel(id,filter)
{
	sealed abstract class ErrorLevel(val errorCode : Int, val cssClass : String)
	object ErrorLevel
	{
		case object UNDEFINED extends ErrorLevel(0,"alert-info")
		case object DEBUG extends ErrorLevel(100,"alert-info")
		case object INFO extends ErrorLevel(200,"alert-info")
		case object SUCCESS extends ErrorLevel(250,"alert-success")
		case object WARNING extends ErrorLevel(300,"alert-warning")
		case object ERROR extends ErrorLevel(400,"alert-danger")
		case object FATAL extends ErrorLevel(500,"alert-danger")

		val values : List[ErrorLevel] = List(UNDEFINED,DEBUG,INFO,SUCCESS,WARNING,ERROR,FATAL)

		def getErrorLevel(errorCode : Int) : ErrorLevel = ErrorLevel.values.find(errorLevel => errorCode == errorLevel.errorCode).getOrElse(null)
	}

	def this(id : String) = this (id, null)

	override protected def getCSSClass(message : FeedbackMessage) : String = "alert " + ErrorLevel.getErrorLevel(message.getLevel).cssClass + " alert-dismissable"

	override protected def newMessageDisplayComponent(id : String, message : FeedbackMessage) : Component =
	{
		val result = new WebMarkupContainer(id)
		result.add(new Button("close"))
		val serializable = message.getMessage
		val label = new Label("content",if (serializable == null) "" else serializable.toString)
		label.setEscapeModelStrings(BootstrapAlertFeedbackPanel.this.getEscapeModelStrings)
		result.add(label)
		result
	}
}