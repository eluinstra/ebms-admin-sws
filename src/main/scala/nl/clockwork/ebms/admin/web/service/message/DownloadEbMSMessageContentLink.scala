/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.UUID
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.xml.bind.JAXBElement
import javax.xml.bind.JAXBException
import javax.xml.namespace.QName

import nl.clockwork.ebms.admin.web.Utils
import nl.clockwork.ebms.admin.web.message.ByteArrayResourceStream
import nl.clockwork.ebms.common.XMLMessageBuilder
import nl.clockwork.ebms.model.{EbMSDataSource, EbMSMessageContent, EbMSMessageContext}
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.model.Model
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler
import org.apache.wicket.request.resource.ContentDisposition
import org.apache.wicket.util.lang.Args
import org.apache.wicket.util.resource.IResourceStream

import scala.collection.JavaConversions._

import resource._

class DownloadEbMSMessageContentLink(val id : String, val messageContent : nl.clockwork.ebms.model.EbMSMessageContent) extends Link[EbMSMessageContent](id,Model of Args.notNull(messageContent,"messageContent"))
{
	@transient val logger = LogFactory getLog getClass

	override def onClick
	{
		try
		{
			val messageContent = getModelObject
			val output = new ByteArrayOutputStream
			managed(new ZipOutputStream(output)).foreach(zip => writeMessageToZip(messageContent,zip))
			val resourceStream = new ByteArrayResourceStream(output,"application/zip")
			getRequestCycle.scheduleRequestHandlerAfterCurrent(createRequestHandler(messageContent,resourceStream))
		}
		catch
		{
			case e @ (_ : IOException | _ : JAXBException) => logger error("",e); error(e getMessage)
		}
	}

	private def writeMessageToZip(messageContent : EbMSMessageContent, zip : ZipOutputStream)
	{
		val entry = new ZipEntry("messageContext.xml")
		zip putNextEntry entry
		zip write(XMLMessageBuilder.getInstance(classOf[EbMSMessageContext]).handle(new JAXBElement[EbMSMessageContext](new QName("http://www.clockwork.nl/ebms/2.0","messageContext"),classOf[EbMSMessageContext],messageContent getContext)) getBytes)
		zip.closeEntry
		messageContent.getDataSources.foreach(dataSource =>
		{
			val entry = new ZipEntry("datasources/" + (if (dataSource.getName isEmpty) UUID.randomUUID + Utils.getFileExtension(dataSource getContentType) else dataSource getName))
			entry.setComment("Content-Type: " + dataSource.getContentType)
			zip.putNextEntry(entry)
			zip.write(dataSource.getContent)
			zip.closeEntry
		})
	}

	private def createRequestHandler(messageContent : EbMSMessageContent, resourceStream : IResourceStream) : ResourceStreamRequestHandler =
		new ResourceStreamRequestHandler(resourceStream) setFileName("messageContent." + messageContent.getContext.getMessageId + ".zip")  setContentDisposition ContentDisposition.ATTACHMENT
}
