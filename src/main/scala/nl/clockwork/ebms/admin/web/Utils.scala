/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.net.URLConnection

import nl.clockwork.ebms.Constants.EbMSMessageStatus

import org.apache.commons.lang.StringUtils
import org.apache.wicket.resource.loader.IStringResourceLoader

import scala.collection.JavaConversions._

object Utils
{
	def getResourceString(`class` : Class[_], propertyName : String) : String =
	{
		val loaders = WicketApplication.get.getResourceSettings.getStringResourceLoaders
		//loaders.map(loader => loader.loadStringResource(`class`,propertyName,null,null,null)).filter(value => StringUtils.isNotBlank(value)).headOption.getOrElse(propertyName)
		for (loader : IStringResourceLoader <- loaders)
		{
			val value = loader.loadStringResource(`class`,propertyName,null,null,null)
			if (StringUtils.isNotBlank(value))
				value
		}
		propertyName
	}

	def getContentType(pathInfo : String) : String = Option(URLConnection.guessContentTypeFromName(pathInfo)).getOrElse("application/octet-stream")
		//Option(new MimetypesFileTypeMap.getContentType(pathInfo)).getOrElse("application/octet-stream")
		//Option(URLConnection.getFileNameMap.getContentTypeFor(pathInfo)).getOrElse("application/octet-stream")

	def getFileExtension(contentType : String) : String = if (StringUtils.isEmpty(contentType)) "" else "." + (if (contentType.contains("text")) "txt" else contentType.split("/")(1))

	def getTableCellCssClass(ebMSMessageStatus : EbMSMessageStatus) : String = "text-" + getTableRowCssClass(ebMSMessageStatus)

	def getTableRowCssClass(ebMSMessageStatus : EbMSMessageStatus) : String =
	{
		ebMSMessageStatus match
		{
			case EbMSMessageStatus.PROCESSED | EbMSMessageStatus.FORWARDED | EbMSMessageStatus.DELIVERED => "success"
			case EbMSMessageStatus.RECEIVED | EbMSMessageStatus.SENT => "warning"
			case EbMSMessageStatus.UNAUTHORIZED | EbMSMessageStatus.NOT_RECOGNIZED | EbMSMessageStatus.FAILED | EbMSMessageStatus.DELIVERY_FAILED | EbMSMessageStatus.EXPIRED => "danger"
			case _ => null
		}
	}

}
