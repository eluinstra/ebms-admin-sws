/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.io.IOException
import java.util.ArrayList
import java.util.List

import nl.clockwork.ebms.admin.PropertyPlaceholderConfigurer
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.BootstrapPanelBorder
import nl.clockwork.ebms.admin.web.ResetButton
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.{Button, Form}
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.ListView
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class EbMSCorePropertiesPage(ebMSCorePropertiesFormModel : EbMSCorePropertiesFormModel) extends BasePage
{
	@transient def logger = LogFactory getLog getClass
	@SpringBean(name="propertyConfigurer") private var propertyPlaceholderConfigurer : PropertyPlaceholderConfigurer = _

	add(new BootstrapFeedbackPanel("feedback"))
	add(new EbMSCorePropertiesForm("form",ebMSCorePropertiesFormModel))

	def this() = this(new EbMSCorePropertiesFormModel)

	override def getPageTitle : String = getLocalizer.getString("ebMSCoreProperties",this)

	private class ComponentsListView(val id : String, val list : List[_ <: BootstrapPanelBorder]) extends ListView[BootstrapPanelBorder](id,list)
	{
		setReuseItems(true)

		override protected def populateItem(item : ListItem[BootstrapPanelBorder]) = item.add(item.getModelObject)
	}

	class EbMSCorePropertiesForm(val id : String, val model : EbMSCorePropertiesFormModel) extends Form[EbMSCorePropertiesFormModel](id,new CompoundPropertyModel[EbMSCorePropertiesFormModel](model))
	{
		val components = new ArrayList[BootstrapPanelBorder]
		components.add(new BootstrapPanelBorder("panelBorder",EbMSCorePropertiesPage.this.getString("coreProperties"),new CorePropertiesFormPanel("component",new PropertyModel[CorePropertiesFormModel](getModelObject,"coreProperties"))))
		components.add(new BootstrapPanelBorder("panelBorder",EbMSCorePropertiesPage.this.getString("httpProperties"),new HttpPropertiesFormPanel("component",new PropertyModel[HttpPropertiesFormModel](getModelObject,"httpProperties"))))
		components.add(new BootstrapPanelBorder("panelBorder",EbMSCorePropertiesPage.this.getString("signatureProperties"),new SignaturePropertiesFormPanel("component",new PropertyModel[SignaturePropertiesFormModel](getModelObject,"signatureProperties"))))
		components.add(new BootstrapPanelBorder("panelBorder",EbMSCorePropertiesPage.this.getString("jdbcProperties"),new JdbcPropertiesFormPanel("component",new PropertyModel[JdbcPropertiesFormModel](getModelObject,"jdbcProperties"))))
		add(new ComponentsListView("components",components))
		add(createValidateButton("validate"))
		add(new DownloadEbMSCorePropertiesButton("download",new ResourceModel("cmd.download"),getModelObject))
		add(new ResetButton("reset",new ResourceModel("cmd.reset"),classOf[EbMSCorePropertiesPage]))

		private def createValidateButton(id : String) : Button =
		{
			new Button(id)
			{
				override def onSubmit = info(EbMSCorePropertiesPage.this.getString("validate.ok"))
			}
		}
	}
}
class EbMSCorePropertiesFormModel(
	 @BeanProperty val coreProperties : CorePropertiesFormModel = new CorePropertiesFormModel,
	 @BeanProperty val httpProperties : HttpPropertiesFormModel = new HttpPropertiesFormModel,
	 @BeanProperty val signatureProperties : SignaturePropertiesFormModel = new SignaturePropertiesFormModel,
	 @BeanProperty val jdbcProperties : JdbcPropertiesFormModel = new JdbcPropertiesFormModel) extends IClusterable
