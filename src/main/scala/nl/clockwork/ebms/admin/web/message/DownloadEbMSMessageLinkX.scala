/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

import nl.clockwork.ebms.admin.model.EbMSMessage
import nl.clockwork.ebms.admin.web.Utils
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.model.Model
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler
import org.apache.wicket.request.resource.ContentDisposition
import org.apache.wicket.util.lang.Args
import org.apache.wicket.util.resource.IResourceStream

import scala.collection.JavaConversions._
import resource._

class DownloadEbMSMessageLinkX(id : String, message : EbMSMessage) extends Link[EbMSMessage](id,Model.of(Args.notNull(message,"message")))
{
	@transient val logger = LogFactory getLog getClass

	override def onClick
	{
		try
		{
			val message = getModelObject
			val output = new ByteArrayOutputStream
			managed (new ZipOutputStream(output)).foreach(zip =>
			{
				writeMessageToZip(message,zip)
			})
			val resourceStream = new ByteArrayResourceStream(output,"application/zip")
			getRequestCycle scheduleRequestHandlerAfterCurrent createRequestHandler(message,resourceStream)
		}
		catch
		{
			case e : IOException => logger error("",e); error(e getMessage)
		}
	}

	private def writeMessageToZip(message : EbMSMessage, zip : ZipOutputStream)
	{
		val entry = new ZipEntry("message.xml")
		zip.putNextEntry(entry)
		zip.write(message.getContent.getBytes)
		zip.closeEntry
		message.getAttachments.foreach(attachment =>
		{
			val entry = new ZipEntry("attachments/" + (if (attachment.getName isEmpty) attachment.getContentId + Utils.getFileExtension(attachment getContentType) else attachment getName))
			entry.setComment("Content-Type: " + attachment.getContentType)
			zip.putNextEntry(entry)
			zip.write(attachment.getContent)
			zip.closeEntry
		})
	}

	private def createRequestHandler(message : EbMSMessage, resourceStream : IResourceStream) : ResourceStreamRequestHandler =
		new ResourceStreamRequestHandler(resourceStream) setFileName("message." + message.getMessageId + "." + message.getMessageNr + ".zip") setContentDisposition ContentDisposition.ATTACHMENT
}
