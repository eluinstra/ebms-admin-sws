/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration


//	sealed abstract class PropertiesType(val filename : String)
sealed trait PropertiesType{def filename : String}
object PropertiesType
{
//	case object EBMS_ADMIN extends PropertiesType("ebms-admin.properties")
//	case object EBMS_ADMIN_EMBEDDED extends PropertiesType("ebms-admin.embedded.properties")
//	case object EBMS_CORE extends PropertiesType("ebms-core.properties")

	case object EBMS_ADMIN extends PropertiesType{val filename = "ebms-admin.properties"}
	case object EBMS_ADMIN_EMBEDDED extends PropertiesType{val filename = "ebms-admin.embedded.properties"}
	case object EBMS_CORE extends PropertiesType{val filename = "ebms-core.properties"}

	def getPropertiesType(propertiesFile : String) : PropertiesType =
	{
		propertiesFile match
		{
			case EBMS_ADMIN.filename => EBMS_ADMIN
			case EBMS_ADMIN_EMBEDDED.filename => EBMS_ADMIN_EMBEDDED
			case EBMS_CORE.filename => EBMS_CORE
			case _ => null
		}
	}
}

sealed abstract class JdbcDriver(val driverClassName : String, val urlExpr : String , val preferredTestQuery : String)
{
	def createJdbcURL(hostname: String, port : Int, database : String) : String =
	{
		JdbcDriver.createJdbcURL(urlExpr,hostname,port,database)
	}
}
object JdbcDriver
{
	case object HSQLDB extends JdbcDriver("org.hsqldb.jdbcDriver", "jdbc:hsqldb:hsql://%s/%s", "select 1 from information_schema.system_tables")

	case object MYSQL extends JdbcDriver("com.mysql.jdbc.Driver", "jdbc:mysql://%s/%s", "select 1")

	case object MARIADB extends JdbcDriver("org.mariadb.jdbc.Driver", "jdbc:mysql://%s/%s", "select 1")

	case object POSTGRESQL extends JdbcDriver("org.postgresql.Driver", "jdbc:postgresql://%s/%s", "select 1")

	case object MSSQL extends JdbcDriver("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://%sdatabaseName=%s", "select 1")

	case object MSSQL_JTDS extends JdbcDriver("net.sourceforge.jtds.jdbc.Driver", "jdbc:jtds:sqlserver://%s/%s", "select 1")

	case object ORACLE extends JdbcDriver("oracle.jdbc.OracleDriver", "jdbc:oracle:thin:@//%s/%s", "select 1 from dual")

	case object ORACLE_ extends JdbcDriver("oracle.jdbc.OracleDriver", "jdbc:oracle:thin:@%s:%s", "select 1 from dual")

	val values = List(HSQLDB,MYSQL,MARIADB,POSTGRESQL,MSSQL,MSSQL_JTDS,ORACLE,ORACLE_)

	def createJdbcURL(urlExpr : String, hostname : String, port : Int, database : String) : String =
	{
		String.format(urlExpr,Utils.createURL(hostname,port),database)
	}
	def getJdbcDriver(driverClassName: String): JdbcDriver =
	{
		driverClassName match
		{
			case HSQLDB.driverClassName => HSQLDB
			case MYSQL.driverClassName => MYSQL
			case MARIADB.driverClassName => MARIADB
			case POSTGRESQL.driverClassName => POSTGRESQL
			case MSSQL.driverClassName => MSSQL
			case MSSQL_JTDS.driverClassName => MSSQL_JTDS
			case ORACLE.driverClassName => ORACLE
			case ORACLE_.driverClassName => ORACLE_
			case _ => null
		}
	}
}
