/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import java.io.ByteArrayInputStream
import java.io.InputStream

import nl.clockwork.ebms.admin.model.EbMSAttachment
import org.apache.wicket.util.lang.Bytes
import org.apache.wicket.util.resource.AbstractResourceStream

class AttachmentResourceStream(val attachment : EbMSAttachment) extends AbstractResourceStream
{
	override def getContentType : String = attachment.getContentType

	override def length : Bytes = Bytes.bytes(attachment.getContent.length)

	override def getInputStream : InputStream =  new ByteArrayInputStream(attachment.getContent)

	override def close {}
}
object AttachmentResourceStream
{
	def apply(attachment : EbMSAttachment) : AttachmentResourceStream = this(attachment)
}