/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.io.File
import java.io.FileReader
import java.io.IOException

import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.model.Model
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel

class LoadEbMSAdminPropertiesButton(val id : String, val resourceModel : ResourceModel, val ebMSAdminPropertiesFormModel : EbMSAdminPropertiesFormModel, val propertiesType : PropertiesType) extends Button(id,resourceModel)
{
	@transient protected val logger = LogFactory getLog getClass

	setDefaultFormProcessing(false)

	override def isEnabled : Boolean = new File(propertiesType.filename).exists

	override def onSubmit
	{
		try
		{
			val file = new File(propertiesType.filename)
			val reader = new FileReader(file)
			new EbMSAdminPropertiesReader(reader).read(ebMSAdminPropertiesFormModel,propertiesType)
			val page = new EbMSAdminPropertiesPage(ebMSAdminPropertiesFormModel)
			page.info(new StringResourceModel("properties.loaded",page,Model of file).getString)
			setResponsePage(page)
		}
		catch
		{
			case e : IOException => logger error("",e); error(e getMessage)
		}
	}
}
