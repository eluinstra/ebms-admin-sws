/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.util.List

import nl.clockwork.ebms.admin.Constants
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.PageLink
import nl.clockwork.ebms.model.EbMSDataSource
import nl.clockwork.ebms.model.EbMSMessageContent
import nl.clockwork.ebms.service.EbMSMessageService

import org.apache.commons.logging.LogFactory
import org.apache.wicket.AttributeModifier
import org.apache.wicket.IGenericComponent
import org.apache.wicket.datetime.markup.html.basic.DateLabel
import org.apache.wicket.markup.html.WebPage
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.PropertyListView
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.spring.injection.annot.SpringBean

class MessagePage(val messageContent : EbMSMessageContent, val responsePage : WebPage) extends BasePage with IGenericComponent[EbMSMessageContent]
{
	private class EbMSDataSourcePropertyListView(val id : String, val list : List[_ <: EbMSDataSource]) extends PropertyListView[EbMSDataSource](id,list)
	{
		private var i = 1

		override protected def populateItem(item : ListItem[EbMSDataSource])
		{
			val dataSource = item.getModelObject()
			if (dataSource.getName.isEmpty)
			{
				dataSource.setName("dataSource." + i)
				i += 1
			}
			item add(new DownloadEbMSDataSourceLink("downloadDataSource",dataSource))
			item add(new Label("contentType"))
		}
	}

	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="ebMSMessageService") var ebMSMessageService : EbMSMessageService = _

	setModel(new CompoundPropertyModel[EbMSMessageContent](messageContent))
	add(new BootstrapFeedbackPanel("feedback"))
	add(new Label("context.messageId"))
	add(new Label("context.conversationId"))
	add(createViewRefToMessageIdLink("viewRefToMessageId",messageContent))
	add(DateLabel.forDatePattern("context.timestamp",Constants.DATETIME_FORMAT))
	add(new Label("context.cpaId"))
	add(new Label("context.fromRole.partyId"))
	add(new Label("context.fromRole.role"))
	add(new Label("context.toRole.partyId"))
	add(new Label("context.toRole.role"))
	add(new Label("context.service"))
	add(new Label("context.action"))
	add(new EbMSDataSourcePropertyListView("dataSources",messageContent.getDataSources))
	add(new PageLink("back",responsePage))
	add(new DownloadEbMSMessageContentLink("download",messageContent))
	add(createProcessLink("process",messageContent,responsePage))

	override def getPageTitle : String = getLocalizer.getString("message",this)

	private def createViewRefToMessageIdLink(id : String, messageContent : EbMSMessageContent) : Link[Void] =
		new Link[Void](id)
		{
			override def onClick = setResponsePage(new MessagePage(ebMSMessageService.getMessage(messageContent.getContext.getRefToMessageId,null),MessagePage.this))
		}
		.add(new Label("context.refToMessageId"))
	  .asInstanceOf[Link[Void]]

	private def createProcessLink(id : String, messageContent : EbMSMessageContent, responsePage :  WebPage) : Link[Void] =
		new Link[Void](id)
		{
			override def onClick
			{
				try
				{
					ebMSMessageService.processMessage(messageContent.getContext.getMessageId)
					setResponsePage(responsePage)
				}
				catch
				{
					case e : Exception => logger error("",e); error(e getMessage)
				}
			}
		}
		.add(AttributeModifier.replace("onclick","confirm('" + getLocalizer.getString("confirm",this) + "')"))
	  .asInstanceOf[Link[Void]]

	override def getModelObject : EbMSMessageContent = getDefaultModelObject.asInstanceOf[EbMSMessageContent]

	override def setModelObject(o : EbMSMessageContent) = setDefaultModelObject(o)

	override def getModel : IModel[EbMSMessageContent] = getDefaultModel.asInstanceOf[IModel[EbMSMessageContent]]

	override def setModel(model: IModel[EbMSMessageContent]) = setDefaultModel(model)
}
