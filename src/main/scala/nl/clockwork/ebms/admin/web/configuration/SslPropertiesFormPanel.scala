/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.util.ArrayList
import java.util.List

import nl.clockwork.ebms.admin.web.WebMarkupContainer
import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.html.form.{CheckBox, CheckBoxMultipleChoice, Form}
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class SslPropertiesFormPanel(val id : String, val model : IModel[SslPropertiesFormModel]) extends Panel(id,model)
{
	@transient val logger = LogFactory getLog getClass

	add(new SslPropertiesForm("form",model))

	class SslPropertiesForm(val id : String, val model : IModel[SslPropertiesFormModel]) extends Form[SslPropertiesFormModel](id,new CompoundPropertyModel[SslPropertiesFormModel](model))
	{
		add(createOverrideDefaultProtocolsCheckBox("overrideDefaultProtocols"))
		add(createEnabledProtocolsContainer("enabledProtocolsContainer"))
		add(createOverrideDefaultCipherSuitesCheckBox("overrideDefaultCipherSuites"))
		add(createEnabledCipherSuitesContainer("enabledCipherSuitesContainer"))
		add(new CheckBox("requireClientAuthentication").setLabel(new ResourceModel("lbl.requireClientAuthentication")))
		add(new CheckBox("verifyHostnames").setLabel(new ResourceModel("lbl.verifyHostnames")))
		add(new CheckBox("validate").setLabel(new ResourceModel("lbl.validate")))
		add(KeystorePropertiesFormPanel("keystoreProperties",new PropertyModel[JavaKeyStorePropertiesFormModel](getModelObject,"keystoreProperties")))
		add(TruststorePropertiesFormPanel("truststoreProperties",new PropertyModel[JavaKeyStorePropertiesFormModel](getModelObject,"truststoreProperties")))

		private def createOverrideDefaultProtocolsCheckBox(id : String) : CheckBox =
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.overrideDefaultProtocols"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(SslPropertiesForm.this)
			})
		  .asInstanceOf[CheckBox]

		private def createEnabledProtocolsContainer(id : String) : WebMarkupContainer =
			new WebMarkupContainer(id)
			{
				override def isVisible : Boolean = getModelObject.overrideDefaultProtocols
			}
			.add(new CheckBoxMultipleChoice[String]("enabledProtocols",getModelObject getSupportedProtocols) setLabel new ResourceModel("lbl.enabledProtocols"))
		  .asInstanceOf[WebMarkupContainer]

		private def createOverrideDefaultCipherSuitesCheckBox(id : String) : CheckBox =
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.overrideDefaultCipherSuites"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget) = target.add(SslPropertiesForm.this)
			})
		  .asInstanceOf[CheckBox]

		private def createEnabledCipherSuitesContainer(id : String) : WebMarkupContainer =
			new WebMarkupContainer(id)
			{
				override def isVisible : Boolean = getModelObject.overrideDefaultCipherSuites
			}
			.add(new CheckBoxMultipleChoice[String]("enabledCipherSuites",getModelObject getSupportedCipherSuites) setLabel new ResourceModel("lbl.enabledCipherSuites"))
		  .asInstanceOf[WebMarkupContainer]
	}
}
object SslPropertiesFormPanel
{
	def apply(id : String, model : IModel[SslPropertiesFormModel]) : SslPropertiesFormPanel = new SslPropertiesFormPanel(id,model)
}

class SslPropertiesFormModel(
	@BeanProperty var overrideDefaultProtocols : Boolean = false,
	@BeanProperty var supportedProtocols : List[String] = Utils.getSupportedSSLProtocols.toList,
	@BeanProperty var enabledProtocols : List[String] = new ArrayList[String],
	@BeanProperty var overrideDefaultCipherSuites : Boolean = true,
	@BeanProperty var supportedCipherSuites : List[String] = Utils.getSupportedSSLCipherSuites.toList,
	@BeanProperty var enabledCipherSuites : List[String] = new ArrayList[String](Array[String]("TLS_DHE_RSA_WITH_AES_128_CBC_SHA","TLS_RSA_WITH_AES_128_CBC_SHA").toList),
	@BeanProperty var requireClientAuthentication : Boolean = true,
	@BeanProperty var verifyHostnames : Boolean = false,
	@BeanProperty var validate : Boolean = true,
	@BeanProperty var keystoreProperties : JavaKeyStorePropertiesFormModel = new JavaKeyStorePropertiesFormModel,
	@BeanProperty var truststoreProperties : JavaKeyStorePropertiesFormModel = new JavaKeyStorePropertiesFormModel) extends IClusterable
