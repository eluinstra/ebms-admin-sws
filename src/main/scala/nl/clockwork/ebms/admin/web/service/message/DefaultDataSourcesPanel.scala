/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.util.ArrayList
import java.util.List

import nl.clockwork.ebms.model.EbMSDataSource
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.markup.html.form.AjaxButton
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.ListView
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class DefaultDataSourcesPanel(id : String) extends DataSourcesPanel(id)
{
	private class EbMSDataSourceListView(val id : String, val dataSourcesForm : DataSourcesForm) extends ListView[EbMSDataSource](id)
	{
		setOutputMarkupId(true)

		override protected def populateItem(item : ListItem[EbMSDataSource])
		{
			item setModel(new CompoundPropertyModel[EbMSDataSource](item getModelObject))
			item add(new Label("name"))
			item add(new Label("contentType"))
			item add(new AjaxButton("remove",new ResourceModel("cmd.remove"),dataSourcesForm)
			{
				override protected def onSubmit(target : AjaxRequestTarget, form : Form[_])
				{
					dataSourcesForm.getModelObject.dataSources.remove(item getModelObject)
					target add(dataSourcesForm)
				}
			})
		}
	}

	add(new DataSourcesForm("form"))

	class DataSourcesForm(id : String) extends Form[DataSourcesModel](id,new CompoundPropertyModel[DataSourcesModel](new DataSourcesModel))
	{
		add(new EbMSDataSourceListView("dataSources", DataSourcesForm.this))
		val dataSourceModalWindow = new DataSourceModalWindow("dataSourceModelWindow", getModelObject.dataSources, DataSourcesForm.this)
		add(dataSourceModalWindow)
		add(createAddButton("add", dataSourceModalWindow))
	}

	private def createAddButton(id : String, dataSourceModalWindow : ModalWindow) : AjaxButton =
		new AjaxButton(id)
		{
			override protected def onSubmit(target : AjaxRequestTarget, form : Form[_]) : Unit = dataSourceModalWindow.show(target)
		}

	override def getDataSources : List[EbMSDataSource] = this.get("form").asInstanceOf[DataSourcesForm].getModelObject.dataSources

	class DataSourcesModel(@BeanProperty val dataSources : List[EbMSDataSource] = new ArrayList[EbMSDataSource]) extends IClusterable
}