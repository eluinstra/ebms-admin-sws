/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import nl.clockwork.ebms.admin.PropertyPlaceholderConfigurer
import nl.clockwork.ebms.admin.web.configuration.EbMSAdminPropertiesPage
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.link.BookmarkablePageLink
import org.apache.wicket.request.mapper.parameter.PageParameters
import org.apache.wicket.spring.injection.annot.SpringBean

class HomePage(override val parameters : PageParameters) extends BasePage(parameters)
{
	@SpringBean(name="propertyConfigurer") var propertyPlaceholderConfigurer : PropertyPlaceholderConfigurer = _

	val file = propertyPlaceholderConfigurer.getOverridePropertiesFile
	add(
	  new org.apache.wicket.markup.html.WebMarkupContainer("configurationFile.found")
	  add(new Label("configurationFile", file.getFile getAbsolutePath))
	  setVisible file.exists)
	add(
	  new org.apache.wicket.markup.html.WebMarkupContainer("configurationFile.notFound")
	  add(
	    new Label("configurationFile", file.getFile getAbsolutePath),
	    new BookmarkablePageLink[Void]("configurationPageLink", classOf[EbMSAdminPropertiesPage]))
	  setVisible !file.exists)

	override def getPageTitle : String =
	{
		getLocalizer().getString("home",this)
	}

}
