/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.io.Reader
import java.util.Properties

import org.apache.commons.lang.StringUtils

import scala.collection.JavaConversions._

class EbMSCorePropertiesReader(val reader : Reader)
{
	def read(ebMSCoreProperties : EbMSCorePropertiesFormModel)
	{
		val properties = new Properties
		properties.load(reader)
		read(properties,ebMSCoreProperties.coreProperties)
		read(properties,ebMSCoreProperties.httpProperties)
		read(properties,ebMSCoreProperties.signatureProperties)
		read(properties,ebMSCoreProperties.jdbcProperties)
	}
	
	protected def read(properties : Properties, coreProperties : CorePropertiesFormModel)
	{
		coreProperties.digipoortPatch = properties.getProperty("patch.digipoort.enable").toBoolean
		coreProperties.oraclePatch = properties.getProperty("patch.oracle.enable").toBoolean
		coreProperties.cleoPatch = properties.getProperty("patch.cleo.enable").toBoolean
	}

	protected def read(properties : Properties, httpProperties : HttpPropertiesFormModel)
	{
		httpProperties.host = properties.getProperty("ebms.host")
		httpProperties.port = if (properties.getProperty("ebms.port") == null) 0 else properties.getProperty("ebms.port").toInt
		httpProperties.path = properties.getProperty("ebms.path")
		httpProperties.ssl = properties.getProperty("ebms.ssl").toBoolean
		httpProperties.proxy = !StringUtils.isEmpty(properties.getProperty("http.proxy.host"))
		httpProperties.chunkedStreamingMode = properties.getProperty("http.chunkedStreamingMode").toBoolean
		httpProperties.base64Writer = properties.getProperty("http.base64Writer").toBoolean
		if (httpProperties.ssl)
			read(properties,httpProperties.sslProperties)
		if (httpProperties.proxy)
			read(properties,httpProperties.proxyProperties)
	}

	protected def read(properties : Properties, sslProperties : SslPropertiesFormModel)
	{
		sslProperties.overrideDefaultProtocols = !StringUtils.isEmpty(properties.getProperty("https.enabledProtocols"))
		sslProperties.enabledProtocols = properties.getProperty("https.enabledProtocols","").split(',').toList
		sslProperties.enabledProtocols.foreach(s => s.trim);
		sslProperties.overrideDefaultCipherSuites = !StringUtils.isEmpty(properties.getProperty("https.enabledCipherSuites"))
		sslProperties.enabledCipherSuites = properties.getProperty("https.enabledCipherSuites","").split(',').toList
		sslProperties.enabledCipherSuites.foreach(s => s.trim)
		sslProperties.requireClientAuthentication = properties.getProperty("https.requireClientAuthentication").toBoolean
		sslProperties.verifyHostnames = properties.getProperty("https.verifyHostnames").toBoolean
		sslProperties.validate = properties.getProperty("https.validate").toBoolean
		sslProperties.keystoreProperties.uri = properties.getProperty("keystore.path")
		sslProperties.keystoreProperties.password = properties.getProperty("keystore.password")
		sslProperties.truststoreProperties.uri = properties.getProperty("truststore.path")
		sslProperties.truststoreProperties.password = properties.getProperty("truststore.password")
	}

	protected def read(properties : Properties, proxyProperties : ProxyPropertiesFormModel)
	{
		proxyProperties.host = Option(properties.getProperty("http.proxy.host")).getOrElse("")
		proxyProperties.port = properties.getProperty("http.proxy.port").toInt
		proxyProperties.nonProxyHosts = Option(properties.getProperty("http.proxy.nonProxyHosts")).getOrElse("")
		proxyProperties.username = properties.getProperty("http.proxy.username")
		proxyProperties.password = properties.getProperty("http.proxy.password")
	}

	protected def read(properties : Properties, signatureProperties : SignaturePropertiesFormModel)
	{
		if (signatureProperties.signing)
		{
			signatureProperties.keystoreProperties.uri = properties.getProperty("signature.keystore.path")
			signatureProperties.keystoreProperties.password = properties.getProperty("signature.keystore.password")
		}
	}

	protected def read(properties : Properties, jdbcProperties : JdbcPropertiesFormModel)
	{
		jdbcProperties.driver = JdbcDriver.getJdbcDriver(properties.getProperty("ebms.jdbc.driverClassName"))
		//jdbcProperties.jdbcURL = properties.getProperty("ebms.jdbc.url")
		Utils.parseJdbcURL(properties.getProperty("ebms.jdbc.url"),jdbcProperties)
		jdbcProperties.username = properties.getProperty("ebms.jdbc.username")
		jdbcProperties.password = properties.getProperty("ebms.jdbc.password")
		//jdbcProperties.preferredTestQuery = properties.getProperty("ebms.pool.preferredTestQuery")
	}

}
