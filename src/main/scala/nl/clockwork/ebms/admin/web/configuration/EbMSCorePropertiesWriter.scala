/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
package nl.clockwork.ebms.admin.web.configuration

import java.io.Writer
import java.util.Properties

import scala.collection.JavaConversions._

class EbMSCorePropertiesWriter(val writer : Writer)
{
	def write(ebMSCoreProperties : EbMSCorePropertiesFormModel)
	{
		val p = new Properties
		write(p,ebMSCoreProperties.coreProperties)
		write(p,ebMSCoreProperties.httpProperties)
		write(p,ebMSCoreProperties.signatureProperties)
		write(p,ebMSCoreProperties.jdbcProperties)
		p.store(writer,"EbMS Core properties")
	}

  protected def write(properties : Properties, coreProperties : CorePropertiesFormModel)
  {
		properties.setProperty("patch.digipoort.enable",coreProperties.digipoortPatch.toString)
		properties.setProperty("patch.oracle.enable",coreProperties.oraclePatch.toString)
		properties.setProperty("patch.cleo.enable",coreProperties.cleoPatch.toString)
  }

	protected def write(properties : Properties, httpProperties : HttpPropertiesFormModel)
  {
		properties.setProperty("ebms.host",httpProperties.host)
		properties.setProperty("ebms.port",if (httpProperties.port == 0) "" else httpProperties.port.toString)
		properties.setProperty("ebms.path",httpProperties.path)
		properties.setProperty("ebms.ssl",httpProperties.ssl.toString)
		properties.setProperty("http.chunkedStreamingMode",httpProperties.chunkedStreamingMode.toString)
		properties.setProperty("http.base64Writer",httpProperties.base64Writer.toString)
		if (httpProperties.ssl)
			write(properties,httpProperties.sslProperties)
		if (httpProperties.proxy)
			write(properties,httpProperties.proxyProperties)
  }

	protected def write(properties : Properties, sslProperties : SslPropertiesFormModel)
  {
		if (sslProperties.overrideDefaultProtocols)
			properties.setProperty("https.enabledProtocols",sslProperties.enabledProtocols.mkString(","))
		if (sslProperties.overrideDefaultCipherSuites)
			properties.setProperty("https.enabledCipherSuites",sslProperties.enabledCipherSuites.mkString(","))
		properties.setProperty("https.requireClientAuthentication",sslProperties.requireClientAuthentication.toString)
		properties.setProperty("https.verifyHostnames",sslProperties.verifyHostnames.toString)
		properties.setProperty("https.validate",sslProperties.validate.toString)
 		properties.setProperty("keystore.path",Option(sslProperties.keystoreProperties.uri).getOrElse(""))
 		properties.setProperty("keystore.password",Option(sslProperties.keystoreProperties.password).getOrElse(""))
 		properties.setProperty("truststore.path",Option(sslProperties.truststoreProperties.uri).getOrElse(""))
 		properties.setProperty("truststore.password",Option(sslProperties.truststoreProperties.password).getOrElse(""))
  }

	protected def write(properties : Properties, proxyProperties : ProxyPropertiesFormModel)
  {
		properties.setProperty("http.proxy.host",Option(proxyProperties.host).getOrElse(""))
		properties.setProperty("http.proxy.port",if (proxyProperties.port == 0) "80" else proxyProperties.port.toString)
		properties.setProperty("http.proxy.nonProxyHosts",Option(proxyProperties.nonProxyHosts).getOrElse(""))
 		properties.setProperty("http.proxy.username",Option(proxyProperties.username).getOrElse(""))
 		properties.setProperty("http.proxy.password",Option(proxyProperties.password).getOrElse(""))
  }

	protected def write(properties : Properties, signatureProperties : SignaturePropertiesFormModel)
  {
  	if (signatureProperties.signing)
  	{
  		properties.setProperty("signature.keystore.path",Option(signatureProperties.keystoreProperties.uri).getOrElse(""))
  		properties.setProperty("signature.keystore.password",Option(signatureProperties.keystoreProperties.password).getOrElse(""))
  	}
  }

	protected def write(properties : Properties, jdbcProperties : JdbcPropertiesFormModel)
  {
		properties.setProperty("ebms.jdbc.driverClassName",jdbcProperties.driver.driverClassName)
		properties.setProperty("ebms.jdbc.url",jdbcProperties.getUrl)
		properties.setProperty("ebms.jdbc.username",jdbcProperties.username)
		properties.setProperty("ebms.jdbc.password",Option(jdbcProperties.password).getOrElse(""))
		properties.setProperty("ebms.pool.preferredTestQuery",jdbcProperties.driver.preferredTestQuery)
  }
}
