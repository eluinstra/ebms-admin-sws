/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.GregorianCalendar
import java.util.Locale
import javax.xml.datatype.DatatypeConfigurationException
import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar

import nl.clockwork.ebms.admin.Constants.JQueryLocale
import org.apache.commons.lang.StringUtils
import org.apache.wicket.MarkupContainer
import org.apache.wicket.markup.head.OnDomReadyHeaderItem
import org.apache.wicket.markup.html.form.FormComponentPanel
import org.apache.wicket.markup.html.form.TextField
import org.apache.wicket.markup.html.internal.HtmlHeaderContainer
import org.apache.wicket.model.IModel
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.util.convert.IConverter
import org.apache.wicket.util.convert.converter.AbstractConverter

class BootstrapXMLGregorianCalendarDateTimePicker(val id : String, val model : IModel[XMLGregorianCalendar], val format : String) extends FormComponentPanel[XMLGregorianCalendar](id,model)
{
	private val formatJS : String = format.replaceAll("H","h")
	private var `type` : Type  = Type.DATE_TIME
	private val hourFormat : HourFormat = if (format.contains("H")) HourFormat.H24 else HourFormat.H12
	private var startDate : Date = _
	private var endDate : Date = _
	private var dateTime : XMLGregorianCalendar = _

	setType(classOf[XMLGregorianCalendar])

	val dateTimePicker = new MarkupContainer("dateTimePicker"){}
	dateTimePicker.setMarkupId(getDateTimePickerId)
	dateTimePicker.setOutputMarkupId(true)
	add(dateTimePicker)

	private val dateTimeField : TextField[XMLGregorianCalendar] = new TextField[XMLGregorianCalendar]("dateTime",new PropertyModel[XMLGregorianCalendar](this,"dateTime"))
	{
		override def getConverter[C](`type` : Class[C]) : IConverter[C] =
		{
			if (classOf[XMLGregorianCalendar].isAssignableFrom(`type`))
				new AbstractConverter[XMLGregorianCalendar]
				{
					override def convertToObject(value : String, locale : Locale) : XMLGregorianCalendar =
					{
						try
						{
							val formatter = new SimpleDateFormat(BootstrapXMLGregorianCalendarDateTimePicker.this.format)
							val calendar = new GregorianCalendar
							val date = formatter.parse(value)
							calendar.setTime(date)
							DatatypeFactory.newInstance.newXMLGregorianCalendar(calendar)
						}
						catch
						{
							case e : ParseException => throw new RuntimeException(e)
							case e : DatatypeConfigurationException => throw new RuntimeException(e)
						}
					}

					override def convertToString(value : XMLGregorianCalendar, locale : Locale) : String =
					{
						val formatter = new SimpleDateFormat(BootstrapXMLGregorianCalendarDateTimePicker.this.format)
						formatter.format(value.toGregorianCalendar.getTime)
					}

					override protected def getTargetType : Class[XMLGregorianCalendar] = classOf[XMLGregorianCalendar]
				}
				.asInstanceOf[(IConverter[C])]
			else
				super.getConverter(`type`)
		}

		override def getLabel : IModel[String] = BootstrapXMLGregorianCalendarDateTimePicker.this.getLabel

		override def isRequired : Boolean = BootstrapXMLGregorianCalendarDateTimePicker.this.isRequired
	}
	dateTimePicker.add(dateTimeField)

	def this(id : String, model : IModel[XMLGregorianCalendar]) = this(id,model,"dd-MM-yyyy HH:mm:ss")

	def this(id : String) = this(id,null.asInstanceOf[IModel[XMLGregorianCalendar]])

	def this(id : String, format : String) = this(id,null,format)

	override def renderHead(container : HtmlHeaderContainer)
	{
		val response = container.getHeaderResponse
		val options = new ArrayList[String]
		if (formatJS != null)
			options.add("format: '" + formatJS + "'")
		if (!Type.DATE_TIME.equals(`type`) & !Type.DATE.equals(`type`))
			options.add("pickDate: false")
		if (!Type.DATE_TIME.equals(`type`) & !Type.TIME.equals(`type`))
			options.add("pickTime: false")
		if (HourFormat.H12.equals(hourFormat))
			options.add("pick12HourFormat: true")
		if (getJQueryLocale != null)
			options.add("language: '" + getLocale.toString + "'")
		if (startDate != null)
			options.add("startDate: new Date(" + startDate.getTime + ")")
		if (endDate != null)
			options.add("endDate: new Date(" + endDate.getTime + ")")
		response.render(OnDomReadyHeaderItem.forScript("$(function  {$('#" + getDateTimePickerId + "').datetimepicker({" + StringUtils.join(options,",") + "})})"))
		super.renderHead(container)
	}
	
	override protected def convertInput
	{
		dateTime = dateTimeField.getConvertedInput
		setConvertedInput(dateTime)
	}
	
	override protected def onBeforeRender
	{
		dateTime = getModelObject
		super.onBeforeRender
	}
	
	def setType(`type` : Type) : BootstrapXMLGregorianCalendarDateTimePicker =
	{
		this.`type` = `type`
		this
	}

	def getJQueryLocale = JQueryLocale.EN

	def setStartDate(startDate : Date) = this.startDate = startDate

	def setEndDate(endDate : Date) = this.endDate = endDate

	private def getDateTimePickerId : String = getMarkupId + "DateTimePicker"

	def getDateTime : XMLGregorianCalendar = dateTime

	def setDateTime(dateTime : XMLGregorianCalendar) = this.dateTime = dateTime
}
object BootstrapXMLGregorianCalendarDateTimePicker
{
	def getLinkBootstrapDateTimePickersJavaScript(startDate : BootstrapXMLGregorianCalendarDateTimePicker, endDate : BootstrapXMLGregorianCalendarDateTimePicker) : String =
	{
		"$(function  {" +
		"$('#" + startDate.getDateTimePickerId + "').on('changeDate',function  {" +
		"var d = $('#" + startDate.getDateTimePickerId + "').data('datetimepicker').getDate" +
		"d.setDate(d.getDate + 1)" +
		"$('#" + endDate.getDateTimePickerId + "').data('datetimepicker').setStartDate(d)" +
		"})" +
		"$('#" + endDate.getDateTimePickerId + "').on('changeDate',function (e) {" +
		"var d = $('#" + endDate.getDateTimePickerId + "').data('datetimepicker').getDate" +
		"d.setDate(d.getDate - 1)" +
		"$('#" + startDate.getDateTimePickerId + "').data('datetimepicker').setEndDate(d)" +
		"})" +
		"})"
	}
}
