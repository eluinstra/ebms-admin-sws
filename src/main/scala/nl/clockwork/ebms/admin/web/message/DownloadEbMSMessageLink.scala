/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.zip.ZipOutputStream

import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.EbMSMessage

import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler
import org.apache.wicket.request.resource.ContentDisposition
import org.apache.wicket.util.resource.IResourceStream

import resource._

class DownloadEbMSMessageLink(val id : String, val ebMSDAO : EbMSDAO, val messageId : String, val messageNr : Int) extends Link[Void](id,null)
{
	@transient val logger = LogFactory getLog getClass

	def this(id : String, ebMSDAO : EbMSDAO, message : EbMSMessage) = this(id,ebMSDAO,message.getMessageId,message.getMessageNr)

	override def onClick
	{
		try
		{
			val output = new ByteArrayOutputStream
			managed (new ZipOutputStream(output)).foreach(zip =>
			{
				ebMSDAO.writeMessageToZip(messageId, messageNr,zip)
			})
			val resourceStream = new ByteArrayResourceStream(output,"application/zip")
			getRequestCycle scheduleRequestHandlerAfterCurrent createRequestHandler(resourceStream)
		}
		catch
		{
			case e : IOException => logger error("",e); error(e getMessage)
		}
	}

	private def createRequestHandler(resourceStream : IResourceStream) : ResourceStreamRequestHandler =
		new ResourceStreamRequestHandler(resourceStream) setFileName("message." + messageId + "." + messageNr + ".zip") setContentDisposition ContentDisposition.ATTACHMENT
}
