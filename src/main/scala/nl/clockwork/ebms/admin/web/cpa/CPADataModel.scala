/**
  * Copyright 2013 Clockwork
  *
  * Licensed under the Apache License, Version 2.0 (the "License")
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package nl.clockwork.ebms.admin.web.cpa

import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.CPA

import org.apache.wicket.model.LoadableDetachableModel

class CPADataModel(val ebMSDAO: EbMSDAO, val cpaId: String) extends LoadableDetachableModel[CPA]
{
  require(Option(cpaId) nonEmpty)

  override def load: CPA = ebMSDAO.findCPA(cpaId).getOrElse(null)
}

object CPADataModel
{
  def apply(ebMSDAO : EbMSDAO, cpaId : String) : CPADataModel = new CPADataModel(ebMSDAO, cpaId)
}
