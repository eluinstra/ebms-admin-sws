/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.form.PasswordTextField
import org.apache.wicket.markup.html.form.TextField
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class JavaKeyStorePropertiesFormPanel(val id : String, val model : IModel[JavaKeyStorePropertiesFormModel], val required : Boolean) extends Panel(id, model)
{
	@transient val logger = LogFactory getLog getClass

	add(new JavaKeyStorePropertiesForm("form", model))

	def this(id : String, model : IModel[JavaKeyStorePropertiesFormModel]) = this(id, model, true)

	class JavaKeyStorePropertiesForm(val id : String, val model : IModel[JavaKeyStorePropertiesFormModel]) extends Form[JavaKeyStorePropertiesFormModel](id, new CompoundPropertyModel[JavaKeyStorePropertiesFormModel](model))
	{
		add(new BootstrapFormComponentFeedbackBorder("uriFeedback", new TextField[String]("uri") setLabel new ResourceModel("lbl.uri") setRequired required))
		add(new BootstrapFormComponentFeedbackBorder("passwordFeedback", new PasswordTextField("password") setResetPassword false setLabel new ResourceModel("lbl.password") setRequired required))
		add(createTestButton("test",model))

		private def createTestButton(id : String, model : IModel[JavaKeyStorePropertiesFormModel]) : Button =
			new Button(id,new ResourceModel("cmd.test"))
			{
				override def onSubmit
				{
					try
					{
						val m = model.getObject
						Utils testKeystore(m uri, m password)
						info(JavaKeyStorePropertiesForm.this getString "test.ok")
					}
					catch
					{
						case e : Exception => logger error("", e); error(new StringResourceModel("test.nok", JavaKeyStorePropertiesForm.this, Model of e) getString)
					}
				}
			}
	}
}
class JavaKeyStorePropertiesFormModel(@BeanProperty var uri : String = "keystore.jks", @BeanProperty var password : String = "password") extends IClusterable
