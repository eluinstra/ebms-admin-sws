/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.cpa

import java.io.Serializable

import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.service.CPAService

import org.apache.commons.logging.LogFactory
import org.apache.wicket.Component
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.markup.html.form.AjaxButton
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.form.TextField
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.ResourceModel

import scala.beans.BeanProperty

class UrlModalWindow(val id : String, val cpaService : CPAService, val cpaId : String, val components : Seq[Component]) extends ModalWindow(id)
{
	setCssClassName(ModalWindow.CSS_CLASS_GRAY)
	setContent(createUrlPanel(cpaService,cpaId,components))
	setCookieName("url")
	setCloseButtonCallback(new nl.clockwork.ebms.admin.web.CloseButtonCallback)

	override def getTitle : IModel[String] = Model.of(getLocalizer.getString("url",this))

	private def createUrlPanel(cpaService : CPAService, cpaId : String, components : Seq[Component]) : UrlPanel =
	  new UrlPanel(getContentId,cpaService.getURL(cpaId))
  	{
  		override def saveUrl(url : String) : Unit = cpaService.setURL(cpaId,url)
  		
  		override def getComponents : Seq[Component] = components
  		
  		override def getWindow : ModalWindow = UrlModalWindow.this
  	}
	
	abstract class UrlPanel(id : String, url: String) extends Panel(id)
	{
		@transient val logger = LogFactory getLog getClass

		add(new UrlForm("form",url))
		
		def saveUrl(url : String) : Unit
		def getComponents : Seq[Component]
		def getWindow : ModalWindow

		class UrlForm(id : String, url : String) extends Form[UrlModel](id,new CompoundPropertyModel[UrlModel](UrlModel(url)))
		{
			add(new BootstrapFeedbackPanel("feedback"))
			add(new TextField[String]("url").setLabel(new ResourceModel("lbl.url")))
			add(createSaveButton("save"))
			add(createClearButton("clear"))
			add(createCancelButton("cancel"))

			private def createSaveButton(id : String) : AjaxButton =
				new AjaxButton(id,new ResourceModel("cmd.save"))
				{
					override protected def onSubmit(target : AjaxRequestTarget, form : Form[_])
					{
						val model : UrlModel = UrlForm.this.getModelObject
						saveUrl(model getUrl)
						if (target != null)
						{
							target.add(getComponents : _*)
							getWindow close target
						}
					}

					override protected def onError(target : AjaxRequestTarget, form : Form[_])
					{
						super.onError(target,form)
						if (target != null)
							target add form
					}
				}

			def createClearButton(id : String) : AjaxButton =
				new AjaxButton(id,new ResourceModel("cmd.clear"))
				{
					override protected def onSubmit(target : AjaxRequestTarget, form : Form[_])
					{
						val model = UrlForm.this.getModelObject
						model setUrl null
						if (target != null)
							target add form
					}
				}

			def createCancelButton(id : String) : Button =
				new AjaxButton(id,new ResourceModel("cmd.cancel"))
				{
					override protected def onSubmit(target : AjaxRequestTarget, form : Form[_]) : Unit = getWindow.close(target)
				}
				.setDefaultFormProcessing(false)

		}
	}

	class UrlModel(@BeanProperty var url : String) extends Serializable
	object UrlModel
	{
		def apply(url: String) : UrlModel = new UrlModel(url)
	}
}
