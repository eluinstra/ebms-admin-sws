/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.html.form.{CheckBox, Form}
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class SignaturePropertiesFormPanel(val id : String, val model : IModel[SignaturePropertiesFormModel]) extends Panel(id,model)
{
	@transient val logger = LogFactory getLog getClass

	add(new SignaturePropertiesForm("form",model))

	class SignaturePropertiesForm(val id : String, val model : IModel[SignaturePropertiesFormModel]) extends Form[SignaturePropertiesFormModel](id,new CompoundPropertyModel[SignaturePropertiesFormModel](model))
	{
		add(createSigningCheckBox("signing"))
		add(createKeystorePropertiesPanel("keystoreProperties"))

		private def createSigningCheckBox(id : String) : CheckBox =
			new CheckBox(id)
			.setLabel(new ResourceModel("lbl.signing"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					target.add(SignaturePropertiesForm.this)
				}
			})
		  .asInstanceOf[CheckBox]

		private def createKeystorePropertiesPanel(id : String) : JavaKeyStorePropertiesFormPanel =
			new JavaKeyStorePropertiesFormPanel(id,new PropertyModel[JavaKeyStorePropertiesFormModel](getModelObject,"keystoreProperties"),false)
			{
				override def isVisible : Boolean = getModelObject.signing
			}
	}

}
class SignaturePropertiesFormModel(@BeanProperty var signing : Boolean = true, @BeanProperty var keystoreProperties : JavaKeyStorePropertiesFormModel = new JavaKeyStorePropertiesFormModel) extends IClusterable
