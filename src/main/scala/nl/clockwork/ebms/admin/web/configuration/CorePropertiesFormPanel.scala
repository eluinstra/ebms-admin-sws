/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.CheckBox
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class CorePropertiesFormPanel(val id : String, model : IModel[CorePropertiesFormModel]) extends Panel(id,model)
{
	@transient val logger = LogFactory getLog getClass

	add(new CorePropertiesForm("form",model))

	class CorePropertiesForm(val id : String, val model : IModel[CorePropertiesFormModel]) extends Form[CorePropertiesFormModel](id,new CompoundPropertyModel[CorePropertiesFormModel](model))
	{
		add(new CheckBox("digipoortPatch").setLabel(new ResourceModel("lbl.digipoortPatch")))
		add(new CheckBox("oraclePatch").setLabel(new ResourceModel("lbl.oraclePatch")))
		add(new CheckBox("cleoPatch").setLabel(new ResourceModel("lbl.cleoPatch")))
	}

}

class CorePropertiesFormModel(@BeanProperty var digipoortPatch : Boolean = true, @BeanProperty var oraclePatch : Boolean = true, @BeanProperty var cleoPatch : Boolean = false) extends IClusterable