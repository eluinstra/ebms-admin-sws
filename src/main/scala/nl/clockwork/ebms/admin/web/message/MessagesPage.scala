/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import java.util.Date

import nl.clockwork.ebms.admin.Constants
import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.EbMSMessage
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapPagingNavigator
import nl.clockwork.ebms.admin.web.MaxItemsPerPageChoice
import nl.clockwork.ebms.admin.web.OddOrEvenIndexStringModel
import nl.clockwork.ebms.admin.web.PageLink
import nl.clockwork.ebms.admin.web.Utils
import nl.clockwork.ebms.admin.web.WebMarkupContainer
import org.apache.commons.lang.SerializationUtils
import org.apache.wicket.AttributeModifier
import org.apache.wicket.datetime.markup.html.basic.DateLabel
import org.apache.wicket.markup.html.WebPage
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.repeater.Item
import org.apache.wicket.markup.repeater.data.DataView
import org.apache.wicket.markup.repeater.data.IDataProvider
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.spring.injection.annot.SpringBean

import scala.beans.BeanProperty

class MessagesPage(val filter : EbMSMessageFilter, val responsePage : WebPage) extends BasePage
{
	class EbMSMessageDataView(val id : String, val dataProvider : IDataProvider[EbMSMessage]) extends DataView[EbMSMessage](id,dataProvider)
	{
		setOutputMarkupId(true)

		override def getItemsPerPage : Long = maxItemsPerPage longValue

		override protected def populateItem(item : Item[EbMSMessage])
		{
			val message = item.getModelObject
			item add createViewLink("view",message)
			item add new Label("messageNr",message.getMessageNr)
			item add createFilterConversationIdLink("filterConversationId",message)
			item add createViewRefToMessageIdLink("viewRefToMessageId",message)
			item add DateLabel.forDatePattern("timestamp",new Model[Date](message.getTimestamp),Constants.DATETIME_FORMAT)
			item add new Label("cpaId",message.getCpaId)
			item add new Label("fromPartyId",message.getFromPartyId)
			item add new Label("fromRole",message.getFromRole)
			item add new Label("toPartyId",message.getToPartyId)
			item add new Label("toRole",message.getToRole)
			item add new Label("service",message.getService)
			item add new Label("action",message.getAction)
			item add new Label("status",message.getStatus) add AttributeModifier.replace("class",Model of Utils.getTableCellCssClass(message getStatus))
			item add DateLabel.forDatePattern("statusTime",new Model[Date](message getStatusTime),Constants DATETIME_FORMAT)
			item add AttributeModifier.replace("class",new OddOrEvenIndexStringModel(item getIndex))
		}

		private def createViewLink(id : String, message : EbMSMessage) : Link[Void] =
			new Link[Void](id)
			{
				override def onClick = setResponsePage(new MessagePage(message,MessagesPage.this))
			}
			.add(new Label("messageId",message getMessageId))
		  .asInstanceOf[Link[Void]]

		private def createFilterConversationIdLink(id : String, message : EbMSMessage) : Link[Void] =
			new Link[Void](id)
			{
				override def onClick
				{
					val filter = SerializationUtils.clone(MessagesPage.this.filter).asInstanceOf[MessageFilterFormModel]
					filter setConversationId message.getConversationId
					setResponsePage(new MessagesPage(filter,MessagesPage.this))
				}
			}
			.add(new Label("conversationId",message getConversationId))
			.setEnabled(MessagesPage.this.filter.getConversationId == null)
		  .asInstanceOf[Link[Void]]

		private def createViewRefToMessageIdLink(id : String, message : EbMSMessage) : Link[Void] =
			new Link[Void](id)
			{
				override def onClick = setResponsePage(new MessagePage(ebMSDAO.findMessage(message getRefToMessageId) getOrElse null,MessagesPage.this))
			}
			.add(new Label("refToMessageId",message.getRefToMessageId))
		  .asInstanceOf[Link[Void]]
	}

	@SpringBean(name="ebMSAdminDAO") private var ebMSDAO : EbMSDAO = _
	@BeanProperty @SpringBean(name="maxItemsPerPage")	var maxItemsPerPage : Integer = _

	add(createMessageFilterPanel("messageFilter",filter))
	val container = new WebMarkupContainer("container")
	add(container)
	val messages = new EbMSMessageDataView("messages",new MessageDataProvider(ebMSDAO,filter))
	container add messages
	val navigator = new BootstrapPagingNavigator("navigator",messages)
	add(navigator)
	add(new MaxItemsPerPageChoice("maxItemsPerPage",new PropertyModel[Int](this,"maxItemsPerPage"),container,navigator))
	add(new PageLink("back",responsePage) setVisible responsePage != null)
	add(new DownloadEbMSMessagesCSVLink("download",ebMSDAO,filter))

	def this(filter : EbMSMessageFilter) = this(filter,null)

	def this() = this(MessageFilterPanel createMessageFilter)

	override def getPageTitle : String = getLocalizer getString("messages",this)

	private def createMessageFilterPanel(id : String, filter : EbMSMessageFilter) : MessageFilterPanel =
		new MessageFilterPanel(id,filter.asInstanceOf[MessageFilterFormModel])
		{
			override def getPage(filter : MessageFilterFormModel) : BasePage = new MessagesPage(filter)
		}

}
