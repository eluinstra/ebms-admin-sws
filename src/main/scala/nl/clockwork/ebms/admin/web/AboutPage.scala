/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.util.Map

import nl.clockwork.ebms.admin.PropertyPlaceholderConfigurer
import org.apache.wicket.markup.html.basic.{Label, MultiLineLabel}
import org.apache.wicket.request.mapper.parameter.PageParameters
import org.apache.wicket.spring.injection.annot.SpringBean

import scala.collection.JavaConversions._

class AboutPage(override val parameters : PageParameters) extends BasePage(parameters)
{
	@SpringBean(name="propertyConfigurer") var propertyPlaceholderConfigurer : PropertyPlaceholderConfigurer = _

	add(new org.apache.wicket.markup.html.WebMarkupContainer("ebms-admin.version") add(new Label("version",nl.clockwork.ebms.admin.Utils.readVersion("/META-INF/maven/nl.clockwork.ebms.admin/ebms-admin/pom.properties"))))
	add(new org.apache.wicket.markup.html.WebMarkupContainer("ebms-core.version") add(new Label("version",nl.clockwork.ebms.admin.Utils.readVersion("/META-INF/maven/nl.clockwork.ebms/ebms-core/pom.properties"))))
	val properties = propertyPlaceholderConfigurer.getProperties
	add(new MultiLineLabel("properties",toString1(properties)))

	override def getPageTitle : String = getLocalizer.getString("about",this)

	private def toString1(properties : Map[String,String]) : String =
	{
		var result = "";
		properties.keySet().foreach(key=>result += key + " = " + (if (key.matches(".*(password|pwd).*")) properties.get(key).replaceAll(".","*") else properties.get(key)) + "\n");
		result;
	}

}
