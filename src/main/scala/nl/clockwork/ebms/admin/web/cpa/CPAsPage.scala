/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.cpa

import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.CPA
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapPagingNavigator
import nl.clockwork.ebms.admin.web.MaxItemsPerPageChoice
import nl.clockwork.ebms.admin.web.OddOrEvenIndexStringModel
import nl.clockwork.ebms.admin.web.WebMarkupContainer

import org.apache.wicket.AttributeModifier
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.repeater.Item
import org.apache.wicket.markup.repeater.data.DataView
import org.apache.wicket.markup.repeater.data.IDataProvider
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.spring.injection.annot.SpringBean

class CPAsPage extends BasePage
{
	class CPADataView(val id : String, val dataProvider : IDataProvider[CPA]) extends DataView[CPA](id,dataProvider)
	{
		setOutputMarkupId(true)

		override def getItemsPerPage : Long = maxItemsPerPage.asInstanceOf[Number].longValue

		override protected def populateItem(item : Item[CPA])
		{
			item add(createViewLink("view",item getModelObject))
			item add(AttributeModifier replace("class",new OddOrEvenIndexStringModel(item getIndex)))
		}

		def createViewLink(id : String, cpa : CPA) : Link[Void] = 
		  new Link[Void](id)
  		{
  			override def onClick = setResponsePage(CPAPage(cpa,CPAsPage.this))
  		}
  		.add(new Label("cpaId",cpa getCpaId)).asInstanceOf[Link[Void]]
	}

	@SpringBean(name="ebMSAdminDAO") var ebMSDAO : EbMSDAO = _
	@SpringBean(name="maxItemsPerPage") var maxItemsPerPage : Integer = _

	val container = new WebMarkupContainer("container")
	add(container)
	val cpas = new CPADataView("cpas",CPADataProvider(ebMSDAO))
	container add cpas
	val navigator = new BootstrapPagingNavigator("navigator",cpas)
	add(navigator)
	add(new MaxItemsPerPageChoice("maxItemsPerPage",new PropertyModel[Int](this,"maxItemsPerPage"),container,navigator))

	override def getPageTitle : String = getLocalizer.getString("cpas",this)
}
object CPAsPage
{
  def apply = new CPAsPage
}
