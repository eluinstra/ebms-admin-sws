/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.form.TextField
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class ConsolePropertiesFormPanel(val id : String, val model : IModel[ConsolePropertiesFormModel]) extends Panel(id,model)
{
	@transient val logger = LogFactory getLog getClass

	add(new ConsolePropertiesForm("form",model))

	class ConsolePropertiesForm(val id : String, val model : IModel[ConsolePropertiesFormModel]) extends Form[ConsolePropertiesFormModel](id,new CompoundPropertyModel[ConsolePropertiesFormModel](model))
	{
		add(new BootstrapFormComponentFeedbackBorder("maxItemsPerPageFeedback",new TextField[Integer]("maxItemsPerPage").setLabel(new ResourceModel("lbl.maxItemsPerPage")).setRequired(true)))
	}

}

class ConsolePropertiesFormModel(@BeanProperty var maxItemsPerPage : Int = 20) extends IClusterable
