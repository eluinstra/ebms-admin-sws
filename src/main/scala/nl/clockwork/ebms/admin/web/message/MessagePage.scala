/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import nl.clockwork.ebms.Constants.EbMSEventStatus
import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.Constants
import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.EbMSEventLog
import nl.clockwork.ebms.admin.model.EbMSMessage
import nl.clockwork.ebms.admin.web.{BasePage, PageLink, Utils, WebMarkupContainer}
import org.apache.wicket.AttributeModifier
import org.apache.wicket.Component
import org.apache.wicket.IGenericComponent
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.markup.html.AjaxLink
import org.apache.wicket.datetime.markup.html.basic.DateLabel
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
import org.apache.wicket.markup.html.WebPage
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.form.TextArea
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.PropertyListView
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.Model
import org.apache.wicket.spring.injection.annot.SpringBean

import scala.collection.JavaConverters._

class MessagePage(val message : EbMSMessage, val responsePage : WebPage) extends BasePage with IGenericComponent[EbMSMessage]
{
	@SpringBean(name="ebMSAdminDAO") var ebMSDAO : EbMSDAO = _
	private var showContent : Boolean = _

	setModel(new CompoundPropertyModel[EbMSMessage](message))
	add(new Label("messageId"))
	add(new Label("messageNr"))
	add(new Label("conversationId"))
	add(createRefToMessageIdLink("viewRefToMessageId",message))
	add(DateLabel.forDatePattern("timestamp",Constants.DATETIME_FORMAT))
	add(new Label("cpaId"))
	add(new Label("fromPartyId"))
	add(new Label("fromRole"))
	add(new Label("toPartyId"))
	add(new Label("toRole"))
	add(new Label("service"))
	add(new Label("action"))
	add(createViewMessageErrorLink("viewMessageError",message))
	add(new Label("statusTime"))
	add(AttachmentsPanel("attachments",message.getAttachments.asScala.toList).setVisible(message.getAttachments.size > 0))
	add(createNextEventContainer("nextEvent",message))
	add(createEventLogContainer("eventLog",message))
	add(new PageLink("back",responsePage))
	add(new DownloadEbMSMessageLink("download",ebMSDAO,message))
	val content = createContentField("content",message)
	add(content)
	add(createToggleContentLink("toggleContent",content))

	override def getPageTitle : String = getLocalizer.getString("message",this)

	def getShowContent : Boolean = showContent

	class ErrorMessageModalWindow(val id : String, val errorMessage : String) extends ModalWindow(id)
	{
		setCssClassName(ModalWindow.CSS_CLASS_GRAY)
		setContent(new ErrorMessagePanel(this,Model.of(errorMessage)))
		setCookieName("eventError")
		setCloseButtonCallback(new ModalWindow.CloseButtonCallback
		{
			override def onCloseButtonClicked(target : AjaxRequestTarget) : Boolean = true
		})

		override def getTitle : IModel[String] = new Model[String](getLocalizer.getString("eventError",this))
	}

	private def createRefToMessageIdLink(id :String, message : EbMSMessage) : Link[Void] =
		new Link[Void](id)
		{
			override def onClick = setResponsePage(new MessagePage(ebMSDAO.findMessage(message.getRefToMessageId).get,MessagePage.this))
		}
		.add(new Label("refToMessageId"))
	  .asInstanceOf[Link[Void]]

	private def createViewMessageErrorLink(id : String, message : EbMSMessage) : AjaxLink[Void] =
		new AjaxLink[Void](id)
		{
			override def onClick(target : AjaxRequestTarget) = setResponsePage(new MessagePage(ebMSDAO.findResponseMessage(message.getMessageId).getOrElse(null),MessagePage.this))
		}
		.add(new Label("status"))
		.setEnabled(EbMSMessageStatus.DELIVERY_FAILED.equals(message.getStatus))
		.add(AttributeModifier.replace("class",Model.of(Utils.getTableCellCssClass(message.getStatus))))
	  .asInstanceOf[AjaxLink[Void]]

	private def createNextEventContainer(id : String, message : EbMSMessage) : WebMarkupContainer =
	{
		val result = new WebMarkupContainer(id)
		result.setVisible(message.getEvent != null)
		if (message.getEvent != null)
		{
			result.add(DateLabel.forDatePattern("event.timestamp",Constants.DATETIME_FORMAT))
			result.add(new Label("event.retries"))
			result.add(DateLabel.forDatePattern("event.timeToLive",Constants.DATETIME_FORMAT))
		}
		result
	}

	private def createEventLogContainer(id : String, message : EbMSMessage) : WebMarkupContainer =
	{
		val eventLog = new WebMarkupContainer(id)
		eventLog.setVisible(message.getEvents.size > 0)
		val events = new PropertyListView[EbMSEventLog]("events",message.getEvents)
		{
			override protected def populateItem(item : ListItem[EbMSEventLog])
			{
				val errorMessageModalWindow = new ErrorMessageModalWindow("errorMessageWindow",item.getModelObject.errorMessage)
				item.add(DateLabel.forDatePattern("timestamp",Constants.DATETIME_FORMAT))
				item.add(new Label("uri"))
				item.add(errorMessageModalWindow)
				val link = new AjaxLink[Void]("showErrorMessageWindow")
				{
					override def onClick(target : AjaxRequestTarget) = errorMessageModalWindow.show(target)
				}
				link.setEnabled(EbMSEventStatus.FAILED.equals(item.getModelObject.status))
				link.add(new Label("status"))
				item.add(link)
			}
		}
		eventLog.add(events)
		eventLog
	}

	def createContentField(id : String, message : EbMSMessage) : TextArea[String] =
		new TextArea[String](id,Model.of(message.getContent))
		{
			override def isVisible : Boolean = showContent
		}
		.setOutputMarkupPlaceholderTag(true)
		.setEnabled(false)
	  .asInstanceOf[TextArea[String]]

	def createToggleContentLink(id : String, content : Component) : AjaxLink[String] =
		new AjaxLink[String](id)
		{
			override def onClick(target : AjaxRequestTarget)
			{
				showContent = !showContent
				target.add(this)
				target.add(content)
			}
		}
		.add(new Label("label",new Model[String]
		{
			override def getObject : String = MessagePage.this.getLocalizer.getString(if (showContent) "cmd.hide" else "cmd.show",MessagePage.this)
		}))
	  .asInstanceOf[AjaxLink[String]]

	override def getModelObject : EbMSMessage = getDefaultModelObject.asInstanceOf[EbMSMessage]

	override def setModelObject(o : EbMSMessage) = setDefaultModelObject(o)

	override def getModel : IModel[EbMSMessage] = getDefaultModel.asInstanceOf[IModel[EbMSMessage]]

	override def setModel(model : IModel[EbMSMessage]) = setDefaultModel(model)
}
