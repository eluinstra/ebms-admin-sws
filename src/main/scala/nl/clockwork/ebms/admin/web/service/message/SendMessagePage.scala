/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.service.message

import java.util.ArrayList
import java.util.List
import javax.xml.bind.JAXBException

import nl.clockwork.ebms.admin.CPAUtils
import nl.clockwork.ebms.admin.Utils
import nl.clockwork.ebms.admin.web.BasePage
import nl.clockwork.ebms.admin.web.BootstrapFeedbackPanel
import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import nl.clockwork.ebms.admin.web.ResetButton
import nl.clockwork.ebms.common.XMLMessageBuilder
import nl.clockwork.ebms.model.EbMSDataSource
import nl.clockwork.ebms.model.EbMSMessageContent
import nl.clockwork.ebms.model.EbMSMessageContext
import nl.clockwork.ebms.model.Role
import nl.clockwork.ebms.service.CPAService
import nl.clockwork.ebms.service.EbMSMessageService
import org.apache.commons.logging.LogFactory
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.ajax.markup.html.form.AjaxButton
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.form.{Button, DropDownChoice, Form, TextField}
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.ListView
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.model.StringResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.CollaborationProtocolAgreement

import scala.beans.BeanProperty

import scala.collection.JavaConversions._

class SendMessagePage extends BasePage
{
	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="cpaService") var cpaService : CPAService = _
	@SpringBean(name="ebMSMessageService") var ebMSMessageService : EbMSMessageService = _

	add(new BootstrapFeedbackPanel("feedback").setOutputMarkupId(true))
	add(new MessageForm("form"))

	override def getPageTitle : String = getLocalizer.getString("messageSend",this)

	class MessageForm(id : String) extends Form[EbMSMessageContextModel](id,new CompoundPropertyModel[EbMSMessageContextModel](new EbMSMessageContextModel))
	{
		setMultiPart(true)
		add(new BootstrapFormComponentFeedbackBorder("cpaIdFeedback",createCPAIdChoice("cpaId")))
		add(new BootstrapFormComponentFeedbackBorder("fromPartyIdFeedback",createFromPartyIdChoice("fromRole.partyId")))
		add(new BootstrapFormComponentFeedbackBorder("fromRoleFeedback",createFromRoleChoice("fromRole.role")))
		add(new BootstrapFormComponentFeedbackBorder("serviceFeedback",createServiceChoice("service")))
		add(new BootstrapFormComponentFeedbackBorder("actionFeedback",createActionChoice("action")))
		add(new TextField[String]("conversationId").setLabel(new ResourceModel("lbl.conversationId")))
		add(new TextField[String]("messageId").setLabel(new ResourceModel("lbl.messageId")))
		add(new TextField[String]("refToMessageId").setLabel(new ResourceModel("lbl.refToMessageId")))
		add(new DataSourcesForm("form",getModelObject.dataSources))
		val send = createSendButton("send")
		setDefaultButton(send)
		add(send)
		add(new ResetButton("reset",new ResourceModel("cmd.reset"),classOf[SendMessagePage]))

		private def createCPAIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,Model ofList Utils.toList(cpaService.getCPAIds toList))
			.setLabel(new ResourceModel("lbl.cpaId"))
			.setRequired(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetFromPartyIds(CPAUtils getPartyIds cpa)
						model resetFromRoles(CPAUtils getRoleNames cpa)
						model.resetServices
						model.resetActions
						model.resetDataSources
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createFromPartyIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"fromPartyIds"))
			.setLabel(new ResourceModel("lbl.fromPartyId"))
			.setRequired(false)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget )
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetFromRoles(CPAUtils getRoleNames(cpa,model.getFromRole getPartyId))
						model resetServices(CPAUtils getServiceNames(cpa,model.getFromRole getRole))
						model.resetActions
						model.resetDataSources
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createFromRoleChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"fromRoles"))
			.setLabel(new ResourceModel("lbl.fromRole"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						if (model.getFromRole.getPartyId == null)
							model resetFromPartyIds(CPAUtils.getPartyIdsByRoleName(cpa,model.getFromRole getRole))
						model resetServices(CPAUtils.getServiceNames(cpa,model.getFromRole getRole))
						model.resetActions
						model.resetDataSources
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createServiceChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"services"))
			.setLabel(new ResourceModel("lbl.service"))
			.setRequired(true)
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
			{
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val cpa = XMLMessageBuilder.getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetActions(CPAUtils getFromActionNames(cpa,model.getFromRole getRole,model getService))
						model.resetDataSources
						target add(getPage get "feedback")
						target add(getPage get "form")
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
			})
		  .asInstanceOf[DropDownChoice[String]]

		private def createActionChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[List[String]](getModelObject,"actions"))
			.setLabel(new ResourceModel("lbl.action"))
			.setRequired(true)
			.setOutputMarkupId(true)
		  .asInstanceOf[DropDownChoice[String]]

		private def createSendButton(id : String) : Button =
			new Button(id,new ResourceModel("cmd.send"))
			{
				override def onSubmit
				{
					try
					{
						val model = MessageForm.this.getModelObject
						val messageContent = new EbMSMessageContent(model,model getDataSources)
						val messageId = ebMSMessageService.sendMessage(messageContent)
						info(new StringResourceModel("sendMessage.ok",Model of messageId) getString)
					}
					catch
					{
						case e : Exception => logger error("",e); error(e getMessage)
					}
				}
			}

	}

	class DataSourcesForm(id : String, dataSources : List[EbMSDataSource]) extends Form[List[_ <: EbMSDataSource]](id,Model.ofList(dataSources))
	{
		val dataSources_ = new ListView[EbMSDataSource]("dataSources",dataSources)
		{
			override protected def populateItem(item : ListItem[EbMSDataSource])
			{
				item setModel(new CompoundPropertyModel[EbMSDataSource](item getModelObject))
				item add(new Label("name"))
				item add(new Label("contentType"))
				item add(new AjaxButton("remove",new ResourceModel("cmd.remove"),DataSourcesForm.this)
				{
					override protected def onSubmit(target : AjaxRequestTarget, form : Form[_])
					{
						DataSourcesForm.this.getModelObject.remove(item getModelObject)
						target add(DataSourcesForm.this)
					}
				})
			}
		}
		dataSources_ setOutputMarkupId true
		add(dataSources_)

		val dataSourceModalWindow = new DataSourceModalWindow("dataSourceModelWindow",dataSources,DataSourcesForm.this)
		add(dataSourceModalWindow)

		add(new AjaxButton("add")
		{
			override protected def onSubmit(target : AjaxRequestTarget, form : Form[_]) = dataSourceModalWindow.show(target)
		})

	}

	class EbMSMessageContextModel(
		@BeanProperty val fromPartyIds : List[String] = new ArrayList[String],
		@BeanProperty val fromRoles : List[String] = new ArrayList[String],
		@BeanProperty val services : List[String] = new ArrayList[String],
		@BeanProperty val actions : List[String] = new ArrayList[String],
		@BeanProperty val dataSources : List[EbMSDataSource] = new ArrayList[EbMSDataSource]) extends EbMSMessageContext
	{
		setFromRole(new Role)

		def resetFromPartyIds
		{
			getFromRoles.clear
			getFromRole.setPartyId(null)
		}
		def resetFromPartyIds(partyIds : List[String])
		{
			resetFromPartyIds
			getFromPartyIds.addAll(partyIds)
		}
		def resetFromRoles
		{
			getFromRoles.clear
			getFromRole.setRole(null)
		}
		def resetFromRoles(roles : List[String])
		{
			resetFromRoles
			getFromRoles.addAll(roles)
			getFromRole.setRole(if (getFromRoles.size == 1) getFromRoles get 0 else null)
		}
		def resetServices
		{
			getServices.clear
			setService(null)
		}
		def resetServices(serviceNames : List[String])
		{
			resetServices
			getServices.addAll(serviceNames)
		}
		def resetActions
		{
			getActions.clear
			setAction(null)
		}
		def resetActions(actionNames : List[String])
		{
			resetActions
			getActions.addAll(actionNames)
		}
		def resetDataSources = getDataSources.clear
	}

}
