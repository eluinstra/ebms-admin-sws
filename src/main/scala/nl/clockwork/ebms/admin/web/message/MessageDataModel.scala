/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.EbMSMessage

import org.apache.wicket.model.LoadableDetachableModel

class MessageDataModel(@transient val ebMSDAO : EbMSDAO, val messageId : String, val messageNr : Int) extends LoadableDetachableModel[EbMSMessage]
{
	def this(ebMSDAO : EbMSDAO, message : EbMSMessage) = this(ebMSDAO,message.getMessageId,message.getMessageNr)

	require(Option(messageId) nonEmpty)

	override protected def load : EbMSMessage = ebMSDAO.findMessage(messageId,messageNr).getOrElse(null)
}
