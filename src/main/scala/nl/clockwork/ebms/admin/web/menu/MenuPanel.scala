/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.menu

import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.ListView
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.Model

import scala.collection.JavaConversions._

class MenuPanel(val id : String, val menuItems : List[MenuItem]) extends Panel(id,Model of menuItems)
{
	add(new MenuItems("menuItems",menuItems,0))
}
class MenuItems(val id : String, val list : List[_ <: MenuItem], val level : Int) extends ListView[MenuItem](id,list)
{
	setRenderBodyOnly(true)

	override protected def populateItem(item : ListItem[MenuItem])
	{
		val menuItem = item.getModelObject
		if (menuItem.isInstanceOf[MenuLinkItem])
			item.add(new MenuLinkItemPanel("menuItem",menuItem.asInstanceOf[MenuLinkItem])/*.setRenderBodyOnly(true)*/)
		else if (menuItem.isInstanceOf[MenuDivider])
			item.add(new MenuDividerPanel("menuItem"))
		else
			item.add(new MenuItemPanel("menuItem",menuItem,level)/*.setRenderBodyOnly(true)*/)
		//item.setRenderBodyOnly(true)
	}
}
