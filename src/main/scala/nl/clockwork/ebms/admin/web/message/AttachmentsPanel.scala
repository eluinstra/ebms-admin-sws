/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import nl.clockwork.ebms.admin.dao.EbMSDAO
import nl.clockwork.ebms.admin.model.EbMSAttachment
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.markup.html.list.ListItem
import org.apache.wicket.markup.html.list.PropertyListView
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.spring.injection.annot.SpringBean

import scala.collection.JavaConversions._

class AttachmentsPanel(val id : String, val attachments : List[EbMSAttachment]) extends Panel(id)
{
	private class EbMSAttachmentPropertyListView(id : String, list : List[_ <: EbMSAttachment]) extends PropertyListView[EbMSAttachment](id, list)
	{
		override protected def populateItem(item : ListItem[EbMSAttachment])
		{
			item add new Label("name")
			val link = new DownloadEbMSAttachmentLink("downloadAttachment",ebMSDAO,item.getModelObject)
			link add new Label("contentId")
			item add link
			item add new Label("contentType")
		}
	}

	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="ebMSAdminDAO") var ebMSDAO : EbMSDAO = _

	add(new EbMSAttachmentPropertyListView("attachments",attachments))
}
object AttachmentsPanel
{
	def apply(id : String, attachments: List[EbMSAttachment]) : AttachmentsPanel = new AttachmentsPanel(id,attachments)
}