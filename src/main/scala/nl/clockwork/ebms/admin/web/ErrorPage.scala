/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.io.{PrintWriter, StringWriter}

import nl.clockwork.ebms.admin.web.ErrorPage.ErrorType
import org.apache.commons.logging.LogFactory
import org.apache.wicket.RuntimeConfigurationType
import org.apache.wicket.authorization.UnauthorizedActionException
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.protocol.http.PageExpiredException

class ErrorPage(exception : Exception) extends BasePage
{
  import ErrorType._
	@transient val logger = LogFactory getLog getClass
	var errorType : ErrorType = _

	logger.error("",exception)
	errorType = ErrorType.get(exception)
	add(new org.apache.wicket.markup.html.WebMarkupContainer("error") add new HomePageLink("homePageLink") setVisible Error.equals(errorType))
	add(new org.apache.wicket.markup.html.WebMarkupContainer("pageExpired") add new HomePageLink("homePageLink") setVisible PageExpired.equals(errorType))
	add(new org.apache.wicket.markup.html.WebMarkupContainer("unauthorizedAction") add new HomePageLink("homePageLink") setVisible UnauthorizedAction.equals(errorType))
	val showStackTrace = RuntimeConfigurationType.DEVELOPMENT.equals(getApplication().getConfigurationType())
	var stackTrace : String = if (showStackTrace)
	{
		val sw = new StringWriter()
		exception.printStackTrace(new PrintWriter(sw))
		sw.getBuffer().toString()
	}
	else
	  null
	add(new Label("stackTrace",stackTrace) setVisible showStackTrace)

	override def isVersioned : Boolean = false

	override def isErrorPage : Boolean = true

	override def getPageTitle : String = errorType toString

}
object ErrorPage
{
object ErrorType extends Enumeration
{
  type ErrorType = Value
  val Error = Value("Error")
	val PageExpired = Value("Page Expired")
	val UnauthorizedAction = Value("Unauthorized Action")
	
	def get(exception : Exception) =
	{
		if (exception.isInstanceOf[PageExpiredException])
			PageExpired
		else if(exception.isInstanceOf[UnauthorizedActionException])
			UnauthorizedAction
		else
			Error
	}
}
}
