/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import nl.clockwork.ebms.admin.web.BootstrapFormComponentFeedbackBorder
import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.Form
import org.apache.wicket.markup.html.form.PasswordTextField
import org.apache.wicket.markup.html.form.TextField
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.IModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

class ProxyPropertiesFormPanel(val id : String, val model : IModel[ProxyPropertiesFormModel]) extends Panel(id,model)
{
	@transient val logger = LogFactory getLog getClass

	add(new ProxyPropertiesForm("form", model))

	class ProxyPropertiesForm(val id : String, val model : IModel[ProxyPropertiesFormModel]) extends Form[ProxyPropertiesFormModel](id, new CompoundPropertyModel[ProxyPropertiesFormModel](model))
	{
		add(new BootstrapFormComponentFeedbackBorder("hostFeedback", new TextField[String]("host") setLabel new ResourceModel("lbl.host") setRequired true))
		add(new BootstrapFormComponentFeedbackBorder("portFeedback", new TextField[Integer]("port") setLabel new ResourceModel("lbl.port")))
		add(new TextField[String]("nonProxyHosts") setLabel new ResourceModel("lbl.nonProxyHosts"))
		add(new TextField[String]("username") setLabel new ResourceModel("lbl.username"))
		add(new PasswordTextField("password") setResetPassword false setLabel new ResourceModel("lbl.password") setRequired false)
	}
}
class ProxyPropertiesFormModel(
	@BeanProperty var host : String,
	@BeanProperty var port : Int,
	@BeanProperty var nonProxyHosts : String,
	@BeanProperty var username : String,
	@BeanProperty var password : String) extends IClusterable
{
	def this() = this(null,0,null,null,null)
}