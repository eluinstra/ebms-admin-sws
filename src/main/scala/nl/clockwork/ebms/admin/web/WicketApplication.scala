/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map

import nl.clockwork.ebms.admin.web.menu.MenuDivider
import nl.clockwork.ebms.admin.web.menu.MenuItem
import nl.clockwork.ebms.admin.web.menu.MenuLinkItem
import org.apache.wicket.Page
import org.apache.wicket.core.request.handler.PageProvider
import org.apache.wicket.core.request.handler.RenderPageRequestHandler
import org.apache.wicket.markup.html.WebPage
import org.apache.wicket.protocol.http.WebApplication
import org.apache.wicket.request.IRequestHandler
import org.apache.wicket.request.cycle.AbstractRequestCycleListener
import org.apache.wicket.request.cycle.RequestCycle
import org.apache.wicket.request.resource.JavaScriptResourceReference
import org.apache.wicket.spring.injection.annot.SpringComponentInjector

/**
 * Application object for your web application. If you want to run this application without deploying, run the Start class.
 * 
 * @see nl.clockwork.ebms.admin.Start#main(String[])
 */
class WicketApplication extends WebApplication
{
	val menuItems = new ArrayList[MenuItem]
	val messageViewPanels = new HashMap[String,MessageViewPanel]
	val messageEditPanels = new HashMap[String,MessageEditPanel]
	
	val home = new MenuLinkItem("0","home",getHomePage)
	menuItems.add(home)

	val cpa = new MenuItem("1","cpaService")
	new MenuLinkItem(cpa,"1","cpas",classOf[nl.clockwork.ebms.admin.web.service.cpa.CPAsPage])
	new MenuLinkItem(cpa,"2","cpa",classOf[nl.clockwork.ebms.admin.web.service.cpa.CPAUploadPage])
	menuItems.add(cpa)

	val message = new MenuItem("2","messageService")
	new MenuLinkItem(message,"1","ping",classOf[nl.clockwork.ebms.admin.web.service.message.PingPage])
	new MenuLinkItem(message,"2","messages",classOf[nl.clockwork.ebms.admin.web.service.message.MessagesPage])
	//new MenuLinkItem(message,"3","messageSend",classOf[nl.clockwork.ebms.admin.web.service.message.SendMessagePage])
	new MenuLinkItem(message,"3","messageSend",classOf[nl.clockwork.ebms.admin.web.service.message.SendMessagePageX])
	new MenuLinkItem(message,"4","messageStatus",classOf[nl.clockwork.ebms.admin.web.service.message.MessageStatusPage])
	menuItems.add(message)

	val advanced = new MenuItem("3","advanced")
	new MenuLinkItem(advanced,"1","traffic",classOf[nl.clockwork.ebms.admin.web.message.TrafficPage])
	new MenuLinkItem(advanced,"2","trafficChart",classOf[nl.clockwork.ebms.admin.web.message.TrafficChartPage])
	new MenuDivider(advanced,"3")
	new MenuLinkItem(advanced,"4","cpas",classOf[nl.clockwork.ebms.admin.web.cpa.CPAsPage])
	new MenuLinkItem(advanced,"5","messages",classOf[nl.clockwork.ebms.admin.web.message.MessagesPage])
	menuItems.add(advanced)

	val configuration = new MenuItem("4","configuration")
	new MenuLinkItem(configuration,"1","ebMSAdminProperties",classOf[nl.clockwork.ebms.admin.web.configuration.EbMSAdminPropertiesPage])
	new MenuLinkItem(configuration,"2","ebMSCoreProperties",classOf[nl.clockwork.ebms.admin.web.configuration.EbMSCorePropertiesPage])
	menuItems.add(configuration)

	val extensionProviders = ExtensionProvider.get
	if (extensionProviders.size > 0)
	{
		val extensions = new MenuItem("5","extensions")
		menuItems.add(extensions)
		var i = 1
		for (provider <- extensionProviders)
		{
			val epmi = new MenuItem("" + i,provider.getName)
			extensions.addChild(epmi)
			for (menuItem <- provider.getMenuItems)
				epmi.addChild(menuItem)
			i += 1
		}
	}

	val messageProviders = MessageProvider.get
	messageProviders.foreach(provider => provider.getMessageViewPanels.foreach(messagePanel => messageViewPanels.put(messagePanel.getId,messagePanel)))

	messageProviders.foreach(provider => provider.getMessageEditPanels.foreach(messagePanel => messageEditPanels.put(messagePanel.getId,messagePanel)))

	val about = new MenuLinkItem("6","about",classOf[nl.clockwork.ebms.admin.web.AboutPage])
	menuItems.add(about)

	/**
	 * @see org.apache.wicket.Application#getHomePage
	 */
	override def getHomePage : Class[_ <: Page] = classOf[HomePage]

	/**
	 * @see org.apache.wicket.Application#init
	 */
	override def init
	{
		super.init
		getDebugSettings.setDevelopmentUtilitiesEnabled(true)
		getComponentInstantiationListeners.add(new SpringComponentInjector(this))
		getJavaScriptLibrarySettings.setJQueryReference(new JavaScriptResourceReference(classOf[HomePage],"../../../../../js/jquery-min.js"))
		getRequestCycleListeners.add(new AbstractRequestCycleListener
		{
			override def onException(cycle : RequestCycle, e : Exception) : IRequestHandler =
			{
				new RenderPageRequestHandler(new PageProvider(new ErrorPage(e)))
			}
		})
		mountPage("/404",classOf[PageNotFoundPage])
	}
	
	def getMenuItems : List[MenuItem] = menuItems

	def getMessageViewPanels : Map[String,MessageViewPanel] = messageViewPanels

	def getMessageEditPanels : Map[String,MessageEditPanel] = messageEditPanels
}
object WicketApplication
{
	def get : WicketApplication = WebApplication.get.asInstanceOf[WicketApplication]
}
