/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.configuration

import java.io.IOException
import java.io.StringWriter

import org.apache.commons.logging.LogFactory
import org.apache.wicket.markup.html.form.Button
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler
import org.apache.wicket.request.resource.ContentDisposition
import org.apache.wicket.util.resource.IResourceStream

class DownloadEbMSAdminPropertiesButton(val id : String, val resourceModel : ResourceModel, val ebMSAdminPropertiesFormModel : EbMSAdminPropertiesFormModel, val propertiesType : PropertiesType) extends Button(id,resourceModel)
{
	@transient val logger = LogFactory getLog getClass

	override def onSubmit
	{
		try
		{
			val writer = new StringWriter
			new EbMSAdminPropertiesWriter(writer).write(ebMSAdminPropertiesFormModel,propertiesType)
			val resourceStream = new StringWriterResourceStream(writer,"plain/text")
			getRequestCycle.scheduleRequestHandlerAfterCurrent(createRequestHandler(resourceStream))
		}
		catch
		{
			case e : IOException => logger error("",e); error(e getMessage)
		}
	}

	private def createRequestHandler(resourceStream : IResourceStream) : ResourceStreamRequestHandler =
		new ResourceStreamRequestHandler(resourceStream) setFileName propertiesType.filename setContentDisposition ContentDisposition.ATTACHMENT
}
