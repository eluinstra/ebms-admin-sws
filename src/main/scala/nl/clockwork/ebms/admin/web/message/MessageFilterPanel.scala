/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web.message

import java.util.{ArrayList, List}
import javax.xml.bind.JAXBException

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import nl.clockwork.ebms.admin.CPAUtils
import nl.clockwork.ebms.admin.Utils
import nl.clockwork.ebms.admin.web.{BasePage, BootstrapDateTimePicker, BootstrapFeedbackPanel, Type}
import nl.clockwork.ebms.common.XMLMessageBuilder
import nl.clockwork.ebms.service.CPAService
import nl.clockwork.ebms.service.EbMSMessageService
import org.apache.commons.logging.LogFactory
import org.apache.wicket.Component
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.head.IHeaderResponse
import org.apache.wicket.markup.head.OnDomReadyHeaderItem
import org.apache.wicket.markup.html.form._
import org.apache.wicket.markup.html.link.Link
import org.apache.wicket.markup.html.panel.Panel
import org.apache.wicket.model.CompoundPropertyModel
import org.apache.wicket.model.Model
import org.apache.wicket.model.PropertyModel
import org.apache.wicket.model.ResourceModel
import org.apache.wicket.spring.injection.annot.SpringBean
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.CollaborationProtocolAgreement

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

abstract class MessageFilterPanel(val id : String, val filter : MessageFilterFormModel) extends Panel(id)
{
	@transient val logger = LogFactory getLog getClass
	@SpringBean(name="cpaService") var cpaService : CPAService = _
	@SpringBean(name="ebMSMessageService") var ebMSMessageService : EbMSMessageService = _
	private var from : BootstrapDateTimePicker = _
	private var to : BootstrapDateTimePicker = _

	add(new BootstrapFeedbackPanel("feedback") setOutputMarkupId true)
	add(new MessageFilterForm("form",filter))
	add(createClearLink("clear"))

	private def createClearLink(id : String) : Link[Void] =
	{
		new Link[Void](id)
		{
			override def onClick = setResponsePage(getPage.getClass)
		}
	}
	
	override def renderHead(response : IHeaderResponse)
	{
		response.render(OnDomReadyHeaderItem forScript(BootstrapDateTimePicker getLinkBootstrapDateTimePickersJavaScript(from,to)))
		super.renderHead(response)
	}

	class MessageFilterForm(val id : String, val model : MessageFilterFormModel) extends Form[MessageFilterFormModel](id,new CompoundPropertyModel[MessageFilterFormModel](model))
	{
		add(createCPAIdChoice("cpaId"))
		add(createFromPartyIdChoice("fromRole.partyId"))
		add(createFromRoleChoice("fromRole.role"))
		add(createToPartyIdChoice("toRole.partyId"))
		add(createToRoleChoice("toRole.role"))
		add(createServiceChoice("service"))
		add(createActionChoice("action"))
		add(new TextField[String]("conversationId").setLabel(new ResourceModel("lbl.conversationId")))
		add(new TextField[String]("messageId").setLabel(new ResourceModel("lbl.messageId")))
		add(new TextField[String]("refToMessageId").setLabel(new ResourceModel("lbl.refToMessageId")))
		add(createStatusesChoice("statuses"))
		from = new BootstrapDateTimePicker("from","dd-MM-yyyy",Type.DATE)
		from.setLabel(new ResourceModel("lbl.from"))
		add(from)
		to = new BootstrapDateTimePicker("to","dd-MM-yyyy",Type.DATE)
		to.setLabel(new ResourceModel("lbl.to"))
		add(to)
		add(createSearchButton("search"))
		add(createResetButton("reset"))

		private def createCPAIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,Model.ofList(Utils.toList(cpaService.getCPAIds.toList)))
			.setLabel(new ResourceModel("lbl.cpaId"))
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageFilterForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetFromPartyIds(CPAUtils.getPartyIds(cpa))
						model.resetFromRoles
						model resetToPartyIds(CPAUtils.getPartyIds(cpa))
						model.resetToRoles
						model.resetServices
						model.resetActions
						target add getFeedbackComponent
						target add getForm
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
      })
		  .asInstanceOf[DropDownChoice[String]]

		private def createFromPartyIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[java.util.List[String]](getModelObject,"fromPartyIds"))
			{
				override def isEnabled : Boolean = MessageFilterForm.this.getModelObject.getToRole == null
			}
			.setLabel(new ResourceModel("lbl.fromPartyId"))
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageFilterForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetFromRoles(CPAUtils.getRoleNames(cpa,model.getFromRole getPartyId))
						model.resetServices
						model.resetActions
						target add getFeedbackComponent
						target add getForm
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
      })
		  .asInstanceOf[DropDownChoice[String]]

		private def createFromRoleChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[java.util.List[String]](getModelObject,"fromRoles"))
			{
				override def isEnabled : Boolean = MessageFilterForm.this.getModelObject.getToRole == null
			}
			.setLabel(new ResourceModel("lbl.fromRole"))
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageFilterForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetServices(CPAUtils.getServiceNames(cpa,model.getFromRole getRole))
						model.resetActions
						target add getFeedbackComponent
						target add getForm
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
      })
		  .asInstanceOf[DropDownChoice[String]]

		private def createToPartyIdChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[java.util.List[String]](getModelObject,"toPartyIds"))
			{
				override def isEnabled : Boolean = MessageFilterForm.this.getModelObject.getFromRole == null
			}
			.setLabel(new ResourceModel("lbl.toPartyId"))
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageFilterForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model resetToRoles(CPAUtils.getRoleNames(cpa,model.getToRole getPartyId))
						model.resetServices
						model.resetActions
						target add getFeedbackComponent
						target add getForm
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
      })
		  .asInstanceOf[DropDownChoice[String]]

		private def createToRoleChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[java.util.List[String]](this.getModelObject,"toRoles"))
			{
				override def isEnabled : Boolean = MessageFilterForm.this.getModelObject.getFromRole == null
			}
			.setLabel(new ResourceModel("lbl.toRole"))
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageFilterForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model.resetServices(CPAUtils.getServiceNames(cpa,model.getToRole getRole))
						model.resetActions
						target add getFeedbackComponent
						target add getForm
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e.getMessage)
					}
				}
      })
		  .asInstanceOf[DropDownChoice[String]]

		private def createServiceChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[java.util.List[String]](this.getModelObject,"services"))
			.setLabel(new ResourceModel("lbl.service"))
			.setOutputMarkupId(true)
			.add(new AjaxFormComponentUpdatingBehavior("onchange")
      {
				override protected def onUpdate(target : AjaxRequestTarget)
				{
					try
					{
						val model = MessageFilterForm.this.getModelObject
						val cpa = XMLMessageBuilder getInstance(classOf[CollaborationProtocolAgreement]) handle(cpaService getCPA model.getCpaId)
						model.resetActions(if (model.getFromRole == null) CPAUtils.getToActionNames(cpa,model.getToRole getRole,model getService) else CPAUtils.getFromActionNames(cpa,model.getFromRole getRole,model getService))
						target add getFeedbackComponent
						target add getForm
					}
					catch
					{
						case e : JAXBException => logger error("",e); error(e getMessage)
					}
				}
      })
		  .asInstanceOf[DropDownChoice[String]]

		private def createActionChoice(id : String) : DropDownChoice[String] =
			new DropDownChoice[String](id,new PropertyModel[java.util.List[String]](this.getModelObject,"actions"))
			.setLabel(new ResourceModel("lbl.action"))
			.setOutputMarkupId(true)
		  .asInstanceOf[DropDownChoice[String]]

		private def createStatusesChoice(id : String) : ListMultipleChoice[EbMSMessageStatus] =
			new ListMultipleChoice[EbMSMessageStatus](id,Model ofList EbMSMessageStatus.values.toList)
			{
				override protected def localizeDisplayValues : Boolean = true
			}
			.setMaxRows(4)
			.setLabel(new ResourceModel("lbl.status"))
		  .asInstanceOf[ListMultipleChoice[EbMSMessageStatus]]

		private def createSearchButton(id : String) : Button =
			new Button(id,new ResourceModel("cmd.search"))
			{
				override def onSubmit = setResponsePage(MessageFilterPanel.this.getPage(MessageFilterForm.this.getModelObject))
			}

		private def createResetButton(id : String) : Button =
			new Button(id,new ResourceModel("cmd.reset"))
			{
				override def onSubmit = setResponsePage(getPage getClass)
			}
	}

	def getPage(filter : MessageFilterFormModel) : BasePage

	private def getFeedbackComponent : Component =  this.get("feedback")

	private def getForm : Component = this.get("form")
}
object MessageFilterPanel
{
	def createMessageFilter : MessageFilterFormModel = new MessageFilterFormModel
}
class 	MessageFilterFormModel(
	@BeanProperty var fromPartyIds : List[String] = new ArrayList[String],
	@BeanProperty var fromRoles : List[String] = new ArrayList[String],
	@BeanProperty var toPartyIds : List[String] = new ArrayList[String],
	@BeanProperty var toRoles : List[String] = new ArrayList[String],
	@BeanProperty var services : List[String] = new ArrayList[String],
	@BeanProperty var actions : List[String] = new ArrayList[String]) extends EbMSMessageFilter
{
	def this() = this(new ArrayList[String],new ArrayList[String],new ArrayList[String],new ArrayList[String],new ArrayList[String],new ArrayList[String])

	def resetFromPartyIds
	{
		getFromPartyIds.clear
		setFromRole(null)
	}
	def resetFromPartyIds(partyIds : List[String])
	{
		resetFromPartyIds
		getFromPartyIds.addAll(partyIds)
	}
	def resetFromRoles
	{
		getFromRoles.clear
		if (getFromRole != null)
			getFromRole.setRole(null)
	}
	def resetFromRoles(roleNames : List[String])
	{
		resetFromRoles
		getFromRoles.addAll(roleNames)
	}
	def resetToPartyIds
	{
		getToPartyIds.clear
		setToRole(null)
	}
	def resetToPartyIds(partyIds : List[String])
	{
		resetToPartyIds
		getToPartyIds.addAll(partyIds)
	}
	def resetToRoles
	{
		getToRoles.clear
		if (getToRole != null)
			getToRole.setRole(null)
	}
	def resetToRoles(roleNames : List[String])
	{
		resetToRoles
		getToRoles.addAll(roleNames)
	}
	def resetServices
	{
		getServices.clear
		setService(null)
	}
	def resetServices(serviceNames : List[String])
	{
		resetServices
		getServices.addAll(serviceNames)
	}
	def resetActions
	{
		getActions.clear
		setAction(null)
	}
	def resetActions(actionNames : List[String])
	{
		resetActions
		getActions.addAll(actionNames)
	}
}
