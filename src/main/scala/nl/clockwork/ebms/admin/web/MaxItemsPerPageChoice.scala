/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.web

import java.util.Arrays

import org.apache.wicket.Component
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior
import org.apache.wicket.markup.html.form.DropDownChoice
import org.apache.wicket.model.IModel

class MaxItemsPerPageChoice(val id : String, val maxItemsPerPage : IModel[Int], val components : Component*) extends DropDownChoice[Int](id,maxItemsPerPage,Arrays.asList(5,10,15,20,25,50,100))
{
	add(new AjaxFormComponentUpdatingBehavior("onchange")
	{
		override protected def onUpdate(target : AjaxRequestTarget) = target.add(components : _*)
	})
}
