/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.model

import java.util.ArrayList
import java.util.Date
import java.util.List

import nl.clockwork.ebms.Constants.EbMSMessageStatus
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty

case class EbMSMessage(
	@BeanProperty var timestamp : Date,
	@BeanProperty var cpaId : String,
	@BeanProperty var conversationId : String,
	@BeanProperty var messageId : String,
	@BeanProperty var messageNr : Int,
	@BeanProperty var refToMessageId : String,
	@BeanProperty var timeToLive : Date,
	@BeanProperty var fromPartyId : String,
	@BeanProperty var fromRole : String,
	@BeanProperty var toPartyId : String,
	@BeanProperty var toRole : String,
	@BeanProperty var service : String,
	@BeanProperty var action : String,
	@BeanProperty var content : String,
	@BeanProperty var status : EbMSMessageStatus,
	@BeanProperty var statusTime : Date,
	@BeanProperty var attachments : List[EbMSAttachment] = new ArrayList[EbMSAttachment],
	@BeanProperty var event : EbMSEvent,
	@BeanProperty var events : List[EbMSEventLog] = new ArrayList[EbMSEventLog]) extends IClusterable
{
	def this() = this(null,null,null,null,0,null,null,null,null,null,null,null,null,null,null,null,new ArrayList[EbMSAttachment],null,new ArrayList[EbMSEventLog])
}