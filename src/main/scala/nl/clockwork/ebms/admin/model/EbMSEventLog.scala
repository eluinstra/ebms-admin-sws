/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin.model;

import java.util.Date

import nl.clockwork.ebms.Constants.EbMSEventStatus
import org.apache.wicket.util.io.IClusterable

import scala.beans.BeanProperty;

case class EbMSEventLog(@BeanProperty var timestamp : Date, @BeanProperty var uri : String, @BeanProperty var status : EbMSEventStatus, @BeanProperty var errorMessage : String) extends IClusterable
{
	@BeanProperty	var message : EbMSMessage = _

	def this() = this(null,null,null,null)
}
