/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin

import java.util.List

import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.CollaborationProtocolAgreement
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.CollaborationRole
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.PartyId
import org.oasis_open.committees.ebxml_cppa.schema.cpp_cpa_2_0.ServiceType

import scala.collection.JavaConversions._

object CPAUtils
{
	def toString(partyId : PartyId) : String = (if (partyId.getType == null) "" else partyId.getType + ":" ) + partyId.getValue

	def getPartyIds(cpa : CollaborationProtocolAgreement) : List[String] =
	{
		for (partyInfo <- cpa.getPartyInfo)
			yield toString(partyInfo.getPartyId.get(0))
//		cpa.getPartyInfo.map(partyInfo => toString(partyInfo.getPartyId.get(0)))
	}
	
	def getPartyIdsByRoleName(cpa : CollaborationProtocolAgreement, roleName : String) : List[String] =
	{
//		for (partyInfo <- cpa.getPartyInfo; role <- partyInfo.getCollaborationRole; if (roleName == null || roleName.equals(role.getRole.getName)))
//				yield toString(partyInfo.getPartyId.get(0))
		cpa.getPartyInfo.withFilter(partyInfo => roleName == null || partyInfo.getCollaborationRole.filter(role => roleName.equals(role.getRole.getName)).size > 0).map(partyInfo => toString(partyInfo.getPartyId.get(0)))
	}

	def getOtherPartyIds(cpa : CollaborationProtocolAgreement, partyId : String) : List[String] =
	{
//		for (partyInfo <- cpa.getPartyInfo; if (!partyId.equals(toString(partyInfo.getPartyId.get(0)))))
//			yield toString(partyInfo.getPartyId.get(0))
		cpa.getPartyInfo.withFilter(partyInfo => !partyId.equals(toString(partyInfo.getPartyId.get(0)))).map(partyInfo => toString(partyInfo.getPartyId.get(0)))
	}
	
	def getRoleNames(cpa : CollaborationProtocolAgreement) : List[String] =
	{
//		for (partyInfo <- cpa.getPartyInfo; role <- partyInfo.getCollaborationRole)
//			yield role.getRole.getName
		cpa.getPartyInfo.flatMap(partyInfo => partyInfo.getCollaborationRole).map(_.getRole.getName)
	}.distinct

	def getRoleNames(cpa : CollaborationProtocolAgreement, partyId : String) : List[String] =
	{
//		for (partyInfo <- cpa.getPartyInfo; if (partyId == null || partyId.equals(toString(partyInfo.getPartyId.get(0)))); role <- partyInfo.getCollaborationRole)
//			yield role.getRole.getName
		cpa.getPartyInfo.withFilter(partyInfo => partyId == null || partyId.equals(toString(partyInfo.getPartyId.get(0)))).flatMap(_.getCollaborationRole).map(_.getRole.getName)
	}.distinct

	def getOtherRoleNames(cpa : CollaborationProtocolAgreement, partyId : String, roleName : String) : List[String] =
	{
		for
		{
			partyInfo <- cpa.getPartyInfo
			if (partyId == null || !partyId.equals(partyInfo.getPartyId.get(0)))
			role <- partyInfo.getCollaborationRole
			if (roleName == null || !roleName.equals(role.getRole.getName))
		}
			yield role.getRole.getName
	}.distinct

	def getServiceNames(cpa : CollaborationProtocolAgreement, roleName : String) : List[String] =
	{
//		for (role <- findRoles(cpa,roleName))
//			yield getServiceName(role.getServiceBinding.getService))
		cpa.getPartyInfo.flatMap(_.getCollaborationRole).withFilter(role => role.getRole.getName.equals(roleName)).map(role => getServiceName(role.getServiceBinding.getService))
	}

	private def findRoles(cpa : CollaborationProtocolAgreement, roleName : String) : List[CollaborationRole] =
	{
//		for (partyInfo <- cpa.getPartyInfo; role <- partyInfo.getCollaborationRole; if (role.getRole.getName.equals(roleName)))
//			yield role
		cpa.getPartyInfo.flatMap(_.getCollaborationRole).filter(role => role.getRole.getName.equals(roleName))
	}
	
	def getServiceName(service : ServiceType) : String = (if (service.getType == null) "" else service.getType + ":") + service.getValue

	def equals(service : ServiceType, serviceName : String) : Boolean = getServiceName(service).equals(serviceName)

	def getFromActionNames(cpa : CollaborationProtocolAgreement, roleName : String, serviceName : String) : List[String] =
	{
		for (role <- findRoles(cpa,roleName,serviceName); canSend <- role.getServiceBinding.getCanSend)
			yield canSend.getThisPartyActionBinding.getAction
	}
	
	def getToActionNames(cpa : CollaborationProtocolAgreement, roleName : String, serviceName : String) : List[String] =
	{
		for (role <- findRoles(cpa,roleName,serviceName); canReceive <- role.getServiceBinding.getCanReceive)
			yield canReceive.getThisPartyActionBinding.getAction
	}
	
	private def findRoles(cpa : CollaborationProtocolAgreement, roleName : String, serviceName : String) : List[CollaborationRole] =
	{
		for (role <- findRoles(cpa,roleName); if (getServiceName(role.getServiceBinding.getService).equals(serviceName)))
			yield role
	}

}
