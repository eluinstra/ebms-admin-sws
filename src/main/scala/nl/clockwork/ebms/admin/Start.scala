/**
 * Copyright 2013 Clockwork
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.clockwork.ebms.admin

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.lang.management.ManagementFactory
import java.util.Collections
import java.util.EnumSet
import java.util.HashMap

import nl.clockwork.ebms.admin.web.ExtensionProvider
import org.apache.commons.cli.BasicParser
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Options
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import org.apache.commons.lang.StringUtils
import org.eclipse.jetty.jmx.MBeanContainer
import org.eclipse.jetty.security.ConstraintMapping
import org.eclipse.jetty.security.ConstraintSecurityHandler
import org.eclipse.jetty.security.HashLoginService
import org.eclipse.jetty.security.SecurityHandler
import org.eclipse.jetty.security.authentication.BasicAuthenticator
import org.eclipse.jetty.server.DispatcherType
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.bio.SocketConnector
import org.eclipse.jetty.server.ssl.SslSocketConnector
import org.eclipse.jetty.servlet.ErrorPageErrorHandler
import org.eclipse.jetty.servlet.FilterHolder
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.util.resource.Resource
import org.eclipse.jetty.util.security.Constraint
import org.eclipse.jetty.util.ssl.SslContextFactory
import org.springframework.web.context.ContextLoaderListener

import scala.collection.JavaConversions._

class Start
{
	protected final val DEFAULT_KEYSTORE_FILE = "nl/clockwork/ebms/admin/keystore.jks"
	protected final val DEFAULT_KEYSTORE_PASSWORD = "password"
	protected final val REALM = "Realm"
	protected final val REALM_FILE = "realm.properties"
	protected var options : Options = _
	protected var cmd : CommandLine = _
	protected var server : Server = _

	protected def initCmd(args : Array[String])
	{
		createOptions
		cmd = new BasicParser().parse(options,args)
	}

	protected def createOptions : Options =
	{
		options = new Options
		options.addOption("h",false,"print this message")
		options.addOption("host",true,"set host")
		options.addOption("port",true,"set port")
		options.addOption("path",true,"set path")
		options.addOption("ssl",false,"use ssl")
		options.addOption("keystore",true,"set keystore")
		options.addOption("password",true,"set keystore password")
		options.addOption("authentication",false,"use basic authentication")
		options.addOption("jmx",false,"start mbean server")
		options
	}
	
	protected def initWebServer
	{
		if (!cmd.hasOption("ssl"))
		{
			val connector = new SocketConnector
			connector.setHost(if (cmd.getOptionValue("host") == null) "0.0.0.0" else cmd.getOptionValue("host"))
			connector.setPort(if (cmd.getOptionValue("port") == null) 8080 else cmd.getOptionValue("port").toInt)
			server.addConnector(connector)
			System.out.println("Web server configured on http://" + getHost(connector.getHost) + ":" + connector.getPort + getPath)
		}
		else
		{
			val keystorePath = cmd.getOptionValue("keystore",DEFAULT_KEYSTORE_FILE)
			val keystorePassword = cmd.getOptionValue("password",DEFAULT_KEYSTORE_PASSWORD)
			if (DEFAULT_KEYSTORE_FILE.equals(keystorePath))
				System.out.println("Using default keystore!")
			else
				System.out.println("Using keystore " + new File(keystorePath).getAbsolutePath)
			val keystore = getResource(keystorePath)
			if (keystore != null && keystore.exists)
			{
				val factory = new SslContextFactory
				factory.setKeyStoreResource(keystore)
				factory.setKeyStorePassword(keystorePassword)
				val connector = new SslSocketConnector(factory)
				connector.setHost(if (cmd.getOptionValue("host") == null) "0.0.0.0" else cmd.getOptionValue("host"))
				connector.setPort(if (cmd.getOptionValue("port") == null) 8433 else cmd.getOptionValue("port").toInt)
				server.addConnector(connector)
				System.out.println("Web server configured on https://" + getHost(connector.getHost) + ":" + connector.getPort)
			}
			else
			{
				System.out.println("Web server not available: keystore " + keystorePath + " not found!")
				System.exit(1)
			}
		}
	}

	protected def getPath : String = if(cmd.getOptionValue("path") == null) "/" else cmd.getOptionValue("path")

	protected def initJMX
	{
		if (cmd.hasOption("jmx"))
		{
			System.out.println("Starting mbean server...")
			val mBeanServer = ManagementFactory.getPlatformMBeanServer
			val mBeanContainer = new MBeanContainer(mBeanServer)
			server.getContainer.addEventListener(mBeanContainer)
			mBeanContainer.start
		}
	}

	protected def initWebContext
	{
		val context = new ServletContextHandler(ServletContextHandler.SESSIONS)
		server.setHandler(context)

		context.setContextPath(getPath)

		if (cmd.hasOption("authentication"))
		{
			System.out.println("Configuring web server authentication:")
			val file = new File(REALM_FILE)
			if (file.exists)
				System.out.println("Using file: " + file.getAbsoluteFile)
			else
				createRealmFile(file)
			context.setSecurityHandler(getSecurityHandler)
		}

		context.setInitParameter("configuration","deployment")

		var contextConfigLocation = "classpath:nl/clockwork/ebms/admin/applicationContext.xml"
		val extensionProviders = ExtensionProvider.get
		for (extensionProvider <- extensionProviders; if (!StringUtils.isEmpty(extensionProvider.getSpringConfigurationFile)))
			contextConfigLocation = "," + extensionProvider.getSpringConfigurationFile

		context.setInitParameter("contextConfigLocation",contextConfigLocation)

		val servletHolder = new ServletHolder(classOf[nl.clockwork.ebms.admin.web.ResourceServlet])
		context.addServlet(servletHolder,"/css/*")
		context.addServlet(servletHolder,"/fonts/*")
		context.addServlet(servletHolder,"/images/*")
		context.addServlet(servletHolder,"/js/*")

		context.addServlet(classOf[org.eclipse.jetty.servlet.DefaultServlet],"/")
		
		val filterHolder = new FilterHolder(classOf[org.apache.wicket.protocol.http.WicketFilter])
		filterHolder.setInitParameter("applicationClassName","nl.clockwork.ebms.admin.web.WicketApplication")
		filterHolder.setInitParameter("filterMappingUrlPattern","/*")
		context.addFilter(filterHolder,"/*",EnumSet.of(DispatcherType.REQUEST,DispatcherType.ERROR))
		
		val errorHandler = new ErrorPageErrorHandler
		context.setErrorHandler(errorHandler)
		val errorPages = new HashMap[String,String]
		errorPages.put("404","/404")
		errorHandler.setErrorPages(errorPages)
		
		val listener = new ContextLoaderListener
		context.addEventListener(listener)
	}

	protected def printUsage
	{
		val formatter = new HelpFormatter
		formatter.printHelp("Start",options,true)
		System.exit(0)
	}

	protected def getResource(path : String) : Resource =
	{
		val result = Resource.newResource(path)
		if (result.exists) result else Resource newClassPathResource path
	}

	protected def createRealmFile(file : File)
	{
		val reader = new BufferedReader(new InputStreamReader(System.in))
		val username = readLine("enter username: ",reader)
		val password = readPassword(reader)
		System.out.println("Writing to file: " + file.getAbsoluteFile)
		FileUtils.writeStringToFile(file,username + ": " + password + ",user",false)
	}

	private def readLine(prompt : String, reader : BufferedReader) : String =
	{
		var result = ""
		while (StringUtils.isBlank(result))
		{
			System.out.print(prompt)
			result = reader.readLine
		}
		result
	}

	private def readPassword(reader : BufferedReader) : String =
	{
		var done = false
		var result = ""
		while (!done)
		{
			result = toMD5(readLine("enter password: ",reader))
			val password = toMD5(readLine("re-enter password: ",reader))
			if (!result.equals(password))
				System.out.println("Passwords don't match! Try again.")
			else
				done = true
		}
		result
	}
	
	private def toMD5(s : String) : String = "MD5:" + DigestUtils.md5Hex(s)

	protected def getSecurityHandler : SecurityHandler =
	{
		val constraint = new Constraint
		constraint.setName("auth")
		constraint.setAuthenticate(true)
		constraint.setRoles(Array[String]("user","admin"))

		val mapping = new ConstraintMapping
		mapping.setPathSpec("/*")
		mapping.setConstraint(constraint)

		val security = new ConstraintSecurityHandler
		security.setConstraintMappings(Collections.singletonList(mapping))
		security.setAuthenticator(new BasicAuthenticator)
		security.setLoginService(new HashLoginService(REALM,REALM_FILE))
		security
	}

	protected def getHost(host : String) : String = if("0.0.0.0".equals(host)) "localhost" else host
}
object Start
{
	def main(args : Array[String])
	{
		val start = new Start
		start.initCmd(args)

		if (start.cmd.hasOption("h"))
			start.printUsage

		start.server = new Server

		start.initWebServer
		start.initJMX
		start.initWebContext

		System.out.println("Starting web server...")

		start.server.start
		start.server.join
	}
}